﻿using Model.Core.Entity;
using Model.Core.Entity.Modules;
using Model.Core.Game;
using Model.Core.GameEvent;
using Model.Core.Player;
using Model.Core.World;
using Model.Generator;
using Model.Statistics;
using Model.Timer;
using Model.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Text;

namespace Model.Factory
{
    [DataContract(IsReference = true)]
    public class SimpleFactory : IFactory
    {
        /// <summary>
        /// Retourne une nouvelle partiee.
        /// </summary>
        /// <param name="name">Nom de la partie.</param>
        /// <param name="seed">Graine de génération du monde de la partie.</param>
        /// <param name="worldParameters">Paramètres du monde de la partie.</param>
        /// <returns>La nouvelle partie.</returns>
        public IGame BuildIGame(string name, int? seed = null, IWorldParameters worldParameters = null)
        {
            return new SimpleGame(name, BuildIGameEvents(), BuildIWorld(seed, worldParameters), BuildITimer(AppConfig.UsedConfig.TickDuration));
        }

        /// <summary>
        /// Retourne une nouvelle bactérie.
        /// </summary>
        /// <param name="location">Location de la bactérie.</param>
        /// <param name="world">Monde dans lequel la bactérie va agir.</param>
        /// <returns>La nouvelle bactérie.</returns>
        public Bacteria BuildBacteria(Vector2 location, IWorld world)
        {
            Type type = typeof(Bacteria);
            return new SimpleBacteria(location, BuildIDieModule(type), BuildIFoodModule(type), BuildIMoveModule(type, world), BuildIReproduceModule(type), BuildIInfectableModule(type));
        }

        /// <summary>
        /// Retourne un nouvel euplote.
        /// </summary>
        /// <param name="location">Location de l'euplote.</param>
        /// <param name="world">Monde dans lequel l'euplote va agir.</param>
        /// <returns>Le nouvel euplote.</returns>
        public Euplote BuildEuplote(Vector2 location, IWorld world)
        {
            Type type = typeof(Euplote);
            return new SimpleEuplote(location, BuildIDieModule(type), BuildIFoodModule(type), BuildIMoveModule(type, world), BuildIReproduceModule(type), BuildIPredatorModule(type, world));
        }

        /// <summary>
        /// Retourne un nouveau joueur.
        /// </summary>
        /// <returns>Le nouveau joueur.</returns>
        public IPlayer BuildIPlayer()
        {
            return new SimplePlayer();
        }

        /// <summary>
        /// Retourne un créateur de statistiques.
        /// </summary>
        /// <returns>Le nouvel objet.</returns>
        public IStatistics BuildIStatistics()
        {
            return new SimpleStatistics();
        }

        /// <summary>
        /// Retourne un nouveau dictionnaire.
        /// </summary>
        /// <returns>Le nouveau dictionnaire.</returns>
        public IDictionary<string, IGameEvent> BuildIGameEvents()
        {
            return new Dictionary<string, IGameEvent>() { { AppConfig.UsedConfig.OilSpillEventName, new SimpleOilSpillGameEvent() } };
        }

        /// <summary>
        /// Retourne un nouveau monde avec son générateur de chunk.
        /// </summary>
        /// <param name="seed">Seed du monde. Si non renseignée, elle est choisie aléatoirement.</param>
        /// <param name="worldParameters">Paramètres du monde de la partie.</param>
        /// <returns>Le nouveau monde.</returns>
        private IWorld BuildIWorld(int? seed = null, IWorldParameters worldParameters = null)
        {
            if (worldParameters == null)
            {
                worldParameters = BuildIWorldParameters();
            }
            if (seed == null)
            {
                seed = Randomizer.Random.Next();
            }
            return new SimpleWorld((int)seed, BuildIChunkGenerator(), BuildIEntityGenerator(), worldParameters);
        }

        /// <summary>
        /// Retourne les paramètres de monde par défaut.
        /// </summary>
        /// <returns>Paramètres de monde par défaut.</returns>
        private IWorldParameters BuildIWorldParameters()
        {
            AppConfig appConfig = AppConfig.UsedConfig;
            return new SimpleWorldParameters(appConfig.SimpleWorldParametersDefaultTemperature, appConfig.SimpleWorldParametersDefaultSalinity,
                appConfig.SimpleWorldParametersDefaultOxygenLevel, appConfig.SimpleWorldParametersDefaultBrightness, appConfig.SimpleWorldParametersDefaultPollution, appConfig.SimpleWorldParametersDefaultFood);
        }

        /// <summary>
        /// Retourne un nouveau module de mort.
        /// </summary>
        /// <param name="type">Type de l'entité qui va recevoir le module.</param>
        /// <returns>Nouveau module.</returns>
        public IDie BuildIDieModule(Type type)
        {
            switch (type.Name)
            {
                case Bacteria.TypeName:
                    return new SimpleHealthDie();
                case Euplote.TypeName:
                    SimpleHealthDie simpleHealthDie = new SimpleHealthDie();
                    SimpleSizeDie simpleSizeDie = new SimpleSizeDie();
                    return new EuploteDie(simpleHealthDie, simpleSizeDie);
                default:
                    throw new NotSupportedException("Ce type d'entité n'est pas supporté par la SimpleFactory.");
            }
        }

        /// <summary>
        /// Retourne un nouveau module de proie.
        /// </summary>
        /// <param name="type">Type de l'entité qui va recevoir le module.</param>
        /// <returns>Nouveau module.</returns>
        public IFood BuildIFoodModule(Type type)
        {
            switch (type.Name)
            {
                case Bacteria.TypeName:
                    return new SimpleFood();
                case Euplote.TypeName:
                    return new SimpleFood();
                default:
                    throw new NotSupportedException("Ce type d'entité n'est pas supporté par la SimpleFactory.");
            }
        }

        /// <summary>
        /// Retourne un nouveau module d'infection.
        /// </summary>
        /// <param name="type">Type de l'entité qui va recevoir le module.</param>
        /// <returns>Nouveau module.</returns>
        public IInfectable BuildIInfectableModule(Type type)
        {
            //Debug.WriteLine($"type.Name : {type.Name}, Bacteria.TypeName : {Bacteria.TypeName} / Cohérence : {type.Name == Bacteria.TypeName}");
            switch (type.Name)
            {
                case Bacteria.TypeName:
                    return new SimpleInfectable();
                case Euplote.TypeName:
                    return null;
                default:
                    throw new NotSupportedException("Ce type d'entité n'est pas supporté par la SimpleFactory.");
            }
        }

        /// <summary>
        /// Retourne un nouveau module de mouvement.
        /// </summary>
        /// <param name="type">Type de l'entité qui va recevoir le module.</param>
        /// <param name="world">Monde dans lequel le module va agir.</param>
        /// <returns>Nouveau module.</returns>
        public IMove BuildIMoveModule(Type type, IWorld world)
        {
            switch (type.Name)
            {
                case Bacteria.TypeName:
                    return new SimpleMove(AppConfig.UsedConfig.BacteriaBaseSpeed, world);
                case Euplote.TypeName:
                    return new SimpleMove(AppConfig.UsedConfig.EuploteBaseSpeed, world);
                default:
                    throw new NotSupportedException("Ce type d'entité n'est pas supporté par la SimpleFactory.");
            }
        }

        /// <summary>
        /// Retourne un nouveau module de prédation.
        /// </summary>
        /// <param name="type">Type de l'entité qui va recevoir le module.</param>
        /// <param name="world">Monde dans lequel le module va agir.</param>
        /// <returns>Nouveau module.</returns>
        public IPredator BuildIPredatorModule(Type type, IWorld world)
        {
            switch (type.Name)
            {
                case Euplote.TypeName:
                    return new SimplePredator(world);
                default:
                    throw new NotSupportedException("Ce type d'entité n'est pas supporté par la SimpleFactory.");
            }
        }

        /// <summary>
        /// Retourne un nouveau module de reproduction.
        /// </summary>
        /// <param name="type">Type de l'entité qui va recevoir le module.</param>
        /// <returns>Nouveau module.</returns>
        public IReproduce BuildIReproduceModule(Type type)
        {
            AppConfig appConfig = AppConfig.UsedConfig;
            switch (type.Name)
            {
                case Bacteria.TypeName:
                    return new SimpleReproduce(appConfig.BacteriaRequiredSizeToReproduce, appConfig.BacteriaRequiredLifePercentageToReproduce);
                case Euplote.TypeName:
                    return new SimpleReproduce(appConfig.EuploteRequiredSizeToReproduce, appConfig.EuploteRequiredLifePercentageToReproduce);
                default:
                    throw new NotSupportedException("Ce type d'entité n'est pas supporté par la SimpleFactory.");
            }
        }

        /// <summary>
        /// Retourne un nouveau générateur de chunk.
        /// </summary>
        /// <returns>Nouveau générateur.</returns>
        public IChunkGenerator BuildIChunkGenerator()
        {
            return new PerlinChunkGenerator(NormalizationMethod.Local);
        }

        /// <summary>
        /// Retourne un nouveau générateur d'entités.
        /// </summary>
        /// <returns>Nouveau générateur.</returns>
        public IEntityGenerator BuildIEntityGenerator()
        {
            return new SimpleEntityGenerator(this);
        }

        /// <summary>
        /// Retourne un nouveau timer.
        /// </summary>
        /// <param name="frequency">Fréquence du timer.</param>
        /// <returns>Nouveau timer.</returns>
        public ITimer BuildITimer(int frequency)
        {
            return new SimpleTimer(frequency);
        }
    }
}
