﻿using Model.Core.Entity;
using Model.Core.Game;
using Model.Core.Player;
using Model.Core.World;
using Model.Core.GameEvent;
using System;
using System.Collections.Generic;
using System.Text;
using Model.Statistics;
using Model.Core.Entity.Modules;
using Model.Utils;
using Model.Generator;
using Model.Timer;

namespace Model.Factory
{
    /// <summary>
    /// Interface qui définit ce qu'est une factory, ainsi que les objets qu'elle doit permettre d'instancier.
    /// </summary>
    public interface IFactory
    {
        /// <summary>
        /// Retourne une nouvelle partiee.
        /// </summary>
        /// <param name="name">Nom de la partie.</param>
        /// <param name="seed">Graine de génération du monde de la partie.</param>
        /// <param name="worldParameters">Paramètres du monde de la partie.</param>
        /// <returns>La nouvelle partie.</returns>
        IGame BuildIGame(string name, int? seed = null, IWorldParameters worldParameters = null);

        /// <summary>
        /// Retourne une nouvelle bactérie.
        /// </summary>
        /// <param name="location">Location de la bactérie.</param>
        /// <param name="world">Monde dans lequel la bactérie va agir.</param>
        /// <returns>La nouvelle bactérie.</returns>
        Bacteria BuildBacteria(Vector2 location, IWorld world);

        /// <summary>
        /// Retourne un nouvel euplote.
        /// </summary>
        /// <param name="location">Location de l'euplote.</param>
        /// <param name="world">Monde dans lequel l'euplote va agir.</param>
        /// <returns>Le nouvel euplote.</returns>
        Euplote BuildEuplote(Vector2 location, IWorld world);

        /// <summary>
        /// Retourne un nouveau joueur.
        /// </summary>
        /// <returns>Le nouveau joueur.</returns>
        IPlayer BuildIPlayer();

        /// <summary>
        /// Retourne un créateur de statistiques.
        /// </summary>
        /// <returns>Retourne le nouvel objet.</returns>
        IStatistics BuildIStatistics();

        /// <summary>
        /// Retourne un nouveau module de mort.
        /// </summary>
        /// <param name="type">Type de l'entité qui va recevoir le module.</param>
        /// <returns>Nouveau module.</returns>
        IDie BuildIDieModule(Type type);

        /// <summary>
        /// Retourne un nouveau module de proie.
        /// </summary>
        /// <param name="type">Type de l'entité qui va recevoir le module.</param>
        /// <returns>Nouveau module.</returns>
        IFood BuildIFoodModule(Type type);

        /// <summary>
        /// Retourne un nouveau module d'infection.
        /// </summary>
        /// <param name="type">Type de l'entité qui va recevoir le module.</param>
        /// <returns>Nouveau module.</returns>
        IInfectable BuildIInfectableModule(Type type);

        /// <summary>
        /// Retourne un nouveau module de mouvement.
        /// </summary>
        /// <param name="type">Type de l'entité qui va recevoir le module.</param>
        /// <param name="world">Monde dans lequel le module va agir.</param>
        /// <returns>Nouveau module.</returns>
        IMove BuildIMoveModule(Type type, IWorld world);

        /// <summary>
        /// Retourne un nouveau module de prédation.
        /// </summary>
        /// <param name="type">Type de l'entité qui va recevoir le module.</param>
        /// <param name="world">Monde dans lequel le module va agir.</param>
        /// <returns>Nouveau module.</returns>
        IPredator BuildIPredatorModule(Type type, IWorld world);

        /// <summary>
        /// Retourne un nouveau module de reproduction.
        /// </summary>
        /// <param name="type">Type de l'entité qui va recevoir le module.</param>
        /// <returns>Nouveau module.</returns>
        IReproduce BuildIReproduceModule(Type type);

        /// <summary>
        /// Retourne un nouveau générateur de chunk.
        /// </summary>
        /// <returns>Nouveau générateur.</returns>
        IChunkGenerator BuildIChunkGenerator();

        /// <summary>
        /// Retourne un nouveau générateur d'entités.
        /// </summary>
        /// <returns>Nouveau générateur.</returns>
        IEntityGenerator BuildIEntityGenerator();

        /// <summary>
        /// Retourne un nouveau timer.
        /// </summary>
        /// <param name="frequency">Fréquence du timer.</param>
        /// <returns>Nouveau timer.</returns>
        ITimer BuildITimer(int frequency);

    }
}
