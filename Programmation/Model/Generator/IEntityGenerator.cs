﻿using Model.Core.Chunk;
using Model.Core.Entity;
using Model.Core.World;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Generator
{
    /// <summary>
    /// Interface qui défini ce que doit réaliser un générateur d'entités.
    /// </summary>
    public interface IEntityGenerator
    {
        /// <summary>
        /// Défini les bactéries à instancier sur un chunk en partant du principe que cer dernier est nouveau.
        /// </summary>
        /// <param name="chunk">Chunk sur lequel on doit générer les bactéries.</param>
        /// <param name="world">Monde possédant le chunk.</param>
        /// <returns>Bactéries générées dans le monde.</returns>
        IEnumerable<Bacteria> GenerateBacteriaOnChunk(IChunk chunk, IWorld world);

        /// <summary>
        /// Défini les euplotes à instancier sur un chunk en partant du principe que cer dernier est nouveau.
        /// </summary>
        /// <param name="chunk">Chunk sur lequel on doit générer les euplotes.</param>
        /// <param name="world">Monde possédant le chunk.</param>
        /// <returns>Euplotes générées dans le monde.</returns>
        IEnumerable<Euplote> GenerateEuploteOnChunk(IChunk chunk, IWorld world);
    }
}
