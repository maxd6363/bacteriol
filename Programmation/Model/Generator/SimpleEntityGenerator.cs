﻿using Model.Core.Chunk;
using Model.Core.Entity;
using Model.Core.World;
using Model.Factory;
using Model.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Text;

namespace Model.Generator
{
    /// <summary>
    /// Implémentation basique de l'interface IEntityGenerator.
    /// </summary>
    [DataContract(IsReference = true)]
    [KnownType(typeof(SimpleFactory))]
    public class SimpleEntityGenerator : IEntityGenerator
    {
        /// <summary>
        /// Retourne ou modifie la factory à utliser pour l'instanciation des entités.
        /// </summary>
        [DataMember]
        private IFactory _Factory { get; set; }

        /// <summary>
        /// Constructeur du générateur.
        /// </summary>
        /// <param name="factory">Factory à utiliser pour l'instanciation des entités.</param>
        public SimpleEntityGenerator(IFactory factory)
        {
            _Factory = factory;
        }

        /// <summary>
        /// Défini les bactéries à instancier sur un chunk en partant du principe que cer dernier est nouveau.
        /// </summary>
        /// <param name="chunk">Chunk sur lequel on doit générer les bactéries.</param>
        /// <param name="world">Monde possédant le chunk.</param>
        /// <returns>Bactéries générées dans le monde.</returns>
        public IEnumerable<Bacteria> GenerateBacteriaOnChunk(IChunk chunk, IWorld world)
        {
            List<Bacteria> bacterias = new List<Bacteria>();
            int nbBacteriasOnChunk = (int)(chunk.NodesAverage * AppConfig.UsedConfig.SimpleEntityGeneratorBacteriaSpawnRate);
            for(int i = 0; i < nbBacteriasOnChunk; i++)
            {
                int posX = (int)(chunk.Position.X * chunk.Size.X) + Randomizer.Random.Next((int)chunk.Size.X);
                int posY = (int)(chunk.Position.Y * chunk.Size.Y) + Randomizer.Random.Next((int)chunk.Size.Y);
                bacterias.Add(_Factory.BuildBacteria(new Vector2(posX, posY), world));
            }
            return bacterias;
        }

        /// <summary>
        /// Défini les euplotes à instancier sur un chunk en partant du principe que cer dernier est nouveau.
        /// </summary>
        /// <param name="chunk">Chunk sur lequel on doit générer les euplotes.</param>
        /// <param name="world">Monde possédant le chunk.</param>
        /// <returns>Euplotes générées dans le monde.</returns>
        public IEnumerable<Euplote> GenerateEuploteOnChunk(IChunk chunk, IWorld world)
        {
            List<Euplote> euplotes = new List<Euplote>();
            int nbEuplotesOnChunk = (int)(chunk.NodesAverage * AppConfig.UsedConfig.SimpleEntityGeneratorBacteriaSpawnRate);
            for (int i = 0; i < nbEuplotesOnChunk; i++)
            {
                int posX = (int)(chunk.Position.X * chunk.Size.X) + Randomizer.Random.Next((int)chunk.Size.X);
                int posY = (int)(chunk.Position.Y * chunk.Size.Y) + Randomizer.Random.Next((int)chunk.Size.Y);
                euplotes.Add(_Factory.BuildEuplote(new Vector2(posX, posY), world));
            }
            return euplotes;
        }
    }
}
