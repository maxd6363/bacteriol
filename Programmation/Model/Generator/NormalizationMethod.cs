﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Generator
{
    /// <summary>
    /// Enumération de toutes les méthodes de normalisation possibles par le PerlinChunkGenerator.
    /// </summary>
    public enum NormalizationMethod
    {
        /* 
            Local   --> Normalise les valeurs du bruit de façon interne à chaque chunk.
                        Très précis pour un monde monochunk mais possède des risques éventuels de
                        créer des incohérences au cours de la génération d'un monde multichunk.

            Global  --> Normalise les valeurs du bruit de façon globale (interchunk) en faisant une estimation
                        de la valeur maximale qu'il est possible d'obtenir dans le monde entier (y compris dans les
                        chunks qui ne sont pas encoré générés).
                        Moins précis que la normalisation locale, mais supprime tout risque d'incohérence
                        au cours de la génération d'un monde multichunk.
        */
        Local, 
        Global
    }
}
