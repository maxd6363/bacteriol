﻿using Model.Core.Chunk;
using Model.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Generator
{
    /// <summary>
    /// Interface qui définit ce qu'est un générateur de chunk.
    /// </summary>
    public interface IChunkGenerator
    {
        /// <summary>
        /// Génère un chunk en fonction d'une graîne de génération et de la position du chunk.
        /// </summary>
        /// <param name="seed">Graîne de génération.</param>
        /// <param name="position">Position du chunk à générer.</param>
        /// <param name="chunkSize">Taille de tout chunk généré dans le monde.</param>
        /// <returns>Le nouveau chunk.</returns>
        IChunk GenerateChunk(int seed, Vector2 position, Vector2 chunkSize);
    }
}
