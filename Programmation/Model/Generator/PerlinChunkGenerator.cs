﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Model.Core.Chunk;
using Model.Utils;

namespace Model.Generator
{
    /// <summary>
    /// Générateur procédural de chunk implémentant IChunkGenerator et utilisant une heightMap.
    /// </summary>
    [DataContract(IsReference = true)]
    public class PerlinChunkGenerator : IChunkGenerator
    {
        /// <summary>
        /// Taille minimum d'une table de permutation.
        /// Cette valeur est propre à cette classe et ne doit jamais changer (même en cas de changement de configuration).
        /// </summary>
        const ushort TableMinimumSize = 32;

        /// <summary>
        /// Valeur initiale de l'amplitude lors de la génération d'une HeightMap.
        /// Cette valeur est propre à cette classe et ne doit jamais changer (même en cas de changement de configuration).
        /// </summary>
        const float InitialAmplitude = 1;

        /// <summary>
        /// Valeur initiale de la fréquence lors de la génération d'une HeightMap.
        /// Cette valeur est propre à cette classe et ne doit jamais changer (même en cas de changement de configuration).
        /// </summary>
        const float InitialFrequency = 1;

        /// <summary>
        /// Valeur initiale de la hauteur lors de la génération d'une HeightMap.
        /// Cette valeur est propre à cette classe et ne doit jamais changer (même en cas de changement de configuration).
        /// </summary>
        const float InitialHeight = 0;

        /// <summary>
        /// Valeur initiale de l'estimation de la hauteur maximum lors de la génération d'une HeightMap (pour la normalisation).
        /// Cette valeur est propre à cette classe et ne doit jamais changer (même en cas de changement de configuration).
        /// </summary>
        const float InitialHeightEstimation = 0;

        /// <summary>
        /// Régulateur de la méthode de normalisation globale.
        /// Plus il est élevé : plus les valeurs du bruit de Perlin atteindront souvent la valeur maximum.
        /// Plus il est bas : plus les valeurs du bruit de Perlin seront abaissées.
        /// Cette valeur est propre à cette classe et ne doit jamais changer (même en cas de changement de configuration).
        /// </summary>
        const float GlobalNormalisationCoefficient = 1.17647058f;

        /// <summary>
        /// Afin d'éviter de devoir recalculer la table de permutation à chaque fois lorsque l'on veut générer un nouveau chunk,
        /// la dernière table de permutation utilisée est stockée ici. Elle est alors réutilisée si sa Seed (premier élément du Tuple
        /// est la même que celle demandée.
        /// </summary>
        [DataMember]
        private Tuple<int?, ushort[]> LastPermutationTableUsed { get; set; }

        /// <summary>
        /// Retourne le mode de normalisation du générateur.
        /// </summary>
        [DataMember]
        public NormalizationMethod NormalizationMethod { get; private set; }

        /// <summary>
        /// Constructeur du générateur de chunk.
        /// </summary>
        /// <param name="normalizationMethod">Mode de normalisation à utiliser.</param>
        public PerlinChunkGenerator(NormalizationMethod normalizationMethod)
        {
            NormalizationMethod = normalizationMethod;
            LastPermutationTableUsed = Tuple.Create((int?)null, new ushort[1]);
        }

        /// <summary>
        /// Génère un chunk en fonction d'une graîne de génération et de la position du chunk.
        /// </summary>
        /// <param name="seed">Graîne de génération.</param>
        /// <param name="position">Position du chunk à générer.</param>
        /// <param name="chunkSize">Taille de tout chunk généré dans le monde.</param>
        /// <returns>Le nouveau chunk.</returns>
        public IChunk GenerateChunk(int seed, Vector2 position, Vector2 chunkSize)
        {
            // Actualisation de la table de permutation utilisée.
            if(seed != LastPermutationTableUsed.Item1)
            {
                Randomizer.Seed = seed;
                ushort[] newPermutationTable = GeneratePermutationTable(AppConfig.UsedConfig.PerlinChunkGeneratorTableSize);
                LastPermutationTableUsed = Tuple.Create((int?)seed, newPermutationTable);
            }

            // Création du chunk.
            float[,] nodes = HeightMap(LastPermutationTableUsed.Item2, position * chunkSize, (int)chunkSize.X, (int)chunkSize.Y,
                AppConfig.UsedConfig.PerlinChunkGeneratorZoom, AppConfig.UsedConfig.PerlinChunkGeneratorNbOctaves,
                AppConfig.UsedConfig.PerlinChunkGeneratorPersistance, AppConfig.UsedConfig.PerlinChunkGeneratorImperfections);
            return new SimpleChunk(position, chunkSize, nodes);
        }

        /// <summary>
        /// Génère une HeightMap en fonction d'une multitude de paramètre.
        /// </summary>
        /// <param name="permutationTable">Table de permutation à utiliser.</param>
        /// <param name="coordinates">Coordonnée du premier point (celui en haut à gauche) à générer.</param>
        /// <param name="lengthX">Longueur du tableau en X.</param>
        /// <param name="lengthY">Longueur du tableau en Y.</param>
        /// <param name="zoom">Zoom à utiliser.</param>
        /// <param name="nbOctaves">Nombre d'octave (plus il est élevé est plus la heightmap aura du détail).</param>
        /// <param name="persistence">Persistance des reliefs (plus elle est élevée et plus les reliefs seront bas).</param>
        /// <param name="imperfections"></param>
        /// <returns>Retourne la HeightMap.</returns>
        private float[,] HeightMap(ushort[] permutationTable, Vector2 coordinates, int lengthX, int lengthY, float zoom, int nbOctaves, float persistence, float imperfections)
        {
            if (lengthX <= 0 || lengthY <= 0 || zoom <= 0 || nbOctaves <= 0)
            {
                throw new Exception("Les paramètres : lengthX, lengthY, zoom et nbOctaves doivent tous être supérieures à 0.");
            }

            // Initialisation.
            float[,] heightMap = new float[lengthX, lengthY]; // Tableau à compléter.
            float amplitude = InitialAmplitude; // Paramètre propre à chaque octave, plus l'amplitude est élevée est plus les reliefs sont importants.
            float frequency; // Paramètre propre à chaque octave, permet de faire un relief différent à chaque octave.
            float height; // Valeur qui completera la variable heightMap.
            float maxHeight = float.MinValue; // Utilisée pour le lissage local.
            float minHeight = float.MaxValue; // Utilisée pour le lissage local.
            float tmpX, tmpY; // A calculer en fonction de la fréquence et du bruit de la variable perlin.
            float perlin; // Valeur issue d'un bruit de Perlin.
            float maxHeightEstimation = InitialHeightEstimation; // Permet de rendre les chunks cohérents entre eux (utilisée dans la normalisation).

            for (int i = 0; i < nbOctaves; i++)
            {
                maxHeightEstimation += amplitude;
                amplitude *= persistence;
            }

            // Génération des points de la HeightMap.
            for (int y = 0; y < lengthY; y++)
            {
                for (int x = 0; x < lengthX; x++)
                {
                    // Initialisation du cycle.
                    amplitude = InitialAmplitude;
                    frequency = InitialFrequency;
                    height = InitialHeight;

                    // Calcul des valeurs de la HeightMap.
                    for (int o = 0; o < nbOctaves; o++)
                    {
                        tmpX = (x + (float)coordinates.X) * frequency;
                        tmpY = (y + (float)coordinates.Y) * frequency;
                        perlin = PerlinNoise2D(permutationTable, tmpX, tmpY, zoom);
                        height += perlin * amplitude;
                        amplitude *= persistence;
                        frequency *= imperfections;
                    }
                    if (height > maxHeight)
                    {
                        maxHeight = height;
                    }
                    if (height < minHeight)
                    {
                        minHeight = height;
                    }
                    heightMap[x, y] = height;
                }
            }

            // Lissage des valeurs (elles seront toutes entre 0 et 1 après ça).
            switch(NormalizationMethod)
            {
                case NormalizationMethod.Local:
                    float gap = maxHeight - minHeight;
                    for (int y = 0; y < lengthY; y++)
                    {
                        for (int x = 0; x < lengthX; x++)
                        {
                            heightMap[x, y] = (heightMap[x, y] - minHeight) / gap;
                        }
                    }
                    break;
                case NormalizationMethod.Global:
                    float standardizedHeight;
                    for (int y = 0; y < lengthY; y++)
                    {
                        for (int x = 0; x < lengthX; x++)
                        {
                            standardizedHeight = (heightMap[x, y] + 1) / (maxHeightEstimation * GlobalNormalisationCoefficient);
                            heightMap[x, y] = MathExtension.Clamp(standardizedHeight, 0, int.MaxValue);
                        }
                    }
                    break;
                default:
                    throw new NotImplementedException("Ce mode de normalisation n'est pas pris en charge par cette classe.");
            }

            return heightMap;
        }

        /// <summary>
        /// Génère une valeur de bruit de perlin en fonction d'une table de permutation, de coordonnées ainsi que d'un zoom.
        /// </summary>
        /// <param name="permutationTable">Table de permutation à utiliser.</param>
        /// <param name="x">Coordonnée X.</param>
        /// <param name="y">Coordonnée Y.</param>
        /// <param name="zoom">Zoom sur le bruit.</param>
        /// <returns>Retourne une valeur altérée par le bruit de Perlin.</returns>
        private float PerlinNoise2D(ushort[] permutationTable, float x, float y, float zoom)
        {
            // Initialisation.
            double unit = 1.0f / Math.Sqrt(2);
            double[,] gradient2D = { { unit, unit }, { -unit, unit }, { unit, -unit }, { -unit, -unit }, { 1, 0 }, { -1, 0 }, { 0, 1 }, { 0, -1 } };
            double tmpX, tmpY;
            int coordX, coordY, maskX, maskY;
            int[] values = new int[4];
            double[] weightedValues = new double[4];
            double coefLerpX, coefLerpY, smoothX, smoothY;
            int tableSize = permutationTable.Length;

            // Récupération des positions de la grille associée à (x,y) une fois l'adaptation faite.
            x /= zoom;
            y /= zoom;
            coordX = (int)x;
            coordY = (int)y;

            // Masque "tableSize - 1" sur les coordonnées (comme un modulo "tableSize" mais ne fournit uniquement des valeurs positives).
            maskX = coordX & (tableSize - 1);
            maskY = coordY & (tableSize - 1);

            // Déterminer des valeurs du gradient2D à utiliser en désordre.
            values[0] = permutationTable[(maskX + permutationTable[maskY]) % tableSize] % 8;
            values[1] = permutationTable[(maskX + 1 + permutationTable[maskY]) % tableSize] % 8;
            values[2] = permutationTable[(maskX + permutationTable[(maskY + 1) % tableSize]) % tableSize] % 8;
            values[3] = permutationTable[(maskX + 1 + permutationTable[(maskY + 1) % tableSize]) % tableSize] % 8;

            // On récupère les valueurs et on les pondère.
            tmpX = x - coordX;
            tmpY = y - coordY;
            weightedValues[0] = gradient2D[values[0], 0] * tmpX + gradient2D[values[0], 1] * tmpY;

            tmpX = x - (coordX + 1);
            tmpY = y - coordY;
            weightedValues[1] = gradient2D[values[1], 0] * tmpX + gradient2D[values[1], 1] * tmpY;

            tmpX = x - coordX;
            tmpY = y - (coordY + 1);
            weightedValues[2] = gradient2D[values[2], 0] * tmpX + gradient2D[values[2], 1] * tmpY;

            tmpX = x - (coordX + 1);
            tmpY = y - (coordY + 1);
            weightedValues[3] = gradient2D[values[3], 0] * tmpX + gradient2D[values[3], 1] * tmpY;

            // Calcul du coefficiant d'interpolation selon X.
            tmpX = x - coordX;
            coefLerpX = 3 * tmpX * tmpX - 2 * tmpX * tmpX * tmpX;

            // Lissage des valeurs 2 à 2.
            smoothX = weightedValues[0] + coefLerpX * (weightedValues[1] - weightedValues[0]);
            smoothY = weightedValues[2] + coefLerpX * (weightedValues[3] - weightedValues[2]);

            // Calcul du coefficiant d'interpolation selon Y.
            tmpY = y - coordY;
            coefLerpY = 3 * tmpY * tmpY - 2 * tmpY * tmpY * tmpY;

            // Lissage final des deux valeurs lissées précédemment.
            return (float)(smoothX + coefLerpY * (smoothY - smoothX));

        }

        /// <summary>
        /// Génère une nouvelle table de permutation contenant des nombres entiers allant de 0 à tableSize - 1.
        /// La nouvelle table est forcément qualitative par rapport aux règles de redondance.
        /// </summary>
        /// <param name="tableSize">Taille de la table de permutation. Uniquement des puissances de 2 supérieures
        /// ou égales à 32.</param>
        /// <returns>La nouvelle table de permutation.</returns>
        private ushort[] GeneratePermutationTable(ushort tableSize)
        {
            // Initialisation globale.
            int draw;
            ushort value;
            ushort[] table = null;
            List<ushort> possibilities;

            // Vérification de la taille choisie (doit être une puissance de 2).
            float tableSizeVerification = tableSize;
            while (tableSizeVerification > 1f)
            {
                tableSizeVerification /= 2f;
            }
            if (tableSizeVerification < 1f || tableSize < TableMinimumSize)
            {
                throw new ArgumentException("La taille voulue de la table de permutation est invalide (elle doit être supérieure ou égale à 32 et être une puissance de 2.");
            }

            while (!IsValidTable(table))
            {
                // Initialisation.
                possibilities = new List<ushort>();
                table = new ushort[tableSize];
                for (int i = 0; i < tableSize; i++)
                {
                    possibilities.Add((ushort)i);
                }

                // Assignation des valeurs.
                for (int i = tableSize; i > 0; i--)
                {
                    draw = Randomizer.Random.Next(0, i);
                    value = possibilities.ElementAt(draw);
                    possibilities.RemoveAt(draw);
                    table[tableSize - i] = value;
                }
            }
            return table;
        }

        /// <summary>
        /// Teste la qualité d'une table de permutation. A tester avec minimum une taille de 64.
        /// </summary>
        /// <param name="table">Table de permutation à tester.</param>
        /// <returns>Retourne vrai si la table de permutation est de qualité suffisante.</returns>
        private bool IsValidTable(ushort[] table)
        {
            // Vérification que la table ne soit pas nulle.
            if (table == null)
            {
                return false;
            }

            // Initialisation
            int tableSize = table.Length;

            // Règle 1 : Chaque index de la table ne doit pas avoir pour valeur lui même.
            for (int i = 0; i < tableSize; i++)
            {
                if (table[i] == i)
                {
                    return false;
                }
            }

            // Règle 2 : Il ne doit pas y avoir de suite de numéro.
            int lastValue = table[tableSize - 1];
            for (int i = 0; i < tableSize; i++)
            {
                if (table[i] == lastValue - 1 || table[i] == lastValue + 1)
                {
                    return false;
                }
                lastValue = table[i];
            }
            if (table[tableSize - 1] == 0)
            {
                return false;
            }

            // Règle 3 : Il ne doit pas y avoir de cycles de valeurs en fonction des index.
            List<uint> testCycle = new List<uint>();
            testCycle.Add(table[0]);
            while (testCycle.Count < tableSize - 1)
            {
                if (testCycle.Contains(table[testCycle.Last()]))
                {
                    return false;
                }
                testCycle.Add(table[testCycle.Last()]);
            }
            if (table[testCycle.Last()] != 0)
            {
                return false;
            }

            // Règle 4 : Les différences de sauts ne doivent pas être similaires.
            int lastgap = 0;
            int currentgap = 0;
            int lastSum = 0;
            int currentSum = 0;
            for (int i = 0; i < tableSize; i++)
            {
                currentSum += table[i];
                currentgap = currentSum - lastSum;
                if (currentgap == lastgap)
                {
                    return false;
                }
                lastSum = currentSum;
                lastgap = currentgap;
            }
            return true;
        }
    }
}
