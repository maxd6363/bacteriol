﻿using Model.Core.Entity;
using Model.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace Model.Core.Player
{
    /// <summary>
    /// Implémentation basique de l'interface IJoueur.
    /// </summary>
    [DataContract(IsReference = true)]
    public class SimplePlayer : IPlayer
    {
        /// <summary>
        /// Compteur des ID des joueurs afin de s'assurer que chaque joueur possède une ID unique.
        /// </summary>
        private static int PlayerIDIncrementer = 0;

        /// <summary>
        /// Retourne l'identifiant du joueur.
        /// </summary>
        [DataMember]
        public int PlayerID { get; set; }

        /// <summary>
        /// Retourne ou modifie l'entité contrôllée par le joueur.
        /// </summary>
        [DataMember]
        public IEntity EntityControlled { get; set; }

        /// <summary>
        /// Retourne ou modifie la position de la vue du joueur.
        /// </summary>
        [DataMember]
        public Vector3 ViewPosition { get; set; }

        /// <summary>
        /// Constructeur du joueur.
        /// </summary>
        public SimplePlayer()
        {
            PlayerID = PlayerIDIncrementer;
            PlayerIDIncrementer++;
            EntityControlled = null;
            ViewPosition = AppConfig.UsedConfig.IPlayerSpawnViewPosition;
        }
    }
}
