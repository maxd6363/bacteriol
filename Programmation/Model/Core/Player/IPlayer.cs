﻿using Model.Core.Entity;
using Model.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.Player
{
    /// <summary>
    /// Interface qui définit ce qu'est un joueur.
    /// </summary>
    public interface IPlayer
    {
        /// <summary>
        /// Retourne l'identifiant du joueur.
        /// </summary>
        int PlayerID { get; }

        /// <summary>
        /// Retourne ou modifie l'entité contrôllée par le joueur.
        /// </summary>
        IEntity EntityControlled { get; set; }

        /// <summary>
        /// Retourne ou modifie la position de la vue du joueur.
        /// </summary>
        Vector3 ViewPosition { get; set; }
    }
}
