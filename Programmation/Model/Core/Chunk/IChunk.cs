﻿using Model.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.Chunk
{
    /// <summary>
    /// Interface qui définit ce qu'est un Chunk.
    /// </summary>
    public interface IChunk
    {
        /// <summary>
        /// Retourne un point du chunk (en fonction de coordonnées relative à ce dernier).
        /// <param name="relativeCoordinates">Coordonnées relatives du node dans le chunk.</param>
        /// <returns>Valeur associé au node voulu.</returns>
        /// </summary>
        float Node(Vector2 relativeCoordinates);

        /// <summary>
        /// Retourne la moyenne de tout les nodes du chunk.
        /// </summary>
        float NodesAverage { get; }

        /// <summary>
        /// Retourne la position du chunk dans le monde.
        /// </summary>
        Vector2 Position { get; }

        /// <summary>
        /// Retourne la taille du chunk.
        /// </summary>
        Vector2 Size { get; }
    }
}
