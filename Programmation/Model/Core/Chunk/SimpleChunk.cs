﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Model.Utils;

namespace Model.Core.Chunk
{
    /// <summary>
    /// Implémentation basique de l'interface IChunk.
    /// </summary>
    [DataContract(IsReference = true)]
    class SimpleChunk : IChunk
    {
        /// <summary>
        /// Retourne la moyenne des nodes du chunk.
        /// </summary>
        public float NodesAverage { get => _nodesAverage; }

        private float _nodesAverage;

        /// <summary>
        /// Retourne un point du chunk (en fonction de coordonnées relative à ce dernier).
        /// <param name="relativeCoordinates">Coordonnée</param>
        /// </summary>
        public float Node(Vector2 relativeCoordinates)
        {
            if (relativeCoordinates.X < 0 || relativeCoordinates.Y < 0 || relativeCoordinates.X >= Size.X || relativeCoordinates.Y >= Size.Y)
            {
                throw new ArgumentException($"Les coordonnées doivent se situer entre (0, 0) et ({Size.X - 1}, {Size.Y - 1}) inclus ({relativeCoordinates}).");
            }
            int x = (int)relativeCoordinates.X;
            int y = (int)relativeCoordinates.Y;
            if (relativeCoordinates.X.CompareTo(x) != 0 || relativeCoordinates.Y.CompareTo(y) != 0)
            {
                throw new ArgumentException("Les coordonnées doivent être des nombres entiers.");
            }
            return Nodes[x, y];
        }

        /// <summary>
        /// Retourne la position du chunk dans le monde.
        /// </summary>
        [DataMember]
        public Vector2 Position { get; private set; }

        /// <summary>
        /// Retourne la taille du chunk.
        /// </summary>
        [DataMember]
        public Vector2 Size { get; private set; }

        /// <summary>
        /// Retourne ou modifie la liste des points du chunk.
        /// </summary>
        private float[,] Nodes { get; set; }

        /// <summary>
        /// Tableau de substitution pour la sérialisation.
        /// </summary>
        [DataMember]
        private float[][] _serializableArray;
        
        /// <summary>
        /// Constructeur du chunk.
        /// </summary>
        /// <param name="position">Position du chunk dans le monde (doit être composée de 2 nombres entiers).</param>
        /// <param name="size">Taille du chunk (doit être composée de 2 nombres entiers).</param>
        /// <param name="nodes">Liste des points du chunk.</param>
        public SimpleChunk(Vector2 position, Vector2 size, float[,] nodes)
        {
            if (position.X.CompareTo((int)position.X) != 0 || position.Y.CompareTo((int)position.Y) != 0)
            {
                throw new ArgumentException("La position du chunk doit être composée de deux nombres entiers.");
            }
            if (size.X.CompareTo((int)size.X) != 0 || size.Y.CompareTo((int)size.Y) != 0)
            {
                throw new ArgumentException("La taille du chunk doit être composée de deux nombres entiers.");
            }
            if (size.X <= 0 || size.Y <= 0)
            {
                throw new ArgumentException("La taille du chunk ne peut pas être négative ou nulle.");
            }
            Position = position;
            Size = size;
            Nodes = nodes;
            _nodesAverage = 0;
            foreach (float value in nodes)
            {
                _nodesAverage += value;
            }
            _nodesAverage /= nodes.Length;
        }
        
        /// <summary>
        /// Appellée à la sérialisation.
        /// </summary>
        /// <param name="context">Contexte de la sérialisation.</param>
        [OnSerializing]
        private void OnSerializing(StreamingContext context)
        {
            int dimension0 = Nodes.GetLength(0);
            int dimension1 = Nodes.GetLength(1);

            _serializableArray = new float[dimension0][];
            for(int x = 0; x < dimension0; x++)
            {
                _serializableArray[x] = new float[dimension1];
                for(int y = 0; y < dimension1; y++)
                {
                    _serializableArray[x][y] = Nodes[x, y];
                }
            }
        }

        /// <summary>
        /// Appellée à la désérialisation.
        /// </summary>
        /// <param name="context">Contexte de la désérialisation.</param>
        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            if(_serializableArray == null)
            {
                Nodes = null;
            }
            else
            {
                int dim0 = _serializableArray.Length;
                int dim1 = _serializableArray[0].Length;
                if(dim0 == 0)
                {
                    Nodes = new float[0, 0];
                }
                else
                {
                    Nodes = new float[dim0, dim1];
                    for (int x = 0; x < dim0; x++)
                    {
                        for(int y = 0; y < dim1; y++)
                        {
                            Nodes[x, y] = _serializableArray[x][y];
                        }
                    }
                }
            }
        }
    }
}
