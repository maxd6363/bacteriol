﻿using Model.Core.Entity;
using Model.Core.GameEvent;
using Model.Core.Player;
using Model.Core.World;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.Specialized;
using Model.Core.Entity.Modules;
using Model.Generator;
using Model.Utils;
using System.Threading;
using Model.Timer;

namespace Model.Core.Game
{
    /// <summary>
    /// Implémentation basique de l'interface IGame.
    /// </summary>
    [DataContract]
    [KnownType(typeof(SimpleWorld))]
    [KnownType(typeof(SimplePlayer))]
    [KnownType(typeof(SimpleOilSpillGameEvent))]
    [KnownType(typeof(SimpleTimer))]
    public class SimpleGame : IGame
    {
        /// <summary>
        /// Retourne le nom de la partie.
        /// </summary>
        [DataMember]
        public string Name { get; private set; }

        /// <summary>
        /// Retourne vrai si la partie est démarré, faux sinon.
        /// </summary>
        [DataMember]
        public bool Started { get; private set; }

        /// <summary>
        /// Retourne le timer de la partie.
        /// </summary>
        [DataMember]
        public ITimer Timer { get; private set; }

        /// <summary>
        /// Retourne les différents événements qu'il est possible de déclancher pendant la partie.
        /// </summary>
        public IReadOnlyDictionary<string, IGameEvent> Events { get; private set; }

        /// <summary>
        /// Permet la sérialisation du dictionnaire d'évènements.
        /// </summary>
        [DataMember]
        private Dictionary<string, IGameEvent> _eventsSerializable { get; set; }

        /// <summary>
        /// Retourne le monde dans lequel la partie a lieu.
        /// </summary>
        [DataMember]
        public IWorld World { get; set; }

        /// <summary>
        /// Retourne le joueur hôte de la partie.
        /// </summary>
        [DataMember]
        public IPlayer Host { get; private set; }

        /// <summary>
        /// Retourne la liste des joueurs présents dans la partie.
        /// </summary>
        public IEnumerable<IPlayer> Players { get => _players; }

        /// <summary>
        /// Retourne ou modifie la liste des joueurs présents dans la partie.
        /// </summary>
        [DataMember]
        private ObservableCollection<IPlayer> _players { get; set; }

        /// <summary>
        /// Constructeur de la partie.
        /// </summary>
        /// <param name="name">Nom de la partie, doit être unique.</param>
        /// <param name="events">Evénements possibles durant la partie.</param>
        /// <param name="world">Monde sur lequel la partie aura lieu.</param>
        /// <param name="entityGenerator">Générateur d'entités de la partie.</param>
        public SimpleGame(string name, IDictionary<string, IGameEvent> events, IWorld world, ITimer timer)
        {
            Name = name;
            Started = false;
            Events = new ReadOnlyDictionary<string, IGameEvent>(events);
            World = world;
            Host = null;
            _players = new ObservableCollection<IPlayer>();
            Timer = timer;
        }

        /// <summary>
        /// Ajoute un joueur dans la partie.
        /// </summary>
        /// <param name="player">Joueur à ajouter.</param>
        public void AddPlayer(IPlayer player)
        {
            if (Host == null)
            {
                Host = player;
            }
            _players.Add(player);
        }

        /// <summary>
        /// Retire un joueur de la partie.
        /// </summary>
        /// <param name="player">Joueur à retirer.</param>
        public void RemovePlayer(IPlayer player)
        {
            if(Host != null && Host == player)
            {
                Host = null;
            }
            _players.Remove(player);
        }

        /// <summary>
        /// Appellée à la désérialisation.
        /// </summary>
        /// <param name="context">Contexte de la désérialisation.</param>
        [OnSerializing]
        private void OnSerializing(StreamingContext context)
        {
            _eventsSerializable = new Dictionary<string, IGameEvent>(Events as IDictionary<string, IGameEvent>);
        }

        /// <summary>
        /// Appellée à la désérialisation.
        /// </summary>
        /// <param name="context">Contexte de la désérialisation.</param>
        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            Events = _eventsSerializable;
        }

        /// <summary>
        /// Génère les chunks est les entités de base;
        /// </summary>
        public void Setup()
        {
            World.GetChunk(AppConfig.UsedConfig.BaseChunkCoords);
        }

        /// <summary>
        /// Démarre la partie, les entités deviennent actives.
        /// </summary>
        public void Start()
        {
            if(!Started)
            {
                Started = true;
                Timer.Start();
                Timer.OnTick += Update;
            }
        }

        /// <summary>
        /// Appellée à chaque tick. Met à jour les informations de toute la partie.
        /// <param name="sender">Auteur de l'événement.</param>
        /// <param name="timeInterval">Temps écoulé depuis le dernier tick.</param>
        /// </summary>
        private void Update(object sender, TimeSpan timeInterval)
        {
            List<IEntity> entities = new List<IEntity>();
            entities.AddRange(World.Bacterias);
            entities.AddRange(World.Euplotes);
            foreach(IEntity entity in entities)
            {
                entity.DoUpdate(timeInterval, World.WorldParameters);
            }
        }

        /// <summary>
        /// Met en pause la partie, les entités deviennent inactives.
        /// </summary>
        public void Stop()
        {
            if(Started)
            {
                Started = false;
                Timer.Stop();
                Timer.OnTick -= Update;
            }
        }

        /// <summary>
        /// Met fin à la partie, et prépare une éventuelle sérialisation.
        /// </summary>
        public void End()
        {
            Stop();
            // Autres actions de éventuelles de fin de partie.
        }
    }
}
