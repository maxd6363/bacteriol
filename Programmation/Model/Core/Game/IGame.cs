﻿using Model.Core.Entity;
using Model.Core.GameEvent;
using Model.Core.Player;
using Model.Core.World;
using Model.Generator;
using Model.Timer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.Game
{
    /// <summary>
    /// Interface qui définit ce qu'est une partie.
    /// </summary>
    public interface IGame
    {
        /// <summary>
        /// Retourne le nom de la partie.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Retourne vrai si la partie est démarré, faux sinon.
        /// </summary>
        bool Started { get; }

        /// <summary>
        /// Retourne le timer de la partie.
        /// </summary>
        ITimer Timer { get; }

        /// <summary>
        /// Retourne les différents événements qu'il est possible de déclancher pendant la partie.
        /// </summary>
        IReadOnlyDictionary<string, IGameEvent> Events { get; }

        /// <summary>
        /// Retourne le monde dans lequel la partie a lieu.
        /// </summary>
        IWorld World { get; }

        /// <summary>
        /// Retourne le joueur hôte de la partie.
        /// </summary>
        IPlayer Host { get; }

        /// <summary>
        /// Retourne la liste des joueurs présents dans la partie.
        /// </summary>
        IEnumerable<IPlayer> Players { get; }

        /// <summary>
        /// Ajoute un joueur dans la partie.
        /// </summary>
        /// <param name="player">Joueur à ajouter.</param>
        void AddPlayer(IPlayer player);

        /// <summary>
        /// Retire un joueur de la partie.
        /// </summary>
        /// <param name="player">Joueur à retirer.</param>
        void RemovePlayer(IPlayer player);

        /// <summary>
        /// Génère les chunks est les entités de base;
        /// </summary>
        void Setup();

        /// <summary>
        /// Démarre la partie, les entités deviennent actives.
        /// </summary>
        void Start();

        /// <summary>
        /// Met en pause la partie, les entités deviennent inactives.
        /// </summary>
        void Stop();

        /// <summary>
        /// Met fin à la partie, et prépare une éventuelle sérialisation.
        /// </summary>
        void End();

    }
}
