﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.GameEvent
{
    /// <summary>
    /// Classe utilisée pour faire passer des informations lorsqu'un GameEvent est déclanché.
    /// </summary>
    public class GameEventArgs : EventArgs
    {
        /// <summary>
        /// Retourne l'intensité de l'événement de jeu.
        /// </summary>
        public int Intensity { get; }

        /// <summary>
        /// Constructeur unique de GameEventArgs.
        /// </summary>
        /// <param name="intensity">Intensité de l'événement de jeu.</param>
        public GameEventArgs(int intensity)
        {
            Intensity = intensity;
        }
    }
}
