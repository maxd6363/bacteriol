﻿using Model.Core.Game;
using Model.Utils;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Model.Core.GameEvent
{
    [DataContract(IsReference = true)] 
    public class SimpleOilSpillGameEvent : IGameEvent
    {
        /// <summary>
        /// Retourne la partie associée à l'événement.
        /// </summary>
        //[DataMember]
        public IGame Game { get; private set; }

        /// <summary>
        /// Déclanche l'événement sur la partie, et applique les effets voulus.
        /// </summary>
        /// <param name="intensity">Intensité de l'événement.</param>
        public void Trigger(int intensity)
        {
            Game.World.WorldParameters.PollutionLevel += AppConfig.UsedConfig.OilSpillGameEventCoef * intensity;
        }

        /// <summary>
        /// Déclanché quand la méthode Trigger est appellée.
        /// </summary>
        public event EventHandler<GameEventArgs> OnTrigger;
    }
}
