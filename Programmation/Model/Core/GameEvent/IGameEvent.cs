﻿using Model.Core.Game;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.GameEvent
{
    /// <summary>
    /// Interface qui définit ce qu'est un événement en jeu.
    /// </summary>
    public interface IGameEvent
    {
        /// <summary>
        /// Retourne la partie associée à l'événement.
        /// </summary>
        IGame Game { get; }

        /// <summary>
        /// Déclanche l'événement sur la partie, et applique les effets voulus.
        /// </summary>
        /// <param name="intensity">Intensité de l'événement.</param>
        void Trigger(int intensity);

        /// <summary>
        /// Déclanché quand la méthode Trigger est appellée.
        /// </summary>
        event EventHandler<GameEventArgs> OnTrigger;
    }
}
