﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Utils;
using Model.Core.Entity.Modules;
using System.Runtime.Serialization;
using Model.Core.World;

namespace Model.Core.Entity
{

    [DataContract(IsReference = true)]
    [KnownType(typeof(SimpleMove))]
    [KnownType(typeof(EuploteDie))]
    [KnownType(typeof(SimplePredator))]
    [KnownType(typeof(SimpleFood))]
    [KnownType(typeof(SimpleReproduce))]
    public class SimpleEuplote : Euplote
    {
        /// <summary>
        /// Retourne vrai si l'entité est en vie, faux si elle est morte.
        /// </summary>
        [DataMember]
        public override bool Alive { get; set; }

        /// <summary>
        /// Retourne ou modifie la vie de l'entité.
        /// </summary>
        [DataMember]
        public override float Health
        {
            get
            {
                return _health;
            }
            set
            {
                _health = value;
                if (_health < 0)
                {
                    _health = 0;
                }
                OnHealthModified?.Invoke(this, Health);
            }
        }
        
        private float _health;

        /// <summary>
        /// Retourne la vie maximale de l'entité.
        /// </summary>
        [DataMember]
        public override float MaxHealth { get; protected set; }

        /// <summary>
        /// Retourne ou modifie la taille de l'entité.
        /// </summary>
        [DataMember]
        public override float Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
                if (_size < 0)
                {
                    _size = 0;
                }
                OnSizeModified?.Invoke(this, Health);
            }
        }
        
        private float _size;

        /// <summary>
        /// Retourne ou modifie la location de l'entité.
        /// </summary>
        [DataMember]
        public override Vector2 Location { get; set; }

        /// <summary>
        /// Retourne ou modifie la direction du regard d'une entité,
        /// sous forme d'un vecteur qui est automatiquement normalisé.
        /// </summary>
        [DataMember]
        public override Vector2 LookingDirection { get; set; }

        /// <summary>
        /// Retourne le module en charge de la mort de l'entité.
        /// </summary>
        [DataMember]
        public override IDie DieModule
        {
            get
            {
                return _dieModule;
            }
            protected set
            {
                _dieModule = value;
                DieModule.OnDie += DieModule_OnDie;
            }
        }
        private IDie _dieModule;

        /// <summary>
        /// Retourne le module en charge du fait que l'entité est une proie.
        /// </summary>
        [DataMember]
        public override IFood FoodModule
        {
            get
            {
                return _foodModule;
            }
            protected set
            {
                _foodModule = value;
                FoodModule.OnEaten += FoodModule_OnEaten;
            }
        }
        private IFood _foodModule;

        /// <summary>
        /// Retourne le module en charge des déplacements de l'entité.
        /// </summary>
        [DataMember]
        public override IMove MoveModule { get; set; }


        /// <summary>
        /// Retourne le module en charge de la reproduction de l'entité.
        /// </summary>
        [DataMember]
        public override IReproduce ReproduceModule { get; protected set; }

        /// <summary>
        /// Retourne le module en charge du caractère de prédateur de l'entité.
        /// </summary>
        [DataMember]
        public override IPredator PredatorModule { get; set; }

        /// <summary>
        /// Constructeur par recopie.
        /// </summary>
        /// <param name="euplote">Euplote à cloner.</param>
        public SimpleEuplote(SimpleEuplote euplote)
        {
            Alive = euplote.Alive;
            Health = euplote.Health;
            MaxHealth = euplote.MaxHealth;
            Size = euplote.Size;
            Location = euplote.Location;
            LookingDirection = euplote.LookingDirection;
            DieModule = euplote.DieModule.Clone() as IDie;
            FoodModule = euplote.FoodModule.Clone() as IFood;
            MoveModule = euplote.MoveModule.Clone() as IMove;
            ReproduceModule = euplote.ReproduceModule.Clone() as IReproduce;
            PredatorModule = euplote.PredatorModule.Clone() as IPredator;
        }

        /// <summary>
        /// Constructeur de l'euplote.
        /// </summary>
        /// <param name="location">Location de l'euplote.</param>
        /// <param name="die">Module de mort.</param>
        /// <param name="food">Module de proie.</param>
        /// <param name="move">Module de déplacement.</param>
        /// <param name="reproduce">Module de reproduction.</param>
        /// <param name="predator">Module de prédation.</param>
        public SimpleEuplote(Vector2 location, IDie die, IFood food, IMove move, IReproduce reproduce, IPredator predator)
        {
            AppConfig config = AppConfig.UsedConfig;
            Alive = true;
            MaxHealth = config.EuploteBaseMaxHealth;
            Health = MaxHealth;
            Size = config.EuploteBaseSize;
            Location = location;
            LookingDirection = Vector2.Normalize(new Vector2(Randomizer.Random.NextDouble(), Randomizer.Random.NextDouble()));
            DieModule = die;
            FoodModule = food;
            MoveModule = move;
            ReproduceModule = reproduce;
            PredatorModule = predator;
            DieModule.Entity = this;
            FoodModule.Entity = this;
            MoveModule.Entity = this;
            ReproduceModule.Entity = this;
            PredatorModule.Entity = this;
        }

        /// <summary>
        /// Clone l'euplote.
        /// </summary>
        /// <returns>Clone de l'euplote.</returns>
        public override object Clone()
        {
            return new SimpleEuplote(this);
        }

        /// <summary>
        /// Actualise l'entité : elle effectue toutes ses actions pour un tick.
        /// </summary>
        /// <param name="gameTime">Durée réprésentée par le tick.</param>
        /// <param name="worldParameters">Paramètres de l'environnement de l'entité.</param>
        public override void DoUpdate(TimeSpan gameTime, IWorldParameters worldParameters)
        {
            if (!Alive) return;
            // 1) Evoluer en fonction des WorldParameters.
            // - Modifier l'entité en fonction des paramètres du monde.

            // 2) Predator
            PredatorModule.EatTargetIfPossible();

            // 3) Die
            DieModule.CheckIfMustDie();
            if (!Alive) return;

            // 4) Move
            MoveModule.Move();

            // 5) Reproduce
            ReproduceModule.TryReproduce();
        }

        /// <summary>
        /// Déclanché à chaque modification de la vie.
        /// </summary>
        public override event EventHandler<float> OnHealthModified;

        /// <summary>
        /// Déclanché à chaque modification de la taille.
        /// </summary>
        public override event EventHandler<float> OnSizeModified;
    }
}
