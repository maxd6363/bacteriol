﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Utils;
using Model.Core.Entity.Modules;
using System.Runtime.Serialization;
using Model.Core.World;

namespace Model.Core.Entity
{

    [DataContract(IsReference = true)]
    [KnownType(typeof(SimpleMove))]
    [KnownType(typeof(SimpleHealthDie))]
    [KnownType(typeof(SimpleInfectable))]
    [KnownType(typeof(SimpleFood))]
    [KnownType(typeof(SimpleReproduce))]
    public class SimpleBacteria : Bacteria
    {
        /// <summary>
        /// Retourne vrai si l'entité est en vie, faux si elle est morte.
        /// </summary>
        [DataMember]
        public override bool Alive { get; set; }

        /// <summary>
        /// Retourne ou modifie la vie de l'entité.
        /// </summary>
        [DataMember]
        public override float Health
        {
            get
            {
                return _health;
            }
            set
            {
                _health = value;
                if (_health < 0)
                {
                    _health = 0;
                }
                OnHealthModified?.Invoke(this, Health);
            }
        }
        
        private float _health;

        /// <summary>
        /// Retourne la vie maximale de l'entité.
        /// </summary>
        [DataMember]
        public override float MaxHealth { get; protected set; }

        /// <summary>
        /// Retourne ou modifie la taille de l'entité.
        /// </summary>
        [DataMember]
        public override float Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
                if (_size < 0)
                {
                    _size = 0;
                }
                OnSizeModified?.Invoke(this, Health);
            }
        }
        
        private float _size;

        /// <summary>
        /// Retourne ou modifie la location de l'entité.
        /// </summary>
        [DataMember]
        public override Vector2 Location { get; set; }

        /// <summary>
        /// Retourne ou modifie la direction du regard d'une entité,
        /// sous forme d'un vecteur qui est automatiquement normalisé.
        /// </summary>
        [DataMember]
        public override Vector2 LookingDirection { get; set; }

        /// <summary>
        /// Retourne le module en charge de la mort de l'entité.
        /// </summary>
        [DataMember]
        public override IDie DieModule
        {
            get
            {
                return _dieModule;
            }
            protected set
            {
                _dieModule = value;
                DieModule.OnDie += DieModule_OnDie;
            }
        }
        private IDie _dieModule;

        /// <summary>
        /// Retourne le module en charge du fait que l'entité est une proie.
        /// </summary>
        [DataMember]
        public override IFood FoodModule
        {
            get
            {
                return _foodModule;
            }
            protected set
            {
                _foodModule = value;
                FoodModule.OnEaten += FoodModule_OnEaten;
            }
        }
        private IFood _foodModule;

        /// <summary>
        /// Retourne le module en charge des déplacements de l'entité.
        /// </summary>
        [DataMember]
        public override IMove MoveModule { get; protected set; }

        /// <summary>
        /// Retourne le module en charge de la reproduction de l'entité.
        /// </summary>
        [DataMember]
        public override IReproduce ReproduceModule { get; protected set; }

        /// <summary>
        /// Retourne le module en charge du fait que l'entité peut être inféctée.
        /// </summary>
        [DataMember]
        public override IInfectable InfectableModule { get; protected set; }

        /// <summary>
        /// Constructeur par recopie.
        /// </summary>
        /// <param name="bacteria">bacterie à cloner.</param>
        public SimpleBacteria(SimpleBacteria bacteria)
        {
            Alive = bacteria.Alive;
            Health = bacteria.Health;
            MaxHealth = bacteria.MaxHealth;
            Size = bacteria.Size;
            Location = bacteria.Location;
            LookingDirection = bacteria.LookingDirection;
            DieModule = bacteria.DieModule.Clone() as IDie;
            FoodModule = bacteria.FoodModule.Clone() as IFood;
            MoveModule = bacteria.MoveModule.Clone() as IMove;
            ReproduceModule = bacteria.ReproduceModule.Clone() as IReproduce;
            InfectableModule = bacteria.InfectableModule.Clone() as IInfectable;
        }

        /// <summary>
        /// Constructeur de l'bacteria.
        /// </summary>
        /// <param name="location">Location de la bacterie.</param>
        /// <param name="die">Module de mort.</param>
        /// <param name="food">Module de proie.</param>
        /// <param name="move">Module de déplacement.</param>
        /// <param name="reproduce">Module de reproduction.</param>
        /// <param name="infectable">Module d'infection.</param>
        public SimpleBacteria(Vector2 location, IDie die, IFood food, IMove move, IReproduce reproduce, IInfectable infectable)
        {
            AppConfig config = AppConfig.UsedConfig;
            Alive = true;
            MaxHealth = config.BacteriaBaseMaxHealth;
            Health = MaxHealth;
            Size = config.BacteriaBaseSize;
            Location = location;
            LookingDirection = Vector2.Normalize(new Vector2(Randomizer.Random.NextDouble(), Randomizer.Random.NextDouble()));
            DieModule = die;
            FoodModule = food;
            MoveModule = move;
            ReproduceModule = reproduce;
            InfectableModule = infectable;
            DieModule.Entity = this;
            FoodModule.Entity = this;
            MoveModule.Entity = this;
            ReproduceModule.Entity = this;
            InfectableModule.Entity = this;
        }
        /// <summary>
        /// Clone la bactérie.
        /// </summary>
        /// <returns>Clone de la bactérie.</returns>
        public override object Clone()
        {
            return new SimpleBacteria(this);          
        }

        /// <summary>
        /// Actualise l'entité : elle effectue toutes ses actions pour un tick.
        /// </summary>
        /// <param name="gameTime">Durée réprésentée par le tick.</param>
        /// <param name="worldParameters">Paramètres de l'environnement de l'entité.</param>
        public override void DoUpdate(TimeSpan gameTime, IWorldParameters worldParameters)
        {
            if (!Alive) return;
            // 1) Evoluer en fonction des WorldParameters.
            // - Modifier l'entité en fonction des paramètres du monde.

            // 2) Infectable
            InfectableModule.DrawInfected();
            InfectableModule.BeInfected();

            // 3) Die
            DieModule.CheckIfMustDie();
            if (!Alive) return;

            // 4) Move
            MoveModule.Move();

            // 5) Reproduce
            ReproduceModule.TryReproduce();
        }

        /// <summary>
        /// Déclanché à chaque modification de la vie.
        /// </summary>
        public override event EventHandler<float> OnHealthModified;

        /// <summary>
        /// Déclanché à chaque modification de la taille.
        /// </summary>
        public override event EventHandler<float> OnSizeModified;
    }

}
