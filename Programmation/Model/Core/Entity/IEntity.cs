﻿using Model.Core.World;
using Model.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.Entity
{
    /// <summary>
    /// Interface qui définit ce qu'est une entité.
    /// </summary>
    public interface IEntity : ICloneable
    {
        /// <summary>
        /// Retourne vrai si l'entité est en vie, faux si elle est morte.
        /// </summary>
        bool Alive { get; set; }

        /// <summary>
        /// Retourne ou modifie la vie de l'entité.
        /// </summary>
        float Health { get; set; }

        /// <summary>
        /// Retourne la vie maximale de l'entité.
        /// </summary>
        float MaxHealth { get; }

        /// <summary>
        /// Retourne ou modifie la taille de l'entité.
        /// </summary>
        float Size { get; set; }

        /// <summary>
        /// Retourne ou modifie la location de l'entité.
        /// </summary>
        Vector2 Location { get; set; }

        /// <summary>
        /// Retourne ou modifie la direction du regard d'une entité,
        /// sous forme d'un vecteur qui est automatiquement normalisé.
        /// </summary>
        Vector2 LookingDirection { get; set; }

        /// <summary>
        /// Actualise l'entité : elle effectue toutes ses actions pour un tick.
        /// </summary>
        /// <param name="gameTime">Durée réprésentée par le tick.</param>
        /// <param name="worldParameters">Paramètres de l'environnement de l'entité.</param>
        void DoUpdate(TimeSpan gameTime, IWorldParameters worldParameters);

        /// <summary>
        /// Déclanché à chaque modification de la vie.
        /// </summary>
        event EventHandler<float> OnHealthModified;

        /// <summary>
        /// Déclanché à chaque modification de la taille.
        /// </summary>
        event EventHandler<float> OnSizeModified;
    }
}
