﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Model.Core.Entity.Modules;
using Model.Core.World;
using Model.Utils;

namespace Model.Core.Entity
{
    /// <summary>
    /// Classe abstraite qui définit ce qu'est une bactérie.
    /// </summary>
    [DataContract(IsReference = true)]
    [KnownType(typeof(SimpleMove))]
    [KnownType(typeof(SimpleHealthDie))]
    [KnownType(typeof(SimpleInfectable))]
    [KnownType(typeof(SimpleFood))]
    [KnownType(typeof(SimpleReproduce))]
    public abstract class Bacteria : IEntity
    { 
        /// <summary>
        /// Nom du type Bacteria.
        /// </summary>
        public const string TypeName = "Bacteria";

        /// <summary>
        /// Retourne vrai si l'entité est en vie, faux si elle est morte.
        /// </summary>
        [DataMember]
        public abstract bool Alive { get; set; }

        /// <summary>
        /// Retourne ou modifie la vie de l'entité.
        /// </summary>
        [DataMember]
        public abstract float Health { get; set; }

        /// <summary>
        /// Retourne la vie maximale de l'entité.
        /// </summary>
        [DataMember]
        public abstract float MaxHealth { get; protected set; }

        /// <summary>
        /// Retourne ou modifie la taille de l'entité.
        /// </summary>
        [DataMember]
        public abstract float Size { get; set; }

        /// <summary>
        /// Retourne ou modifie la location de l'entité.
        /// </summary>
        [DataMember]
        public abstract Vector2 Location { get; set; }

        /// <summary>
        /// Retourne ou modifie la direction du regard d'une entité,
        /// sous forme d'un vecteur qui est automatiquement normalisé.
        /// </summary>
        [DataMember]
        public abstract Vector2 LookingDirection { get; set; }

        /// <summary>
        /// Retourne le module en charge de la mort de l'entité.
        /// </summary>
        [DataMember]
        public abstract IDie DieModule { get; protected set; }

        /// <summary>
        /// Retourne le module en charge du fait que l'entité est une proie.
        /// </summary>
        [DataMember]
        public abstract IFood FoodModule { get; protected set; }

        /// <summary>
        /// Retourne le module en charge des déplacements de l'entité.
        /// </summary>
        [DataMember]
        public abstract IMove MoveModule { get; protected set; }

        /// <summary>
        /// Retourne le module en charge de la reproduction de l'entité.
        /// </summary>
        [DataMember]
        public abstract IReproduce ReproduceModule { get; protected set; }

        /// <summary>
        /// Retourne le module en charge du fait que l'entité peut être inféctée.
        /// </summary>
        [DataMember]
        public abstract IInfectable InfectableModule { get; protected set; }

        /// <summary>
        /// Clone la bactérie.
        /// </summary>
        /// <returns>Clone de la bactérie.</returns>
        public abstract object Clone();

        /// <summary>
        /// Actualise l'entité : elle effectue toutes ses actions pour un tick.
        /// </summary>
        /// <param name="gameTime">Durée réprésentée par le tick.</param>
        /// <param name="worldParameters">Paramètres de l'environnement de l'entité.</param>
        public abstract void DoUpdate(TimeSpan gameTime, IWorldParameters worldParameters);

        /// <summary>
        /// Appellée lors de la mort de l'euplote.
        /// <param name="sender">Module de proie.</param>
        /// <param name="eventArgs">Arguments de l'événement (vide).</param>
        /// </summary>
        protected void DieModule_OnDie(object sender, EventArgs eventArgs)
        {
            Alive = false;
        }

        /// <summary>
        /// Appellée lorsque l'euplote est mangée (et donc juste avant sa mort).
        /// <param name="sender">Module de proie.</param>
        /// <param name="eventArgs">Arguments de l'événement (contient le prédateur et la quantité de nourriture qui lui est donnée).</param>
        /// </summary>
        protected void FoodModule_OnEaten(object sender, OnEatenEventArgs eventArgs)
        {
            (sender as IFood).Entity.Health = 0;
            eventArgs.Eater.ReceiveNutriment(eventArgs.NutrimentQuantity);
        }

        /// <summary>
        /// Déclanché à chaque modification de la vie.
        /// </summary>
        public abstract event EventHandler<float> OnHealthModified;

        /// <summary>
        /// Déclanché à chaque modification de la taille.
        /// </summary>
        public abstract event EventHandler<float> OnSizeModified;
    }
}
