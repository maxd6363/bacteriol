﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Model.Core.Entity.Modules
{


    /// <summary>
    /// Implémentation basique du module de proie.
    /// </summary>
    [DataContract(IsReference = true)]
    class SimpleFood : IFood
    {
        /// <summary>
        /// Entité concernée par le module.
        /// </summary>
        [DataMember]
        public IEntity Entity
        {
            get
            {
                return entity;
            }
            set
            {
                if (entity != null)
                {
                    throw new AccessViolationException("Vous ne pouvez pas modifier l'entité d'un module une fois que ce dernier en a une.");
                }
                entity = value;
            }
        }
        private IEntity entity;

        /// <summary>
        /// Constructeur par recopie du module.
        /// </summary>
        /// <param name="module">Module à cloner.</param>
        public SimpleFood(SimpleFood module)
        {
            Entity = module.Entity;
        }

        /// <summary>
        /// Constructeur du module.
        /// </summary>
        public SimpleFood(){}

        /// <summary>
        /// Invoque l'événement onEaten et calcule la quantité de nourriture à retourner.
        /// <param name="predator">Prédateur l'ayant mangé.</param>
        /// </summary>
        public void GetEaten(IPredator predator)
        {

            OnEaten?.Invoke(this, new OnEatenEventArgs(Entity.Size, predator));
        }

        /// <summary>
        /// Clone le module de proie.
        /// </summary>
        /// <returns>Clone du module.</returns>
        public object Clone()
        {
            return new SimpleFood(this);
        }

        /// <summary>
        /// Déclanché quand l'entité doit être mangée et retourne la quantité de nourriture donnée.
        /// </summary>
        public event EventHandler<OnEatenEventArgs> OnEaten;
    }
}
