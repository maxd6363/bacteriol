﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.Entity.Modules
{
    /// <summary>
    /// Interface mère de tout module de proie.
    /// </summary>
    public interface IFood : ICloneable, IEntityModule
    {

        /// <summary>
        /// Invoque l'événement onEaten et calcule la quantité de nourriture à retourner.
        /// <param name="predator">Prédateur l'ayant mangé.</param>
        /// </summary>
        void GetEaten(IPredator predator);

        /// <summary>
        /// Déclanché quand l'entité doit être mangée et retourne la quantité de nourriture donnée.
        /// </summary>
        event EventHandler<OnEatenEventArgs> OnEaten;
    }
}
