﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.Entity.Modules
{
    /// <summary>
    /// Classe utilisée pour faire passer des informations lorsqu'un événement OnEaten est déclanché.
    /// </summary>
    public class OnEatenEventArgs : EventArgs
    {
        /// <summary>
        /// Retourne la quantité de nourriture donnée au prédateur.
        /// </summary>
        public float NutrimentQuantity { get; }

        /// <summary>
        /// Retourne le prédateur ayant mangé l'entité.
        /// </summary>
        public IPredator Eater { get; }

        /// <summary>
        /// Constructeur unique de OnEatenEventArgs
        /// </summary>
        /// <param name="nutrimentQuantity">Quantité de nourriture donnée au prédateur.</param>
        /// <param name="eater">Prédateur ayant mangé l'entité.</param>
        public OnEatenEventArgs(float nutrimentQuantity, IPredator eater)
        {
            NutrimentQuantity = nutrimentQuantity;
            Eater = eater;
        }
    }
}
