﻿using Model.Core.World;
using Model.Utils;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Model.Core.Entity.Modules
{
    /// <summary>
    /// Implémentation basique du module de prédation des euplotes.
    /// </summary>
    [DataContract(IsReference = true)]
    class SimplePredator : IPredator
    {
        /// <summary>
        /// Entité concernée par le module.
        /// </summary>
        [DataMember]
        public IEntity Entity
        {
            get
            {
                return entity;
            }
            set
            {
                if (entity != null)
                {
                    throw new AccessViolationException("Vous ne pouvez pas modifier l'entité d'un module une fois que ce dernier en a une.");
                }
                entity = value;
                SearchNewTarget();
            }
        }
        private IEntity entity;

        /// <summary>
        /// Retourne la cible de l'entité concernée.
        /// </summary>
        [DataMember]
        public IFood Target { get; private set; }

        /// <summary>
        /// Retourne le monde dans lequel le module va agir.
        /// </summary>
        [DataMember]
        public IWorld World { get; private set; }

        /// <summary>
        /// Constructeur par recopie du module.
        /// </summary>
        /// <param name="module">Module à cloner.</param>
        public SimplePredator(SimplePredator module)
        {
            Entity = module.Entity;
            Target = module.Target;
            World = module.World;
        }


        /// <summary>
        /// Constructeur du module.
        /// </summary>
        /// <param name="world">Monde dans lequel le module va agir.</param>
        public SimplePredator(IWorld world)
        {
            World = world;
        }

        /// <summary>
        /// Appellée dès que la cible meurt, permet de choisir quelle est la nouvelle cible.
        /// </summary>
        public void SearchNewTarget()
        {
            if (Target == null || !Target.Entity.Alive)
            {
                Tuple<IFood, float> favoriteTarget = null;
                foreach (Bacteria bacteria in World.Bacterias)
                {
                    float distance = (float)Vector2.Distance(bacteria.Location, Entity.Location);
                    if (favoriteTarget == null || distance < favoriteTarget.Item2)
                    {
                        favoriteTarget = Tuple.Create(bacteria.FoodModule, distance);
                    }
                }
                if (favoriteTarget == null)
                {
                    foreach (Euplote euplote in World.Euplotes)
                    {
                        float distance = (float)Vector2.Distance(euplote.Location, Entity.Location);
                        if (favoriteTarget == null || distance < favoriteTarget.Item2)
                        {
                            favoriteTarget = Tuple.Create(euplote.FoodModule, distance);
                        }
                    }
                }
                if (favoriteTarget == null)
                {
                    Entity.LookingDirection = Vector2.Zero;
                }
                else
                {
                    Entity.LookingDirection = Vector2.Normalize(favoriteTarget.Item1.Entity.Location - Entity.Location);
                }
            }
        }

        /// <summary>
        /// Appellée à chaque tick, permet à l'entité de manger sa cible.
        /// </summary>
        public void EatTargetIfPossible()
        {
            Entity.LookingDirection = Vector2.Normalize(Target.Entity.Location - Entity.Location);
            if (Vector2.Distance(Entity.Location, Target.Entity.Location) < AppConfig.UsedConfig.MaxDistanceWithTargetToEat)
            {
                Target.GetEaten(this);
                Target = null;
            }
        }

        /// <summary>
        /// Appellée lorsque l'entité en mange une autre, elle reçoit alors de la nourriture pour se développer.
        /// </summary>
        /// <param name="nutrimentQuantity">Quantité de nourriture reçue.</param>
        public void ReceiveNutriment(float nutrimentQuantity)
        {
            Entity.Size += nutrimentQuantity;
            Entity.Health += nutrimentQuantity;
        }

        /// <summary>
        /// Clone le module de prédation.
        /// </summary>
        /// <returns>Clone du module.</returns>
        public object Clone()
        {
            return new SimplePredator(this);
        }
    }
}
