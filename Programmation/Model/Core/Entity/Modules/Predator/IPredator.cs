﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.Entity.Modules
{
    /// <summary>
    /// Interface mère de tout module de prédation.
    /// </summary>
    public interface IPredator : ICloneable, IEntityModule
    {
        /// <summary>
        /// Retourne la cible de l'entité concernée.
        /// </summary>
        IFood Target { get; }

        /// <summary>
        /// Appellée dès que la cible meurt, permet de choisir quelle est la nouvelle cible.
        /// </summary>
        void SearchNewTarget();

        /// <summary>
        /// Appellée à chaque tick, permet à l'entité de manger sa cible.
        /// </summary>
        void EatTargetIfPossible();

        /// <summary>
        /// Appellée lorsque l'entité en mange une autre, elle reçoit alors de la nourriture pour se développer.
        /// </summary>
        /// <param name="nutrimentQuantity">Quantité de nourriture reçue.</param>
        void ReceiveNutriment(float nutrimentQuantity);
    }
}
