﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Model.Core.Entity.Modules
{
    /// <summary>
    /// Implémentation basique du module de mort correspondant à une entité qui est mangée par une autre.
    /// </summary>
    [DataContract(IsReference = true)]
    class SimpleHealthDie : IDie
    {
        /// <summary>
        /// Entité concernée par le module.
        /// </summary>
        [DataMember]
        public IEntity Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                if (_entity != null)
                {
                    throw new AccessViolationException("Vous ne pouvez pas modifier l'entité d'un module une fois que ce dernier en a une.");
                }
                _entity = value;
                Entity.OnHealthModified += Entity_OnHealthModified;
            }
        }
        private IEntity _entity;

        /// <summary>
        /// Constructeur par recopie du module.
        /// </summary>
        /// <param name="module">Module à cloner.</param>
        public SimpleHealthDie(SimpleHealthDie module)
        {
            Entity = module.Entity;
            Entity.OnHealthModified += Entity_OnHealthModified;
        }

        /// <summary>
        /// Constructeur du module.
        /// </summary>
        public SimpleHealthDie(){}

        /// <summary>
        /// Appellée quand la vie de l'entité change.
        /// </summary>
        /// <param name="sender">Entité concernée.</param>
        /// <param name="health">Nouvelle vitalité de l'entité.</param>
        private void Entity_OnHealthModified(object sender, float health)
        {
            CheckIfMustDie();
        }

        /// <summary>
        /// Vérifie si l'entité doit mourrir en regardant sa vie.
        /// </summary>
        public void CheckIfMustDie()
        {
            if (Entity.Health <= 0)
            {
                Entity.Alive = false;
                OnDie?.Invoke(this, null);
            }
        }

        /// <summary>
        /// Clone le module de mort.
        /// </summary>
        /// <returns>Clone du module.</returns>
        public object Clone()
        {
            return new SimpleHealthDie(this);
        }

        /// <summary>
        /// Déclanché quand l'entité doit mourrir.
        /// </summary>
        public event EventHandler OnDie;
    }
}
