﻿using Model.Factory;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Text;

namespace Model.Core.Entity.Modules
{
    /// <summary>
    /// Implémentation du module de mort spécialisée pour les Euplotes.
    /// </summary>
    [DataContract(IsReference = true)]
    class EuploteDie : IDie
    {
        /// <summary>
        /// Entité concernée par le module.
        /// </summary>
        [DataMember]
        public IEntity Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                if (_entity != null)
                {
                    throw new AccessViolationException("Vous ne pouvez pas modifier l'entité d'un module une fois que ce dernier en a une.");
                }
                _entity = value;
                HealthDie.Entity = value;
                SizeDie.Entity = value;
            }
        }
        private IEntity _entity;

        /// <summary>
        /// Retourne ou modifie le module testant la vie de l'entité.
        /// </summary>
        private SimpleHealthDie HealthDie
        {
            get
            {
                return _healthDie;
            }
            set
            {
                if (_healthDie != null)
                {
                    throw new AccessViolationException("Vous ne pouvez pas modifier un composant déjà défini de ce module.");
                }
                _healthDie = value;
                HealthDie.OnDie += Component_OnDie;
            }
        }
        private SimpleHealthDie _healthDie;

        /// <summary>
        /// Retourne ou modifie le module testant la taille de l'entité.
        /// </summary>
        private SimpleSizeDie SizeDie
        {
            get
            {
                return _sizeDie;
            }
            set
            {
                if (_sizeDie != null)
                {
                    throw new AccessViolationException("Vous ne pouvez pas modifier un composant déjà défini de ce module.");
                }
                _sizeDie = value;
                SizeDie.OnDie += Component_OnDie;
            }
        }
        private SimpleSizeDie _sizeDie;


        /// <summary>
        /// Constructeur par recopie du module.
        /// </summary>
        /// <param name="module">Module à cloner.</param>
        public EuploteDie(EuploteDie module)
        {
            HealthDie = new SimpleHealthDie(module.HealthDie);
            SizeDie = new SimpleSizeDie(module.SizeDie);
            Entity = module.Entity;
        }

        /// <summary>
        /// Constructeur du module.
        /// </summary>
        /// <param name="healthDie">Module testant la vie de l'entité.</param>
        /// <param name="sizeDie">Module testant la taille de l'entité.</param>
        public EuploteDie(SimpleHealthDie healthDie, SimpleSizeDie sizeDie)
        {
            HealthDie = healthDie;
            SizeDie = sizeDie;
        }

        /// <summary>
        /// Vérifie si l'entité doit mourrir en regardant sa vie.
        /// </summary>
        public void CheckIfMustDie()
        {
            HealthDie.CheckIfMustDie();
            SizeDie.CheckIfMustDie();
        }

        /// <summary>
        /// Clone le module de mort.
        /// </summary>
        /// <returns>Clone du module.</returns>
        public object Clone()
        {
            return new EuploteDie(this);
        }

        /// <summary>
        /// Appellée quand l'entité meurt.
        /// </summary>
        /// <param name="sender">Entité concernée.</param>
        /// <param name="e">Argument de l'événement.</param>
        private void Component_OnDie(object sender, EventArgs e)
        {
            OnDie?.Invoke(this, null);
        }

        /// <summary>
        /// Déclanché quand l'entité doit mourrir.
        /// </summary>
        public event EventHandler OnDie;

        /// <summary>
        /// Permet, avant la déserialisation, d'instancier les attributs HealthDie et SizeDie
        /// </summary>
        /// <param name="context">context de la sérialisation</param>
        [OnDeserializing]
        private void OnDeserializing(StreamingContext context)
        {
            HealthDie = new SimpleHealthDie();
            SizeDie = new SimpleSizeDie();
        }
    }

}
