﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.Entity.Modules
{
    /// <summary>
    /// Interface mère de tout module de mort.
    /// </summary>
    public interface IDie : IEntityModule, ICloneable
    {
        /// <summary>
        /// Vérifie si l'entité doit mourrir.
        /// </summary>
        void CheckIfMustDie();


        /// <summary>
        /// Déclanché quand l'entité doit mourrir.
        /// </summary>
        event EventHandler OnDie;
    }
}
