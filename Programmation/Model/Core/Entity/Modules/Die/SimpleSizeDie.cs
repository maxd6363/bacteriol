﻿using Model.Utils;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Model.Core.Entity.Modules
{

    /// <summary>
    /// Implémentation basique du module de mort correspondant à la mort d'un euplote par explosion (trop grande taille).
    /// </summary>
    [DataContract(IsReference = true)]
    class SimpleSizeDie : IDie
    {
        /// <summary>
        /// Entité concernée par le module.
        /// </summary>
        [DataMember]
        public IEntity Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                if (_entity != null)
                {
                    throw new AccessViolationException("Vous ne pouvez pas modifier l'entité d'un module une fois que ce dernier en a une.");
                }
                _entity = value;
                Entity.OnSizeModified += Entity_OnSizeModified;
            }
        }
        private IEntity _entity;


        /// <summary>
        /// Constructeur par recopie du module.
        /// </summary>
        /// <param name="module">Module à cloner.</param>
        public SimpleSizeDie(SimpleSizeDie module)
        {
            Entity = module.Entity;
            Entity.OnSizeModified += Entity_OnSizeModified;
        }

        /// <summary>
        /// Constructeur du module.
        /// </summary>
        public SimpleSizeDie(){}

        /// <summary>
        /// Appellée quand la taille de l'entité change.
        /// </summary>
        /// <param name="sender">Entité concernée.</param>
        /// <param name="size">Nouvelle taille de l'entité.</param>
        private void Entity_OnSizeModified(object sender, float size)
        {
            CheckIfMustDie();
        }

        /// <summary>
        /// Vérifie si l'entité doit mourrir ( qu'elle est trop grosse).
        /// </summary>
        public void CheckIfMustDie() 
        {
            if (Entity.Size > AppConfig.UsedConfig.IDieMaxSize)
            {
                Entity.Alive = false;
                OnDie?.Invoke(this, null);
            }
        }

        /// <summary>
        /// Clone le module de mort.
        /// </summary>
        /// <returns>Clone du module.</returns>
        public object Clone()
        {
            return new SimpleSizeDie(this);
        }

        /// <summary>
        /// Déclanché quand l'entité doit mourrir.
        /// </summary>
        public event EventHandler OnDie;
    }
}
