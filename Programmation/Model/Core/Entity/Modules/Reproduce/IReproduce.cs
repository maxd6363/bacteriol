﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.Entity.Modules
{
    /// <summary>
    /// Interface mère de tout module de reproduction.
    /// </summary>
    public interface IReproduce : ICloneable, IEntityModule
    {
        /// <summary>
        /// Essaye de se reproduire, si cela réussit :
        /// invoque l'événement OnReproduce et créé la nouvelle entité.
        /// </summary>
        void TryReproduce();

        /// <summary>
        /// Déclanché quand l'entité se clone et retourne la nouvelle entité.
        /// </summary>
        event EventHandler<IEntity> OnReproduce;
    }
}
