﻿using Model.Utils;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Model.Core.Entity.Modules
{
    [DataContract(IsReference = true)]
    class SimpleReproduce : IReproduce
    {
        /// <summary>
        /// Entité concernée par le module.
        /// </summary>
        [DataMember]
        public IEntity Entity
        {
            get
            {
                return entity;
            }
            set
            {
                if (value == null) return;
                if (entity != null)
                {
                    throw new AccessViolationException("Vous ne pouvez pas modifier l'entité d'un module une fois que ce dernier en a une.");
                }
                entity = value;
                Entity.OnHealthModified += Entity_OnHealthModified;
                Entity.OnSizeModified += Entity_OnSizeModified;
            }
        }
        private IEntity entity;

        /// <summary>
        /// Retourne ou modifie la taille requise pour se reproduire.
        /// </summary>
        [DataMember]
        public float RequiredSize { get; private set; }

        /// <summary>
        /// Retourne ou modifie le pourcentage de vie nécéssaire pour se reproduire.
        /// Doit être entre 0 et 1.
        /// </summary>
        [DataMember]
        public float RequiredLifePercentage { get; private set; }

        /// <summary>
        /// Essaye de se reproduire, si cela réussit :
        /// invoque l'événement OnReproduce et créé la nouvelle entité.
        /// </summary>
        public void TryReproduce()
        {
            if (Entity == null) return;
            if (Entity.Size >= RequiredSize && Entity.Health / Entity.MaxHealth >= RequiredLifePercentage)
            {
                Entity.Size /= AppConfig.UsedConfig.IReproduceSizeReductionCoef;
                IEntity clone = Entity.Clone() as IEntity;
                OnReproduce?.Invoke(this, clone);
            }
        }

        /// <summary>
        /// Déclanché quand l'entité se clone et retourne la nouvelle entité.
        /// </summary>
        public event EventHandler<IEntity> OnReproduce;

        /// <summary>
        /// Constructeur par recopie du module.
        /// </summary>
        /// <param name="module">Module à cloner.</param>
        public SimpleReproduce(SimpleReproduce module)
        {
            Entity = module.Entity;
            RequiredSize = module.RequiredSize;
            RequiredLifePercentage = module.RequiredLifePercentage;
        }

        /// <summary>
        /// Constructeur du module.
        /// </summary>
        /// <param name="requiredSize">Taille requise pour se reproduire.</param>
        /// <param name="requiredLifePercentage">Pourcentage de vie requis pour se reproduire.</param>
        public SimpleReproduce(float requiredSize, float requiredLifePercentage)
        {
            Entity = entity;
            RequiredSize = requiredSize;
            RequiredLifePercentage = requiredLifePercentage;
            if (Entity != null)
            {
                Entity.OnHealthModified += Entity_OnHealthModified;
                Entity.OnSizeModified += Entity_OnSizeModified;

            }
        }

        /// <summary>
        /// Appellée quand la vie de l'entité change.
        /// </summary>
        /// <param name="sender">Entité concernée.</param>
        /// <param name="health">Nouvelle vie de l'entité.</param>
        private void Entity_OnHealthModified(object sender, float health)
        {
            TryReproduce();
        }

        /// <summary>
        /// Appellée quand la taille de l'entité change.
        /// </summary>
        /// <param name="sender">Entité concernée.</param>
        /// <param name="size">Nouvelle taille de l'entité.</param>
        private void Entity_OnSizeModified(object sender, float size)
        {
            TryReproduce();
        }

        /// <summary>
        /// Clone le module de reproduction.
        /// </summary>
        /// <returns>Clone du module.</returns>
        public object Clone()
        {
            return new SimpleReproduce(this);
        }
    }
}
