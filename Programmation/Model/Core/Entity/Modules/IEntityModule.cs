﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.Entity.Modules
{
    /// <summary>
    /// Interface mère de tout module.
    /// </summary>
    public interface IEntityModule
    {
        /// <summary>
        /// Retourne ou modifie l'entité concernée par le module.
        /// </summary>
        IEntity Entity { get; set; }
    }
}
