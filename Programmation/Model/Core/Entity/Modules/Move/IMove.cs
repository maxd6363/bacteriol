﻿using Model.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.Entity.Modules
{
    /// <summary>
    /// Interface mère de tout module de déplacement.
    /// </summary>
    public interface IMove : ICloneable, IEntityModule
    {
        /// <summary>
        /// Retourne la vitesse de l'entité concernée.
        /// </summary>
        float Speed { get; set; }

        /// <summary>
        /// Fait bouger l'entité.
        /// </summary>
        void Move();
    }
}
