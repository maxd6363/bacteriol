﻿using Model.Core.World;
using Model.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Model.Core.Entity.Modules
{
    /// <summary>
    /// Implémentation basique du module de déplacement.
    /// </summary>
    [DataContract(IsReference = true)]
    public class SimpleMove : IMove
    {
        /// <summary>
        /// Entité concernée par le module.
        /// </summary>
        [DataMember]
        public IEntity Entity
        {
            get
            {
                return entity;
            }
            set
            {
                if (entity != null)
                {
                    throw new AccessViolationException("Vous ne pouvez pas modifier l'entité d'un module une fois que ce dernier en a une.");
                }
                entity = value;
            }
        }
        private IEntity entity;

        /// <summary>
        /// Retourne ou modifie le monde dans lequel le module agit.
        /// </summary>
        private IWorld _World { get; set; }


        /// <summary>
        /// Retourne la vitesse de l'entité concernée.
        /// </summary>
        [DataMember]
        public float Speed { get; set; }


        /// <summary>
        /// Retourne ou modifie le sens de rotation de l'entité lorsqu'elle est bloquée.
        /// </summary>
        [DataMember]
        private sbyte _RotationMultiplier { get; set; }

        /// <summary>
        /// Constructeur du module.
        /// </summary>
        /// <param name="speed">Vitesse de base.</param>
        /// <param name="world">Monde dans lequel le module va agir.</param>
        public SimpleMove(float speed, IWorld world)
        {
            Speed = speed;
            _World = world;
            if (Randomizer.Draw(AppConfig.UsedConfig.MoveRotationDrawProbability))
            {
                _RotationMultiplier = 1;
            }
            else
            {
                _RotationMultiplier = -1;
            }
        }

        /// <summary>
        /// Constructeur par recopie du module.
        /// </summary>
        /// <param name="module">Module à cloner.</param>
        public SimpleMove(SimpleMove module)
        {
            Entity = module.Entity;
            Speed = module.Speed;
            _World = module._World;
            _RotationMultiplier = module._RotationMultiplier;
        }

        /// <summary>
        /// Clone le module de déplacement.
        /// </summary>
        /// <returns>Clone du module.</returns>
        public object Clone()
        {
            return new SimpleMove(this);
        }

        /// <summary>
        /// Fait bouger l'entité d'un Vector2 : Speed * LookingDirection
        /// </summary>
        public void Move()
        {
            Vector2 newLocation = Entity.Location + Entity.LookingDirection * Speed;
            if (_World.GetNodeValue(new Vector2((int)newLocation.X, (int)newLocation.Y)) == null)
            {
                Entity.LookingDirection = Vector2.Normalize(Entity.LookingDirection - new Vector2(_RotationMultiplier, -_RotationMultiplier));
            }
            else
            {
                Entity.Location = newLocation;
            }
        }
    }
}
