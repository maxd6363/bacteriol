﻿using Model.Core.Chunk;
using Model.Core.World;
using Model.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Model.Core.Entity.Modules
{
    /// <summary>
    /// Implémentation spécifiques aux bactéries du module de déplacement.
    /// </summary>
    [DataContract(IsReference = true)]
    public class ChunkValuesOrientedMove : IMove
    {
        /// <summary>
        /// Entité concernée par le module.
        /// </summary>
        [DataMember]
        public IEntity Entity
        {
            get
            {
                return entity;
            }
            set
            {
                if (entity != null)
                {
                    throw new AccessViolationException("Vous ne pouvez pas modifier l'entité d'un module une fois que ce dernier en a une.");
                }
                entity = value;
            }
        }
        private IEntity entity;

        /// <summary>
        /// Retourne ou modifie le monde dans lequel le module agit.
        /// </summary>
        private IWorld _World { get; set; }

        /// <summary>
        /// Retourne ou modifie le sens de rotation de l'entité lorsqu'elle est bloquée.
        /// </summary>
        [DataMember]
        private sbyte _RotationMultiplier { get; set; }

        /// <summary>
        /// Retourne la vitesse de l'entité concernée.
        /// </summary>
        [DataMember]
        public float Speed { get; private set; }
        float IMove.Speed { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        /// <summary>
        /// Constructeur du module.
        /// </summary>
        /// <param name="speed">Vitesse de base.</param>
        /// <param name="world">Monde dans lequel le module va agir.</param>
        public ChunkValuesOrientedMove(IWorld world, float speed)
        {
            Speed = speed;
            _World = world;
            if (Randomizer.Draw(AppConfig.UsedConfig.MoveRotationDrawProbability))
            {
                _RotationMultiplier = 1;
            }
            else
            {
                _RotationMultiplier = -1;
            }
        }

        /// <summary>
        /// Constructeur par recopie du module.
        /// </summary>
        /// <param name="module">Module à cloner.</param>
        public ChunkValuesOrientedMove(ChunkValuesOrientedMove module)
        {
            Entity = module.Entity;
            Speed = module.Speed;
            _World = module._World;
            _RotationMultiplier = module._RotationMultiplier;
        }

        /// <summary>
        /// Clone le module de déplacement.
        /// </summary>
        /// <returns>Clone du module.</returns>
        public object Clone()
        {
            return new ChunkValuesOrientedMove(this);
        }

        /// <summary>
        /// Fait bouger l'entité d'un Vector2 en fonction du monde.
        /// </summary>
        public void Move()
        {
            Dictionary<Vector2, float> gradient = new Dictionary<Vector2, float>();
            gradient.Add(Entity.LookingDirection, 1);
            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    Vector2 coords = new Vector2(Entity.Location.X + x, Entity.Location.Y + y);
                    float? value = _World.GetNodeValue(coords);
                    if (value.HasValue && (x != 0 || y != 0))
                    {
                        gradient.Add(Vector2.Normalize(new Vector2(x, y)), (float)value);
                    }
                }
            }
            int count = 1;
            Vector2 newLookingDirection = Vector2.Zero;
            foreach (KeyValuePair<Vector2, float> pair in gradient)
            {
                if (count == 1)
                {
                    newLookingDirection = pair.Key;
                }
                else
                {
                    newLookingDirection = Vector2.Lerp(newLookingDirection, pair.Key, 1 / (float)count);
                }
                count++;
            }
            Entity.LookingDirection = newLookingDirection;
            Vector2 newLocation = Entity.Location + Entity.LookingDirection * Speed;
            if (_World.GetNodeValue(new Vector2((int)newLocation.X, (int)newLocation.Y)) == null)
            {
                Entity.LookingDirection = Vector2.Normalize(Entity.LookingDirection - new Vector2(_RotationMultiplier, -_RotationMultiplier));
            }
            else
            {
                Entity.Location = newLocation;
            }
        }
    }
}
