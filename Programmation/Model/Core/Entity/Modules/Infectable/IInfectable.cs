﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.Entity.Modules
{
    /// <summary>
    /// Interface mère de tout module d'infection.
    /// </summary>
    public interface IInfectable : ICloneable, IEntityModule
    {
        /// <summary>
        /// Retourne vrai si l'entité concernée est infectée.
        /// </summary>
        bool Infected { get; }

        /// <summary>
        /// Tirage ayant une faible chance d'infecter l'entité concernée.
        /// </summary>
        void DrawInfected();

        /// <summary>
        /// Déclanché quand l'entité concernée devient infectée.
        /// </summary>
        event EventHandler OnInfected;

        /// <summary>
        /// Fait subir des dégâts à l'entité concernée si elle est infectée.
        /// </summary>
        void BeInfected();
    }
}
