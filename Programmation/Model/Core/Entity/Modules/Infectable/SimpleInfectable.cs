﻿using Model.Utils;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Model.Core.Entity.Modules
{

    
    /// <summary>
    /// Implémentation basique du module d'infection des bactéries.
    /// </summary>
    [DataContract(IsReference = true)]
    class SimpleInfectable : IInfectable
    {
        /// <summary>
        /// Entité concernée par le module.
        /// </summary>
        [DataMember]
        public IEntity Entity
        {
            get
            {
                return entity;
            }
            set
            {
                if (entity != null)
                {
                    throw new AccessViolationException("Vous ne pouvez pas modifier l'entité d'un module une fois que ce dernier en a une.");
                }
                entity = value;
            }
        }
        private IEntity entity;



        /// <summary>
        /// Retourne vrai si l'entité concernée est infectée.
        /// </summary>
        [DataMember]
        public bool Infected { get; private set; }

        /// <summary>
        /// Constructeur par recopie du module.
        /// </summary>
        /// <param name="module">Module à cloner.</param>
        public SimpleInfectable(SimpleInfectable module)
        {
            Entity = module.Entity;
            Infected = module.Infected;
        }

        /// <summary>
        /// Constructeur du module.
        /// </summary>
        public SimpleInfectable()
        {
            Infected = false;
        }

        /// <summary>
        /// Tirage ayant une faible chance d'infecter l'entité concernée (1 chance sur 10 d'être infecté).
        /// </summary>
        public void DrawInfected() 
        {
            if(!Infected)
            {
                if (Randomizer.Draw(AppConfig.UsedConfig.IInfectableDrawProbability))
                {
                    Infected = true;
                    OnInfected?.Invoke(this, null);
                }
            }
        }

        /// <summary>
        /// Déclanché quand l'entité concernée devient infectée.
        /// </summary>
        public event EventHandler OnInfected;

        /// <summary>
        /// Fait subir des dégâts à l'entité concernée si elle est infectée.
        /// </summary>
        public void BeInfected() 
        {
            if (Infected && Entity.Health > 0)
            {
                Entity.Health -= AppConfig.UsedConfig.IInfectableDamagePerTick;
            }
        }

        /// <summary>
        /// Clone le module d'infection.
        /// </summary>
        /// <returns>Clone du module.</returns>
        public object Clone()
        {
            return new SimpleInfectable(this);
        }
    }
}
