﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Core.Chunk;
using Model.Utils;
using Model.Generator;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using Model.Core.Entity;
using System.Collections.Specialized;
using Model.Core.Entity.Modules;
using System.Linq;
using System.Diagnostics;

namespace Model.Core.World
{
    /// <summary>
    /// Implémentation basique de l'interface IWorld, ce monde est monochunk.
    /// </summary>
    [DataContract(IsReference = true)]
    [KnownType(typeof(SimpleChunk))]
    [KnownType(typeof(SimpleWorldParameters))]
    [KnownType(typeof(PerlinChunkGenerator))]
    [KnownType(typeof(SimpleEntityGenerator))]
    [KnownType(typeof(SimpleBacteria))]
    [KnownType(typeof(SimpleEuplote))]
    public class SimpleWorld : IWorld

    {
        /// <summary>
        /// Retourne la seed avec laquelle le monde a été construit.
        /// </summary>
        [DataMember]
        public int Seed { get; private set; }

        /// <summary>
        /// Retourne la liste des chunks du monde.
        /// </summary>
        public IEnumerable<IChunk> Chunks { get => new List<IChunk> { _Chunk }; }

        /// <summary>
        /// Chunk unique du monde.
        /// </summary>
        [DataMember]
        private IChunk _Chunk { get; set; }

        /// <summary>
        /// Retourne la liste des bactéries du monde.
        /// </summary>
        public IEnumerable<Bacteria> Bacterias { get => _bacterias; private set { } }

        /// <summary>
        /// Retourne ou modifie la liste des bactéries du monde.
        /// </summary>
        [DataMember]
        private ObservableCollection<Bacteria> _bacterias { get; set; }

        /// <summary>
        /// Retourne la liste des euplotes du monde.
        /// </summary>
        public IEnumerable<Euplote> Euplotes { get => _euplotes; }

        /// <summary>
        /// Retourne ou modifie la liste des euplotes du monde.
        /// </summary>
        [DataMember]
        private ObservableCollection<Euplote> _euplotes { get; set; }

        /// <summary>
        /// Retourne les paramètres du monde.
        /// </summary>
        [DataMember]
        public IWorldParameters WorldParameters { get; set; }

        /// <summary>
        /// Retourne le générateur de chunks du monde.
        /// </summary>
        [DataMember]
        public IChunkGenerator ChunkGenerator { get; private set; }

        /// <summary>
        /// Retourne le générateur d'entités de la partie.
        /// </summary>
        [DataMember]
        public IEntityGenerator EntityGenerator { get; private set; }

        /// <summary>
        /// Retourne le chunk correspondant à la position donnée. Si il n'existe pas, le génère
        /// ou bien renvoi null si la position indiquée est invalide.
        /// </summary>
        /// <param name="position">Position.</param>
        /// <returns>Chunk voulu si il existe, sinon null.</returns>
        public IChunk GetChunk(Vector2 position)
        {
            if(_Chunk == null)
            {
                _Chunk = GenerateNewChunkWithEntities(Seed, Vector2.Zero);
            }
            if (_Chunk.Position.Equals(position))
            {
                return _Chunk;
            }
            return null;
        }

        /// <summary>
        /// Constructeur du monde.
        /// </summary>
        /// <param name="seed">Graine de génération du monde.</param>
        /// <param name="chunkGenerator">Générateur de chunks du monde.</param>
        /// <param name="worldParameters">Paramètres du monde.</param>
        public SimpleWorld(int seed, IChunkGenerator chunkGenerator, IEntityGenerator entityGenerator, IWorldParameters worldParameters)
        {
            Seed = seed;
            ChunkGenerator = chunkGenerator;
            EntityGenerator = entityGenerator;
            WorldParameters = worldParameters;
            _bacterias = new ObservableCollection<Bacteria>();
            _euplotes = new ObservableCollection<Euplote>();
            Subscribe();
        }

        /// <summary>
        /// Retourne la valeur du noeud pour des coordonnées donnée.
        /// </summary>
        /// <param name="absoluteCoordinates">Coordonnées.</param>
        /// <returns>Valeur du noeud, null si le noeud n'existe pas.</returns>
        public float? GetNodeValue(Vector2 absoluteCoordinates)
        {
            Vector2 minCoord = Vector2.Zero;
            Vector2 maxCoord = new Vector2(_Chunk.Size.X - 1, _Chunk.Size.Y - 1);
            if (absoluteCoordinates.X < minCoord.X || absoluteCoordinates.Y < minCoord.Y || absoluteCoordinates.X > maxCoord.X || absoluteCoordinates.Y > maxCoord.Y)
            {
                return null;
            }
            return _Chunk.Node(absoluteCoordinates);
        }

        /// <summary>
        /// Génère un nouveau chunk et ajoute automatiquement les entités dessus.
        /// </summary>
        /// <param name="seed">Graine de génération du monde.</param>
        /// <param name="position">Position du chunk dans le monde.</param>
        /// <returns></returns>
        private IChunk GenerateNewChunkWithEntities(int seed, Vector2 position)
        {
            IChunk chunk = ChunkGenerator.GenerateChunk(seed, position, AppConfig.UsedConfig.ChunkSize);
            foreach (Bacteria bacteria in EntityGenerator.GenerateBacteriaOnChunk(chunk, this))
            {
                _bacterias.Add(bacteria);
            }
            foreach (Euplote euplote in EntityGenerator.GenerateEuploteOnChunk(chunk, this))
            {
                _euplotes.Add(euplote);
            }
            return chunk;
        }

        /// <summary>
        /// Appellée à chaque fois que la liste des bactérie est modifiée.
        /// </summary>
        /// <param name="sender">Liste des bactéries.</param>
        /// <param name="eventArgs">Arguments de l'événement.</param>
        private void Bacteria_CollectionChanged(object sender, NotifyCollectionChangedEventArgs eventArgs)
        {
            switch (eventArgs.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Bacteria bacteria in eventArgs.NewItems)
                    {
                        OnBacteriaSpawn?.Invoke(this, bacteria);
                        bacteria.DieModule.OnDie += Bacteria_DieModule_OnDie;
                        bacteria.ReproduceModule.OnReproduce += Bacteria_ReproduceModule_OnReproduce;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (Bacteria bacteria in eventArgs.OldItems)
                    {
                        OnBacteriaDie?.Invoke(this, bacteria);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    throw new InvalidOperationException("Vous ne pouvez pas vider la collection de cette manière, merci de passer par des opérations de type \"Remove\".");

                case NotifyCollectionChangedAction.Replace:
                    foreach (Bacteria bacteria in eventArgs.OldItems)
                    {
                        OnBacteriaDie?.Invoke(this, bacteria);
                    }
                    foreach (Bacteria bacteria in eventArgs.NewItems)
                    {
                        OnBacteriaSpawn?.Invoke(this, bacteria);
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Appellée à chaque fois qu'une bactérie meurt.
        /// </summary>
        /// <param name="sender">Module de mort de la bactérie.</param>
        /// <param name="eventArgs">Arguments de l'événement.</param>
        private void Bacteria_DieModule_OnDie(object sender, EventArgs eventArgs)
        {
            OnBacteriaDie?.Invoke(this, (sender as IDie).Entity as Bacteria);
        }

        /// <summary>
        /// Appellée à chaque fois qu'une bactérie se divise.
        /// </summary>
        /// <param name="sender">Module de reproduction de la bactérie.</param>
        /// <param name="entity">Nouvelle bactérie.</param>
        private void Bacteria_ReproduceModule_OnReproduce(object sender, IEntity entity)
        {
            OnBacteriaDivide?.Invoke(this, Tuple.Create((sender as IReproduce).Entity as Bacteria, entity as Bacteria));
        }

        /// <summary>
        /// Appellée à chaque fois que la liste des euplotes est modifiée.
        /// </summary>
        /// <param name="sender">Liste des euplotes.</param>
        /// <param name="eventArgs">Arguments de l'événement.</param>
        private void Euplote_CollectionChanged(object sender, NotifyCollectionChangedEventArgs eventArgs)
        {
            switch (eventArgs.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Euplote euplote in eventArgs.NewItems)
                    {
                        OnEuploteSpawn?.Invoke(this, euplote);
                        euplote.DieModule.OnDie += Euplote_DieModule_OnDie;
                        euplote.ReproduceModule.OnReproduce += Euplote_ReproduceModule_OnReproduce;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (Euplote euplote in eventArgs.OldItems)
                    {
                        OnEuploteDie?.Invoke(this, euplote);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    throw new InvalidOperationException("Vous ne pouvez pas vider la collection de cette manière, merci de passer par des opérations de type \"Remove\".");

                case NotifyCollectionChangedAction.Replace:
                    foreach (Euplote euplote in eventArgs.OldItems)
                    {
                        OnEuploteDie?.Invoke(this, euplote);
                    }
                    foreach (Euplote euplote in eventArgs.NewItems)
                    {
                        OnEuploteSpawn?.Invoke(this, euplote);
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Appellée à chaque fois qu'une euplote meurt.
        /// </summary>
        /// <param name="sender">Module de mort de l'euplote.</param>
        /// <param name="eventArgs">Arguments de l'événement.</param>
        private void Euplote_DieModule_OnDie(object sender, EventArgs eventArgs)
        {
            OnEuploteDie?.Invoke(this, (sender as IDie).Entity as Euplote);
        }

        /// <summary>
        /// Appellée à chaque fois qu'une euplote se divise.
        /// </summary>
        /// <param name="sender">Module de reproduction de l'euplote.</param>
        /// <param name="entity">Nouvelle bactérie.</param>
        private void Euplote_ReproduceModule_OnReproduce(object sender, IEntity entity)
        {
            OnEuploteDivide?.Invoke(this, Tuple.Create((sender as IReproduce).Entity as Euplote, entity as Euplote));
        }

        /// <summary>
        /// Déclanché à chaque fois qu'une bactérie apparaît.
        /// </summary>
        public event EventHandler<Bacteria> OnBacteriaSpawn;

        /// <summary>
        /// Déclanché à chaque fois qu'une bactérie se divise en deux.
        /// </summary>
        public event EventHandler<Tuple<Bacteria, Bacteria>> OnBacteriaDivide;

        /// <summary>
        /// Déclanché à chaque fois qu'une bactérie meurt.
        /// </summary>
        public event EventHandler<Bacteria> OnBacteriaDie;

        /// <summary>
        /// Déclanché à chaque fois qu'un euplote apparaît.
        /// </summary>
        public event EventHandler<Euplote> OnEuploteSpawn;

        /// <summary>
        /// Déclanché à chaque fois qu'un euplote se divise en deux.
        /// </summary>
        public event EventHandler<Tuple<Euplote, Euplote>> OnEuploteDivide;

        /// <summary>
        /// Déclanché à chaque fois qu'un euplote meurt.
        /// </summary>
        public event EventHandler<Euplote> OnEuploteDie;

        /// <summary>
        /// S'abonner à une liste d'événements.
        /// </summary>
        private void Subscribe()
        {
            _bacterias.CollectionChanged += Bacteria_CollectionChanged;
            _euplotes.CollectionChanged += Euplote_CollectionChanged;
        }

        /// <summary>
        /// Se réabonner aux événements après une désérialisation. 
        /// </summary>
        /// <param name="context">Contexte de la sérialisation.</param>
        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            Subscribe();
        }
    }
}
