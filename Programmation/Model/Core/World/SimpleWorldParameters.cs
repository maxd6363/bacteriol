﻿using Model.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace Model.Core.World
{
    /// <summary>
    /// Implémentation basique de l'interface IWorldParameters.
    /// </summary>
    [DataContract(IsReference = true)]
    public class SimpleWorldParameters : IWorldParameters
    {
        /// <summary>
        /// Retourne la température du monde.
        /// </summary>
        public float Temperature {
            get => _temperature;
            set
            {
                _temperature = value;
                if (_temperature < AppConfig.UsedConfig.AbsoluteCold)
                {
                    _temperature = AppConfig.UsedConfig.AbsoluteCold;
                }
            }
        }

        /// <summary>
        /// Température du monde.
        /// </summary>
        [DataMember]
        private float _temperature;

        /// <summary>
        /// Retourne la salinité du monde.
        /// </summary>
        public float Salinity {
            get => _salinity;
            set
            {
                _salinity = value;
                if (_salinity < AppConfig.UsedConfig.MinimumPercentage)
                {
                    _salinity = AppConfig.UsedConfig.MinimumPercentage;
                }
                if (_salinity > AppConfig.UsedConfig.MaximumPercentage)
                {
                    _salinity = AppConfig.UsedConfig.MaximumPercentage;
                }
            }
        }

        /// <summary>
        /// Salinité du monde.
        /// </summary>
        [DataMember]
        private float _salinity;

        /// <summary>
        /// Retourne le taux d'oxygène du monde.
        /// </summary>
        public float OxygenLevel
        {
            get => _oxygenLevel;
            set
            {
                _oxygenLevel = value;
                if (_oxygenLevel < AppConfig.UsedConfig.MinimumPercentage)
                {
                    _oxygenLevel = AppConfig.UsedConfig.MinimumPercentage;
                }
                if (_oxygenLevel > AppConfig.UsedConfig.MaximumPercentage)
                {
                    _oxygenLevel = AppConfig.UsedConfig.MaximumPercentage;
                }
            }
        }

        /// <summary>
        /// Taux d'oxygène du monde.
        /// </summary>
        [DataMember]
        private float _oxygenLevel;


        /// <summary>
        /// Retourne la luminosité du monde.
        /// </summary>
        public float Brightness
        {
            get => _brightness;
            set
            {
                _brightness = value;
                if (_brightness < AppConfig.UsedConfig.MinimumPercentage)
                {
                    _brightness = AppConfig.UsedConfig.MinimumPercentage;
                }
                if (_brightness > AppConfig.UsedConfig.MaximumPercentage)
                {
                    _brightness = AppConfig.UsedConfig.MaximumPercentage;
                }
            }
        }


        /// <summary>
        /// Luminosité du monde.
        /// </summary>
        [DataMember]
        private float _brightness;

        /// <summary>
        /// Retourne ou modifie le taux de pollution du monde.
        /// </summary>
        public float PollutionLevel
        {
            get => _pollutionLevel;
            set
            {
                _pollutionLevel = value;
                if (_pollutionLevel < AppConfig.UsedConfig.MinimumPercentage)
                {
                    _pollutionLevel = AppConfig.UsedConfig.MinimumPercentage;
                }
                if (_pollutionLevel > AppConfig.UsedConfig.MaximumPercentage)
                {
                    _pollutionLevel = AppConfig.UsedConfig.MaximumPercentage;
                }
            }
        }

        /// <summary>
        /// Taux de pollution du monde.
        /// </summary>
        [DataMember]
        private float _pollutionLevel;

        /// <summary>
        /// Retourne ou modifie le taux de nourriture du monde.
        /// </summary>
        public float FoodLevel
        {
            get => _foodLevel;
            set
            {
                _foodLevel = value;
                if (_foodLevel < AppConfig.UsedConfig.MinimumPercentage)
                {
                    _foodLevel = AppConfig.UsedConfig.MinimumPercentage;
                }
                if (_foodLevel > AppConfig.UsedConfig.MaximumPercentage)
                {
                    _foodLevel = AppConfig.UsedConfig.MaximumPercentage;
                }
            }
        }

        /// <summary>
        /// Taux de pollution du monde.
        /// </summary>
        [DataMember]
        private float _foodLevel;

        /// <summary>
        /// Constructeur des paramètres
        /// </summary>
        /// <param name="temperature">Température de base.</param>
        /// <param name="salinity">Salinité de base.</param>
        /// <param name="oxygenLevel">Taux d'oxygène de base.</param>
        /// <param name="brightness">Luminosité de base.</param>
        /// <param name="pollutionLevel">Taux de pollution de base.</param>
        /// <param name="foodLevel">Taux de nourriture de base.</param>
        public SimpleWorldParameters(float temperature, float salinity, float oxygenLevel, float brightness, float pollutionLevel, float foodLevel)
        {
            Temperature = temperature;
            Salinity = salinity;
            OxygenLevel = oxygenLevel;
            //Food = food;
            Brightness = brightness;
            PollutionLevel = pollutionLevel;
            FoodLevel = foodLevel;
        }
    }
}
