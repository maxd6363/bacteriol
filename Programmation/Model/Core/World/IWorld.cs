﻿using Model.Core.Chunk;
using Model.Utils;
using Model.Generator;
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using Model.Core.Entity;

namespace Model.Core.World
{
    /// <summary>
    /// Interface qui définit ce qu'est un monde.
    /// </summary>
    public interface IWorld
    {
        /// <summary>
        /// Retourne la seed avec laquelle le monde a été construit.
        /// </summary>
        int Seed { get; }

        /// <summary>
        /// Retourne la liste des chunks du monde.
        /// </summary>
        IEnumerable<IChunk> Chunks { get; }

        /// <summary>
        /// Retourne la liste des bactéries du monde
        /// </summary>
        IEnumerable<Bacteria> Bacterias { get; }

        /// <summary>
        /// Retourne la liste des euplotes du monde.
        /// </summary>
        IEnumerable<Euplote> Euplotes { get; }

        /// <summary>
        /// Retourne les paramètres du monde.
        /// </summary>
        IWorldParameters WorldParameters { get; }

        /// <summary>
        /// Retourne le chunk correspondant à la position donnée. Si il n'existe pas, le génère
        /// ou bien renvoi null si la position indiquée est invalide.
        /// </summary>
        /// <param name="position">Position.</param>
        /// <returns>Chunk voulu si il existe, sinon null.</returns>
        IChunk GetChunk(Vector2 position);

        /// <summary>
        /// Retourne le générateur de chunks du monde.
        /// </summary>
        IChunkGenerator ChunkGenerator { get; }

        /// <summary>
        /// Retourne le générateur d'entités du monde.
        /// </summary>
        IEntityGenerator EntityGenerator { get; }

        /// <summary>
        /// Retourne la valeur du noeud pour des coordonnées donnée.
        /// </summary>
        /// <param name="absoluteCoordinates">Coordonnées.</param>
        /// <returns>Valeur du noeud, null si le noeud n'existe pas.</returns>
        float? GetNodeValue(Vector2 absoluteCoordinates);

        /// <summary>
        /// Déclanché à chaque fois qu'une bactérie apparaît.
        /// </summary>
        event EventHandler<Bacteria> OnBacteriaSpawn;

        /// <summary>
        /// Déclanché à chaque fois qu'une bactérie se divise en deux.
        /// </summary>
        event EventHandler<Tuple<Bacteria, Bacteria>> OnBacteriaDivide;

        /// <summary>
        /// Déclanché à chaque fois qu'une bactérie meurt.
        /// </summary>
        event EventHandler<Bacteria> OnBacteriaDie;

        /// <summary>
        /// Déclanché à chaque fois qu'un euplote apparaît.
        /// </summary>
        event EventHandler<Euplote> OnEuploteSpawn;

        /// <summary>
        /// Déclanché à chaque fois qu'un euplote se divise en deux.
        /// </summary>
        event EventHandler<Tuple<Euplote, Euplote>> OnEuploteDivide;

        /// <summary>
        /// Déclanché à chaque fois qu'un euplote meurt.
        /// </summary>
        event EventHandler<Euplote> OnEuploteDie;
    }
}
