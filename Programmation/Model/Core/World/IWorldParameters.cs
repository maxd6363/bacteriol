﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Core.World
{
    /// <summary>
    /// Interface définissant tout ensemble de paramètres d'un monde tel que la température, la salinité etc...
    /// </summary>
    public interface IWorldParameters
    {
        /// <summary>
        /// Retourne ou modifie la température du monde.
        /// </summary>
        float Temperature { get; set; }

        /// <summary>
        /// Retourne ou modifie la salinité du monde.
        /// </summary>
        float Salinity { get; set; }

        /// <summary>
        /// Retourne ou modifie le taux d'oxygène du monde.
        /// </summary>
        float OxygenLevel { get; set; }

        /// <summary>
        /// Retourne ou modifie la luminosité du monde.
        /// </summary>
        float Brightness { get; set; }

        /// <summary>
        /// Retourne ou modifie le taux de pollution du monde.
        /// </summary>
        float PollutionLevel { get; set; }

        /// <summary>
        /// Retourne ou modifie le taux de nourriture du monde.
        /// </summary>
        float FoodLevel { get; set; }
    }
}
