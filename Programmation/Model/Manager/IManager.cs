﻿using Model.Core.Game;
using Model.Core.World;
using Model.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Manager
{
    /// <summary>
    /// Interface qui définit ce qu'est un manager.
    /// </summary>
    public interface IManager
    {
        /// <summary>
        /// Retourne la partie courante (null si in n'y en a pas).
        /// </summary>
        IGame CurrentGame { get; }

        /// <summary>
        /// Retourne la liste des parties disponibles.
        /// </summary>
        IEnumerable<IGame> Games { get; }

        /// <summary>
        /// Retourne la configuration utilisée.
        /// </summary>
        AppConfig Configuration { get; }

        /// <summary>
        /// Charge une partie pour qu'elle devienne la partie courante.
        /// </summary>
        /// <param name="name">Nom de la partie à charger.</param>
        /// <returns>La partie chargée (null en cas d'échec).</returns>
        IGame SetCurrentGame(string name);

        /// <summary>
        /// Créé une nouvelle partie.
        /// </summary>
        /// <param name="name">Nom de la partie.</param>
        /// <param name="seed">Graine de génération du monde de la partie.</param>
        /// <param name="worldParameters">Paramètres par défaut de la partie.</param>
        /// <returns>La nouvelle partie (null en cas d'échec).</returns>
        IGame CreateNewGame(string name, int? seed = null, IWorldParameters worldParameters = null);

        /// <summary>
        /// Supprime une partie.
        /// </summary>
        /// <param name="name">Nom de la partie.</param>
        /// <returns>Vrai si la partie a bien été supprimée, faux dans le cas contraire.</returns>
        bool DeleteGame(string name);

        /// <summary>
        /// Sauvegarde toutes les parties.
        /// </summary>
        void SaveGames();
    }
}
