﻿using Model.Core.Game;
using Model.Core.World;
using Model.Factory;
using Model.Serialization;
using Model.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Model.Manager
{
    /// <summary>
    /// Implémentation basique de l'interface IManager.
    /// </summary>
    public class SimpleManager : IManager
    {
        /// <summary>
        /// Retourne la partie courante (null si in n'y en a pas).
        /// </summary>
        public IGame CurrentGame { get; private set; }

        /// <summary>
        /// Retourne la liste des parties disponibles.
        /// Accès en lecture seule profonde.
        /// </summary>
        public IEnumerable<IGame> Games { get => new List<IGame>(_games); }

        /// <summary>
        /// Retourne ou modifie la liste des parties disponibles.
        /// </summary>
        private List<IGame> _games { get; set; }

        /// <summary>
        /// Retourne la configuration utilisée.
        /// </summary>
        public AppConfig Configuration { get => AppConfig.UsedConfig; }

        /// <summary>
        /// Retourne la factory utilisée.
        /// </summary>
        private IFactory _Factory { get; }

        /// <summary>
        /// Désérialiseur de parties utilisé.
        /// </summary>
        private IGameLoader _GameLoader { get; }

        /// <summary>
        /// Sérialiseur de parties utilisé.
        /// </summary>
        private IGameSaver _GameSaver { get; }

        /// <summary>
        /// Constructeur du manager.
        /// </summary>
        /// <param name="factory">Factory à utiliser.</param>
        /// <param name="gameLoader">Désérialiseur de parties à utiliser.</param>
        /// <param name="gameSaver">Sérialiseur de parties à utiliser.</param>
        public SimpleManager(IFactory factory, IGameLoader gameLoader, IGameSaver gameSaver)
        {
            _Factory = factory;
            _GameLoader = gameLoader;
            _GameSaver = gameSaver;
            LoadAllGames();
        }

        /// <summary>
        /// Charge une partie pour qu'elle devienne la partie courante.
        /// </summary>
        /// <param name="name">Nom de la partie à charger.</param>
        /// <returns>La partie chargée (null en cas d'échec).</returns>
        public IGame SetCurrentGame(string name)
        {
            foreach (IGame game in _games)
            {
                if (game.Name == name)
                {
                    CurrentGame = game;
                    return CurrentGame;
                }
            }
            return null;
        }

        /// <summary>
        /// Créé une nouvelle partie.
        /// </summary>
        /// <param name="name">Nom de la partie.</param>
        /// <param name="seed">Graine de génération du monde de la partie.</param>
        /// <param name="worldParameters">Paramètres par défaut de la partie.</param>
        /// <returns>La nouvelle partie (null en cas d'échec).</returns>
        public IGame CreateNewGame(string name, int? seed = null, IWorldParameters worldParameters = null)
        {
            IGame game = _Factory.BuildIGame(name, seed, worldParameters);
            _games.Add(game);
            return game;
        }

        /// <summary>
        /// Supprime une partie.
        /// </summary>
        /// <param name="name">Nom de la partie.</param>
        /// <returns>Vrai si la partie a bien été supprimée, faux dans le cas contraire.</returns>
        public bool DeleteGame(string name)
        {
            IGame gameToDelete = null;
            foreach (IGame game in _games)
            {
                if (game.Name == name)
                {
                    gameToDelete = game;
                    break;
                }
            }
            if (gameToDelete != null)
            {
                _games.Remove(gameToDelete);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Sauvegarde toutes les parties grâce au sérialiseur.
        /// </summary>
        public void SaveGames()
        {
            _GameSaver.Save(_games);
        }

        /// <summary>
        /// Charge toutes les parties en mémoire grâce au désérialiseur.
        /// </summary>
        private void LoadAllGames()
        {
            try
            {
                _games = new List<IGame>(_GameLoader.Load());
            }
            catch (FileNotFoundException)
            {
                _games = new List<IGame>();
            }
        }
    }
}
