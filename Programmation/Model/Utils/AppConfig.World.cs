﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Utils
{
    public partial class AppConfig
    {
        /// <summary>
        /// Température du froid absolu.
        /// </summary>
        public virtual float AbsoluteCold { get => -273.15f; }

        /// <summary>
        /// Taille d'un chunk.
        /// Il est recommandé de ne pas y toucher tout le temps.
        /// </summary>
        public virtual Vector2 ChunkSize { get => new Vector2(1024, 1024); }

        /// <summary>
        /// Coordonnées du chunk de base.
        /// </summary>
        public virtual Vector2 BaseChunkCoords { get => Vector2.Zero; }

        /// <summary>
        /// Température par défaut dans un monde.
        /// </summary>
        public virtual float SimpleWorldParametersDefaultTemperature { get => 20; }

        /// <summary>
        /// Salinité par défaut dans un monde.
        /// </summary>
        public virtual float SimpleWorldParametersDefaultSalinity { get => 20; }

        /// <summary>
        /// Taux d'oxygène par défaut dans un monde.
        /// </summary>
        public virtual float SimpleWorldParametersDefaultOxygenLevel { get => 20; }

        /// <summary>
        /// Luminosité par défaut dans un monde.
        /// </summary>
        public virtual float SimpleWorldParametersDefaultBrightness { get => 20; }

        /// <summary>
        /// Taux de pollution par défaut dans un monde.
        /// </summary>
        public virtual float SimpleWorldParametersDefaultPollution { get => 0; }

        /// <summary>
        /// Taux de pollution par défaut dans un monde.
        /// </summary>
        public virtual float SimpleWorldParametersDefaultFood { get => 50; }

        /// <summary>
        /// Taille des tables de permutation utilisées pour le bruit de perlin.
        /// Doit être supérieure ou égale à la constante TableMinimumSize dans PerlinChunkGenerator
        /// et doit aussi être une puissance de 2.
        /// Il est vivement recommandé de ne pas y toucher.
        /// </summary>
        public virtual ushort PerlinChunkGeneratorTableSize { get => 256; }

        /// <summary>
        /// Zoom à utiliser (0+) lors de la génération de chunk avec un PerlinChunkGenerator.
        /// Il est recommandé de ne pas y toucher tout le temps.
        /// </summary>
        public virtual float PerlinChunkGeneratorZoom { get => 5f; }

        /// <summary>
        /// Nombre d'octaves (0+) à utiliser lors de la génération de chunk avec un PerlinChunkGenerator.
        /// Il est recommandé de ne pas y toucher tout le temps.
        /// </summary>
        public virtual int PerlinChunkGeneratorNbOctaves { get => 4; }

        /// <summary>
        /// Taux de persistance des reliefs (entre 0 et 1) à utiliser lors de la génération de chunk avec un PerlinChunkGenerator.
        /// Il est recommandé de ne pas y toucher tout le temps.
        /// </summary>
        public virtual float PerlinChunkGeneratorPersistance { get => 0.45f; }

        /// <summary>
        /// Quantité d'imperfections (0+) à utiliser lors de la génération de chunk avec un PerlinChunkGenerator.
        /// Il est recommandé de ne pas y toucher tout le temps.
        /// </summary>
        public virtual float PerlinChunkGeneratorImperfections { get => 2f; }

    }
}
