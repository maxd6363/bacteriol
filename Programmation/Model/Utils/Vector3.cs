﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Utils
{
    /// <summary>
    /// Structure qui joue le rôle de vecteur à 3 coordonnées.
    /// </summary>
    public struct Vector3 : IEquatable<Vector3>, IFormattable
    {
        /// <summary>
        /// Retourne l'élément X du vecteur.
        /// </summary>
        public double X { get; private set; }

        /// <summary>
        /// Retourne l'élément Y du vecteur.
        /// </summary>
        public double Y { get; private set; }

        /// <summary>
        /// Retourne l'élément X du vecteur.
        /// </summary>
        public double Z { get; private set; }

        /// <summary>
        /// Retourne un nouveau vecteur (0, 0, 0).
        /// </summary>
        public static Vector3 Zero { get => new Vector3(0); }

        /// <summary>
        /// Retourne un nouveau vecteur (1, 0, 0).
        /// </summary>
        public static Vector3 UnitX { get => new Vector3(1, 0, 0); }

        /// <summary>
        /// Retourne un nouveau vecteur (0, 1, 0).
        /// </summary>
        public static Vector3 UnitY { get => new Vector3(0, 1, 0); }

        /// <summary>
        /// Retourne un nouveau vecteur (0, 0, 1).
        /// </summary>
        public static Vector3 UnitZ { get => new Vector3(0, 0, 1); }

        /// <summary>
        /// Retourne un nouveau vecteur (1, 1, 1).
        /// </summary>
        public static Vector3 One { get => new Vector3(1); }

        /// <summary>
        /// Crée un vecteur dont les deux éléments ont la même valeur.
        /// </summary>
        /// <param name="value">Valeur des deux éléments.</param>
        public Vector3(double value)
        {
            X = value;
            Y = value;
            Z = value;
        }

        /// <summary>
        /// Crée un vecteur dont les éléments ont les valeurs spécifiées.
        /// </summary>
        /// <param name="x">Valeur de l'élément X.</param>
        /// <param name="y">Valeur de l'élément Y.</param>
        /// <param name="z">Valeur de l'élément Z.</param>
        public Vector3(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        /// <summary>
        /// Additione deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vecteur résultant de l'addition.</returns>
        public static Vector3 operator +(Vector3 vector1, Vector3 vector2)
        {
            return new Vector3(vector1.X + vector2.X, vector1.Y + vector2.Y, vector1.Z + vector2.Z);
        }

        /// <summary>
        /// Soustrait deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vecteur résultant de la soustraction.</returns>
        public static Vector3 operator -(Vector3 vector1, Vector3 vector2)
        {
            return new Vector3(vector1.X - vector2.X, vector1.Y - vector2.Y, vector1.Z - vector2.Z);
        }

        /// <summary>
        /// Multiplie deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vecteur résultant de la multiplication.</returns>
        public static Vector3 operator *(Vector3 vector1, Vector3 vector2)
        {
            return new Vector3(vector1.X * vector2.X, vector1.Y * vector2.Y, vector1.Z * vector2.Z);
        }

        /// <summary>
        /// Multiplie un vecteur par un nombre.
        /// </summary>
        /// <param name="vector">Vecteur.</param>
        /// <param name="number">Nombre.</param>
        /// <returns>Vecteur résultant de la multiplication.</returns>
        public static Vector3 operator *(Vector3 vector, Single number)
        {
            return new Vector3(vector.X * number, vector.Y * number, vector.Z * number);
        }

        /// <summary>
        /// Divise deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vecteur résultant de la division.</returns>
        public static Vector3 operator /(Vector3 vector1, Vector3 vector2)
        {
            return new Vector3(vector1.X / vector2.X, vector1.Y / vector2.Y, vector1.Z / vector2.Z);
        }

        /// <summary>
        /// Multiplie un vecteur par un nombre.
        /// </summary>
        /// <param name="vector">Vecteur.</param>
        /// <param name="number">Nombre.</param>
        /// <returns>Vecteur résultant de la division.</returns>
        public static Vector3 operator /(Vector3 vector, Single number)
        {
            return new Vector3(vector.X / number, vector.Y / number, vector.Z / number);
        }

        /// <summary>
        /// Retourne un boolean qui indique si deux vecteurs sont identiques.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vrai si les vecteurs sont identiques.</returns>
        public static bool operator ==(Vector3 vector1, Vector3 vector2)
        {
            return vector1.Equals(vector2);
        }

        /// <summary>
        /// Retourne un boolean qui indique si deux vecteurs sont différents.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vrai si les vecteurs sont différents.</returns>
        public static bool operator !=(Vector3 vector1, Vector3 vector2)
        {
            return !vector1.Equals(vector2);
        }

        /// <summary>
        /// Inverse le signe du vecteur spécifié.
        /// </summary>
        /// <param name="vector">Vecteur.</param>
        /// <returns>Vecteur résultant de l'inversion.</returns>
        public static Vector3 operator -(Vector3 vector)
        {
            return Zero - vector;
        }

        /// <summary>
        /// Retourne un vecteur dont les éléments sont les valeurs absolues de chacun des éléments du vecteur spécifié.
        /// </summary>
        /// <param name="vector">Vecteur spécifié.</param>
        /// <returns>Vecteur absolu du vecteur spécifié.</returns>
        public static Vector3 Abs(Vector3 vector)
        {
            return new Vector3(Math.Abs(vector.X), Math.Abs(vector.Y), Math.Abs(vector.Z));
        }

        /// <summary>
        /// Retourne un vecteur dont les éléments sont le minimum des éléments de deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vecteur minimisé.</returns>
        public static Vector3 Min(Vector3 vector1, Vector3 vector2)
        {
            return new Vector3(
                (vector1.X < vector2.X) ? vector1.X : vector2.X,
                (vector1.Y < vector2.Y) ? vector1.Y : vector2.Y,
                (vector1.Z < vector2.Z) ? vector1.Z : vector2.Z);
        }

        /// <summary>
        /// Retourne un vecteur dont les éléments sont le maximum des éléments de deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vecteur maximisé.</returns>
        public static Vector3 Max(Vector3 vector1, Vector3 vector2)
        {
            return new Vector3(
                (vector1.X > vector2.X) ? vector1.X : vector2.X,
                (vector1.Y > vector2.Y) ? vector1.Y : vector2.Y,
                (vector1.Z > vector2.Z) ? vector1.Z : vector2.Z);
        }

        /// <summary>
        /// Calcule la distance euclidienne entre les deux points donnés.
        /// </summary>
        /// <param name="vector1">Premier point.</param>
        /// <param name="vector2">Deuxième point.</param>
        /// <returns>Distance.</returns>
        public static double Distance(Vector3 vector1, Vector3 vector2)
        {
            double dx = vector1.X - vector2.X;
            double dy = vector1.Y - vector2.Y;
            double dz = vector1.Z - vector2.Z;
            double ls = dx * dx + dy * dy + dz * dz;
            return Math.Sqrt(ls);
        }

        /// <summary>
        /// Retourne le produit scalaire de deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Produit scalaire.</returns>
        public static double Dot(Vector3 vector1, Vector3 vector2)
        {
            return vector1.X * vector2.X + vector1.Y * vector2.Y + vector1.Z * vector2.Z;
        }


        /// <summary>
        /// Retourne la longueur du vecteur.
        /// </summary>
        /// <returns>Longueur du vecteur.</returns>
        public double Length()
        {
            double ls = X * X + Y * Y + Z * Z;
            return Math.Sqrt(ls);
        }

        /// <summary>
        /// Restreint un vecteur entre un minimum et un maximum.
        /// </summary>
        /// <param name="vector">Vecteur source.</param>
        /// <param name="min">Vecteur minimum.</param>
        /// <param name="max">Vecteur maximum.</param>
        public static Vector3 Clamp(Vector3 vector, Vector3 min, Vector3 max)
        {
            double x = vector.X;
            x = (x > max.X) ? max.X : x;
            x = (x < min.X) ? min.X : x;
            double y = vector.Y;
            y = (y > max.Y) ? max.Y : y;
            y = (y < min.Y) ? min.Y : y;
            double z = vector.Z;
            z = (z > max.Z) ? max.Z : z;
            z = (z < min.Z) ? min.Z : z;
            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Fait une interpolation linéaire entre deux vecteurs basées sur un élargissement.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <param name="amount">Valeur entre 0 et 1 indiquant le poids de deuxième vecteur.</param>
        /// <returns>The interpolated vector.</returns>
        public static Vector3 Lerp(Vector3 vector1, Vector3 vector2, float amount)
        {
            return new Vector3(
                vector1.X + (vector2.X - vector1.X) * amount,
                vector1.Y + (vector2.Y - vector1.Y) * amount,
                vector1.Z + (vector2.Z - vector1.Z) * amount);
        }

        /// <summary>
        /// Retourne un vecteur avec le même direction que celui donné, mais de longueur 1.
        /// </summary>
        /// <param name="vector">Vecteur à normaliser.</param>
        /// <returns>Vecteur normalisé.</returns>
        public static Vector3 Normalize(Vector3 vector)
        {
            double ls = vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z;
            double invNorm = 1.0 / Math.Sqrt(ls);
            return new Vector3(vector.X * invNorm, vector.Y * invNorm, vector.Z * invNorm);
        }

        /// <summary>
        /// Retourne la réflexion d'un vecteur depuis une surface qui a la normale spécifiée.
        /// </summary>
        /// <param name="vector">Vecteur source.</param>
        /// <param name="normal">Normale de la surface depuis laquelle la réflexion est effectuée.</param>
        /// <returns>Vecteur reflété.</returns>
        public static Vector3 Reflect(Vector3 vector, Vector3 normal)
        {
            double dot = vector.X * normal.X + vector.Y * normal.Y + vector.Z * normal.Z;
            double tempX = normal.X * dot * 2f;
            double tempY = normal.Y * dot * 2f;
            double tempZ = normal.Z * dot * 2f;
            return new Vector3(vector.X - tempX, vector.Y - tempY, vector.Z - tempZ);
        }

        /// <summary>
        /// Redéfinition du HashCode.
        /// </summary>
        /// <returns>HashCode du vecteur.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Redéfinition du protocole d'égalité.
        /// </summary>
        /// <param name="other"></param>
        /// <returns>Vrai si les objets sont identiques.</returns>
        public override bool Equals(object other)
        {
            if (ReferenceEquals(other, null)) return false;
            if (ReferenceEquals(other, this)) return true;
            if (GetType() != other.GetType()) return false;
            return Equals((Vector2)other);
        }

        /// <summary>
        /// Redéfinition du protocole d'égalité.
        /// </summary>
        /// <param name="other"></param>
        /// <returns>Vrai si les vecteurs ont les mêmes caractéristiques.</returns>
        public bool Equals(Vector3 other)
        {
            if (X != other.X) return false;
            if (Y == other.Y) return Z == other.Z;
            return false;
        }

        /// <summary>
        /// Implémentation de IFormattable.
        /// </summary>
        /// <param name="format">Format à utiliser (par défaut : "X, Y, Z").</param>
        /// <param name="formatProvider">Provider à utiliser (par défaut : null).</param>
        /// <returns>Vecteur sous forme de string au format désiré.</returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (string.IsNullOrEmpty(format)) format = "X, Y, Z";
            StringBuilder builder = new StringBuilder();

            foreach (char c in format)
            {
                if (char.ToUpper(c) == 'X')
                {
                    builder.Append(X);
                    continue;
                }
                if (char.ToUpper(c) == 'Y')
                {
                    builder.Append(Y);
                    continue;
                }
                if (char.ToUpper(c) == 'Z')
                {
                    builder.Append(Y);
                    continue;
                }
                builder.Append(c);
            }

            return builder.ToString();
        }

        /// <summary>
        /// Méthode ToString.
        /// </summary>
        /// <returns>Vecteur sous forme de string au format de base.</returns>
        public override string ToString()
        {
            return ToString(null, null);
        }
    }
}
