﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Utils
{
    /// <summary>
    /// Toutes les constantes en lien avec le Model de l'application sont stockées ici.
    /// </summary>
    public partial class AppConfig
    {
        /// <summary>
        /// Configuration à utiliser.
        /// </summary>
        public static AppConfig UsedConfig { get => new AppConfig(); }

        /// <summary>
        /// Constructeur privé pour empêcher l'instanciation.
        /// </summary>
        private AppConfig(){}

        /// <summary>
        /// Durée d'un tick en millisecondes.
        /// </summary>
        public virtual int TickDuration { get => 20; }

        /// <summary>
        /// Minimum de toute valeur en pourcentage.
        /// </summary>
        public virtual float MinimumPercentage { get => 0; }

        /// <summary>
        /// Maximum de toute valeur en pourcentage.
        /// </summary>
        public virtual float MaximumPercentage { get => 100; }
    }
}
