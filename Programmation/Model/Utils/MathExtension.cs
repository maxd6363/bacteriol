﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Utils
{
    /// <summary>
    /// Classe statique ajoutant des fonctions manquantes dans la classe Math.
    /// </summary>
    public static class MathExtension
    {
        /// <summary>
        /// Retourne une valeur entrée mais limitée dans une plage inclusive.
        /// </summary>
        /// <typeparam name="T">Tout type implémentant IComparable<T>.</typeparam>
        /// <param name="value">Valeur à traiter.</param>
        /// <param name="minimum">Minimum de la plage inclusive.</param>
        /// <param name="maximum">Maximum de la plage inclusive.</param>
        /// <returns>Valeur limitée dans la plage inclusive.</returns>
        public static T Clamp<T>(T value, T minimum, T maximum) where T : IComparable<T>
        {
            var result = value;
            if (value.CompareTo(maximum) > 0)
            {
                result = maximum;
            }
            else if (value.CompareTo(minimum) < 0)
            {
                result = minimum;
            }
            return result;
        }

        public static float DCos(double angle)
        {
            return (float)Math.Cos(DegreeToRadian(angle));
        }
        public static float DSin(double angle)
        {
            return (float)Math.Sin(DegreeToRadian(angle));
        }
        public static float DegreeToRadian(double angle)
        {
            return (float)(Math.PI * angle / 180.0);
        }

        public static float RadianToDegree(double angle)
        {
            return (float)(angle * 180.0 / Math.PI);
        }



    }
}
