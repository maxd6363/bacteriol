﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Utils
{
    /// <summary>
    /// Classe qui encapsule le générateur de nombres aléatoires, afin de préserver son unique instance.
    /// </summary>
    public class Randomizer
    {
        /// <summary>
        /// Retourne l'instance unique du Random.
        /// </summary>
        public static Random Random { get => random; }


        /// <summary>
        /// Retourne ou modifie la seed du Random.
        /// </summary>
        public static int? Seed
        {
            get => seed;
            set
            {
                seed = value;
                if (seed != null)
                {
                    random = new Random((int) seed);
                }
                else
                {
                    random = new Random();
                }
                
            }
        }

        private static Random random = new Random();
        private static int? seed = null;

        /// <summary>
        /// Effectue un tirage, ayant un certain pourcentage de retourner vrai.
        /// </summary>
        /// <param name="percentage">Pourcentage de chance que le tirage retourne vrai (entre 0 et 100).</param>
        /// <returns>Résultat du tirage.</returns>
        public static bool Draw(float percentage)
        {
            return percentage / 100.0 > random.NextDouble();
        }
    }
}
