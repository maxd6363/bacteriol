﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Model.Utils
{
    /// <summary>
    /// Structure qui joue le rôle de vecteur à 2 coordonnées.
    /// </summary>
    public struct Vector2 : IEquatable<Vector2>, IFormattable
    {
        /// <summary>
        /// Retourne l'élément X du vecteur.
        /// </summary>
        public double X { get; private set; }

        /// <summary>
        /// Retourne l'élément Y du vecteur.
        /// </summary>
        public double Y { get; private set; }

        /// <summary>
        /// Retourne un nouveau vecteur (0, 0).
        /// </summary>
        public static Vector2 Zero { get => new Vector2(0); }

        /// <summary>
        /// Retourne un nouveau vecteur (1, 0).
        /// </summary>
        public static Vector2 UnitX { get => new Vector2(1, 0); }

        /// <summary>
        /// Retourne un nouveau vecteur (0, 1).
        /// </summary>
        public static Vector2 UnitY { get => new Vector2(0, 1); }

        /// <summary>
        /// Retourne un nouveau vecteur (1, 1).
        /// </summary>
        public static Vector2 One { get => new Vector2(1); }

        /// <summary>
        /// Crée un vecteur dont les deux éléments ont la même valeur.
        /// </summary>
        /// <param name="value">Valeur des deux éléments.</param>
        public Vector2(double value)
        {
            X = value;
            Y = value;
        }

        /// <summary>
        /// Crée un vecteur dont les éléments ont les valeurs spécifiées.
        /// </summary>
        /// <param name="x">Valeur de l'élément X.</param>
        /// <param name="y">Valeur de l'élément Y.</param>
        public Vector2(double x, double y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Additione deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vecteur résultant de l'addition.</returns>
        public static Vector2 operator +(Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(vector1.X + vector2.X, vector1.Y + vector2.Y);
        }

        /// <summary>
        /// Soustrait deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vecteur résultant de la soustraction.</returns>
        public static Vector2 operator -(Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(vector1.X - vector2.X, vector1.Y - vector2.Y);
        }

        /// <summary>
        /// Multiplie deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vecteur résultant de la multiplication.</returns>
        public static Vector2 operator *(Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(vector1.X * vector2.X, vector1.Y * vector2.Y);
        }

        /// <summary>
        /// Multiplie un vecteur par un nombre.
        /// </summary>
        /// <param name="vector">Vecteur.</param>
        /// <param name="number">Nombre.</param>
        /// <returns>Vecteur résultant de la multiplication.</returns>
        public static Vector2 operator *(Vector2 vector, Single number)
        {
            return new Vector2(vector.X * number, vector.Y * number);
        }

        /// <summary>
        /// Divise deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vecteur résultant de la division.</returns>
        public static Vector2 operator /(Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(vector1.X / vector2.X, vector1.Y / vector2.Y);
        }

        /// <summary>
        /// Multiplie un vecteur par un nombre.
        /// </summary>
        /// <param name="vector">Vecteur.</param>
        /// <param name="number">Nombre.</param>
        /// <returns>Vecteur résultant de la division.</returns>
        public static Vector2 operator /(Vector2 vector, Single number)
        {
            return new Vector2(vector.X / number, vector.Y / number);
        }

        /// <summary>
        /// Retourne un boolean qui indique si deux vecteurs sont identiques.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vrai si les vecteurs sont identiques.</returns>
        public static bool operator ==(Vector2 vector1, Vector2 vector2)
        {
            return vector1.Equals(vector2);
        }

        /// <summary>
        /// Retourne un boolean qui indique si deux vecteurs sont différents.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vrai si les vecteurs sont différents.</returns>
        public static bool operator !=(Vector2 vector1, Vector2 vector2)
        {
            return !vector1.Equals(vector2);
        }

        /// <summary>
        /// Inverse le signe du vecteur spécifié.
        /// </summary>
        /// <param name="vector">Vecteur.</param>
        /// <returns>Vecteur résultant de l'inversion.</returns>
        public static Vector2 operator -(Vector2 vector)
        {
            return Zero - vector;
        }

        /// <summary>
        /// Retourne un vecteur dont les éléments sont les valeurs absolues de chacun des éléments du vecteur spécifié.
        /// </summary>
        /// <param name="vector">Vecteur spécifié.</param>
        /// <returns>Vecteur absolu du vecteur spécifié.</returns>
        public static Vector2 Abs(Vector2 vector)
        {
            return new Vector2(Math.Abs(vector.X), Math.Abs(vector.Y));
        }

        /// <summary>
        /// Retourne un vecteur dont les éléments sont le minimum des éléments de deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vecteur minimisé.</returns>
        public static Vector2 Min(Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(
                (vector1.X < vector2.X) ? vector1.X : vector2.X,
                (vector1.Y < vector2.Y) ? vector1.Y : vector2.Y);
        }

        /// <summary>
        /// Retourne un vecteur dont les éléments sont le maximum des éléments de deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Vecteur maximisé.</returns>
        public static Vector2 Max(Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(
                (vector1.X > vector2.X) ? vector1.X : vector2.X,
                (vector1.Y > vector2.Y) ? vector1.Y : vector2.Y);
        }

        /// <summary>
        /// Calcule la distance euclidienne entre les deux points donnés.
        /// </summary>
        /// <param name="vector1">Premier point.</param>
        /// <param name="vector2">Deuxième point.</param>
        /// <returns>Distance.</returns>
        public static double Distance(Vector2 vector1, Vector2 vector2)
        {
            double dx = vector1.X - vector2.X;
            double dy = vector1.Y - vector2.Y;
            double ls = dx * dx + dy * dy;
            return Math.Sqrt(ls);
        }

        /// <summary>
        /// Retourne le produit scalaire de deux vecteurs.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <returns>Produit scalaire.</returns>
        public static double Dot(Vector2 vector1, Vector2 vector2)
        {
            return vector1.X * vector2.X + vector1.Y * vector2.Y;
        }


        /// <summary>
        /// Retourne la longueur du vecteur.
        /// </summary>
        /// <returns>Longueur du vecteur.</returns>
        public double Length()
        {
            double ls = X * X + Y * Y;
            return Math.Sqrt(ls);
        }

        /// <summary>
        /// Restreint un vecteur entre un minimum et un maximum.
        /// </summary>
        /// <param name="vector">Vecteur source.</param>
        /// <param name="min">Vecteur minimum.</param>
        /// <param name="max">Vecteur maximum.</param>
        public static Vector2 Clamp(Vector2 vector, Vector2 min, Vector2 max)
        {
            double x = vector.X;
            x = (x > max.X) ? max.X : x;
            x = (x < min.X) ? min.X : x;
            double y = vector.Y;
            y = (y > max.Y) ? max.Y : y;
            y = (y < min.Y) ? min.Y : y;
            return new Vector2(x, y);
        }

        /// <summary>
        /// Fait une interpolation linéaire entre deux vecteurs basées sur un élargissement.
        /// </summary>
        /// <param name="vector1">Premier vecteur.</param>
        /// <param name="vector2">Deuxième vecteur.</param>
        /// <param name="amount">Valeur entre 0 et 1 indiquant le poids de deuxième vecteur.</param>
        /// <returns>Le vecteur interpolé.</returns>
        public static Vector2 Lerp(Vector2 vector1, Vector2 vector2, float amount)
        {
            return new Vector2(
                vector1.X + (vector2.X - vector1.X) * amount,
                vector1.Y + (vector2.Y - vector1.Y) * amount);
        }

        /// <summary>
        /// Retourne un vecteur avec le même direction que celui donné, mais de longueur 1.
        /// </summary>
        /// <param name="vector">Vecteur à normaliser.</param>
        /// <returns>Vecteur normalisé.</returns>
        public static Vector2 Normalize(Vector2 vector)
        {
            double ls = vector.X * vector.X + vector.Y * vector.Y;
            double invNorm = 1.0 / Math.Sqrt(ls);
            return new Vector2(vector.X * invNorm, vector.Y * invNorm);
        }

        /// <summary>
        /// Retourne la réflexion d'un vecteur depuis une surface qui a la normale spécifiée.
        /// </summary>
        /// <param name="vector">Vecteur source.</param>
        /// <param name="normal">Normale de la surface depuis laquelle la réflexion est effectuée.</param>
        /// <returns>Vecteur reflété.</returns>
        public static Vector2 Reflect(Vector2 vector, Vector2 normal)
        {
            double dot = vector.X * normal.X + vector.Y * normal.Y;
            return new Vector2(vector.X - 2.0f * dot * normal.X, vector.Y - 2.0f * dot * normal.Y);
        }

        public static Vector2 AngleToVector(double angle)
        {
            return new Vector2(MathExtension.DCos(angle), MathExtension.DSin(angle));
        }
        public static float VectorToAngle(Vector2 vector)
        {
            return MathExtension.RadianToDegree(Math.Atan2(vector.X, vector.Y));
        }

        /// <summary>
        /// Redéfinition du HashCode.
        /// </summary>
        /// <returns>HashCode du vecteur.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Redéfinition du protocole d'égalité.
        /// </summary>
        /// <param name="other"></param>
        /// <returns>Vrai si les objets sont identiques.</returns>
        public override bool Equals(object other)
        {
            if (ReferenceEquals(other, null)) return false;
            if (ReferenceEquals(other, this)) return true;
            if (GetType() != other.GetType()) return false;
            return Equals((Vector2) other);
        }

        /// <summary>
        /// Redéfinition du protocole d'égalité.
        /// </summary>
        /// <param name="other"></param>
        /// <returns>Vrai si les vecteurs ont les mêmes caractéristiques.</returns>
        public bool Equals(Vector2 other)
        {
            if (X == other.X) return Y == other.Y;
            return false;
        }

        /// <summary>
        /// Implémentation de IFormattable.
        /// </summary>
        /// <param name="format">Format à utiliser (par défaut : "X, Y").</param>
        /// <param name="formatProvider">Provider à utiliser (par défaut : null).</param>
        /// <returns>Vecteur sous forme de string au format désiré.</returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (string.IsNullOrEmpty(format)) format = "X, Y";
            StringBuilder builder = new StringBuilder();

            foreach(char c in format)
            {
                if(char.ToUpper(c) == 'X')
                {
                    builder.Append(X);
                    continue;
                }
                if (char.ToUpper(c) == 'Y')
                {
                    builder.Append(Y);
                    continue;
                }
                builder.Append(c);
            }

            return builder.ToString();
        }

        /// <summary>
        /// Méthode ToString.
        /// </summary>
        /// <returns>Vecteur sous forme de string au format de base.</returns>
        public override string ToString()
        {
            return ToString(null, null);
        }
    }
}
