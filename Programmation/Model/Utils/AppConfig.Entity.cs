﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Utils
{
    public partial class AppConfig
    {
        // |-----------------------------------------------------------------------|
        // |                                                                       |
        // |                                                                       |
        // | ----------========== Commun à toutes les entités ==========---------- |
        // |                                                                       |
        // |                                                                       |
        // |-----------------------------------------------------------------------|

        /// <summary>
        /// Retourne l'écart maximum entre deux entité pour qu'une des deux puisse manger l'autre.
        /// </summary>
        public virtual float MaxDistanceWithTargetToEat { get => 1; }

        /// <summary>
        /// Position d'un joueur lors de son apparition.
        /// </summary>
        public virtual Vector3 IPlayerSpawnViewPosition { get => Vector3.Zero; }

        /// <summary>
        /// Taille maximum d'une entité qui peut mourrir en explosant.
        /// </summary>
        public virtual float IDieMaxSize { get => 10; }

        /// <summary>
        /// Chance pour une bactérie d'être infectée à chaque tirage.
        /// </summary>
        public virtual float IInfectableDrawProbability { get => 0.01f; }

        /// <summary>
        /// Quantité de vie enlevé à chaque tick sur une entité infectée.
        /// </summary>
        public virtual float IInfectableDamagePerTick { get => 1; }

        /// <summary>
        /// Diviseur de la taille d'une entité lorsqu'elle se divise.
        /// </summary>
        public virtual float IReproduceSizeReductionCoef { get => 2; }

        /// <summary>
        /// Variation de la direction prise par une entité se déplaçant avec
        /// SimpleMove comme module de déplacement.
        /// Plus ce nombre est grand et plus leurs déplacements seront chaotiques.
        /// A l'inverse, plus il est petit et plus les déplacements seront linéaires.
        /// </summary>
        public virtual int SimpleMoveVariation { get => 5; }

        /// <summary>
        /// Probabilité de tourner dans un sens plutôt que dans l'autre lorsque l'entité est bloquée
        /// (c'est à dire qu'elle essaye d'avancer en dehors de la map).
        /// </summary>
        public virtual int MoveRotationDrawProbability { get => 50; }

        // |-----------------------------------------------------------------------|
        // |                                                                       |
        // |                                                                       |
        // | ----------==========          Bactéries          ==========---------- |
        // |                                                                       |
        // |                                                                       |
        // |-----------------------------------------------------------------------|

        /// <summary>
        /// Taux d'apparition des bactéries lors de la génération d'un nouveau chunk.
        /// </summary>
        public virtual float SimpleEntityGeneratorBacteriaSpawnRate { get => 100; }

        /// <summary>
        /// Vie de base d'une bacterie.
        /// </summary>
        public virtual float BacteriaBaseMaxHealth { get => 100; }

        /// <summary>
        /// Taille de base d'une bactérie.
        /// </summary>
        public virtual float BacteriaBaseSize { get => 2; }

        /// <summary>
        /// Vitesse de base d'une bactérie.
        /// </summary>
        public virtual float BacteriaBaseSpeed { get => 0.5f; }

        /// <summary>
        /// Taille requise pour la reproduction d'une bactérie.
        /// </summary>
        public virtual float BacteriaRequiredSizeToReproduce { get => 3; }

        /// <summary>
        /// Vitalité requise pour la reproduction d'une bactérie.
        /// </summary>
        public virtual float BacteriaRequiredLifePercentageToReproduce { get => 85; }

        // |-----------------------------------------------------------------------|
        // |                                                                       |
        // |                                                                       |
        // | ----------==========          Euplotes           ==========---------- |
        // |                                                                       |
        // |                                                                       |
        // |-----------------------------------------------------------------------|

        /// <summary>
        /// Taux d'apparition des euplotes lors de la génération d'un nouveau chunk.
        /// </summary>
        public virtual float SimpleEntityGeneratorEuploteSpawnRate { get => 20; }

        /// <summary>
        /// Vie de base d'une euplote.
        /// </summary>
        public virtual float EuploteBaseMaxHealth { get => 100; }

        /// <summary>
        /// Taille de base d'une euplote.
        /// </summary>
        public virtual float EuploteBaseSize { get => 2; }

        /// <summary>
        /// Retourne la vitesse de base d'un euplote.
        /// </summary>
        public virtual float EuploteBaseSpeed { get => 1; }

        /// <summary>
        /// Taille requise pour la reproduction d'un euplote.
        /// </summary>
        public virtual float EuploteRequiredSizeToReproduce { get => 5; }

        /// <summary>
        /// Vitalité requise pour la reproduction d'un euplote.
        /// </summary>
        public virtual float EuploteRequiredLifePercentageToReproduce { get => 85; }
    }
}
