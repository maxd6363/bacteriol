﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Utils
{
    public partial class AppConfig
    {
        /// <summary>
        /// Nom du fichier qui liste les parties.
        /// </summary>
        public virtual string GamesFileLocation { get => "Games.xml"; }
    }
}
