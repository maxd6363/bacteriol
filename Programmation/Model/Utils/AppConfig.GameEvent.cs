﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Utils
{
    public partial class AppConfig
    {
        /// <summary>
        /// Nom de l'événement de la marée noire.
        /// </summary>
        public virtual string OilSpillEventName { get => "Marée noire"; }

        /// <summary>
        /// Coefficient de "violence" de la polution.
        /// </summary>
        public virtual float OilSpillGameEventCoef { get => 0.1f; }
    }
}
