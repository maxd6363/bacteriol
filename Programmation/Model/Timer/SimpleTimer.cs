﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Timers;

namespace Model.Timer
{
    /// <summary>
    /// Implémentation basique de l'interface ITimer.
    /// </summary>
    [DataContract(IsReference = true)]
    public class SimpleTimer : ITimer
    {
        /// <summary>
        /// Retourne ou modifie la fréquence du timer (en millisecondes).
        /// </summary>
        [DataMember]
        public int Frequency { get; private set; }

        /// <summary>
        /// Retourne ou modifie le timer utilisé.
        /// </summary>
        [DataMember]
        private System.Timers.Timer _Timer { get; set; }

        private DateTime _LastEvent;

        /// <summary>
        /// Constructeur du timer.
        /// </summary>
        /// <param name="frequency">Fréquence du timer en millisecondes.</param>
        public SimpleTimer(int frequency)
        {
            Frequency = frequency; 
            _Timer = new System.Timers.Timer(Frequency);
            _Timer.Elapsed += OnTimedEvent;
            _Timer.Enabled = false;
            _Timer.AutoReset = true;
        }

        /// <summary>
        /// Active le timer.
        /// </summary>
        public void Start()
        {
            _Timer.Start();
            _LastEvent = DateTime.Now;
        }

        /// <summary>
        /// Désactive le timer.
        /// </summary>
        public void Stop()
        {
            _Timer.Stop();
        }

        /// <summary>
        /// Appellée pour former chaque tick.
        /// </summary>
        /// <param name="source">Source de l'événement du timer.</param>
        /// <param name="e">Arguments de l'événement.</param>
        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            OnTick.Invoke(this, DateTime.Now - _LastEvent);
            _LastEvent = DateTime.Now;
        }

        /// <summary>
        /// Déclanché régulièrement après un certain laps de temps depuis le dernier déclanchement.
        /// Le temps écoulé depuis le dernier tick est passé en argument.
        /// </summary>
        public event EventHandler<TimeSpan> OnTick;
    }
}
