﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Timer
{
    /// <summary>
    /// Interface qui définit ce qu'est un timer (horloge).
    /// </summary>
    public interface ITimer
    {
        /// <summary>
        /// Retourne ou modifie la fréquence du timer.
        /// </summary>
        int Frequency { get; }

        /// <summary>
        /// Active le timer.
        /// </summary>
        void Start();

        /// <summary>
        /// Désactive le timer.
        /// </summary>
        void Stop();

        /// <summary>
        /// Déclanché régulièrement après un certain laps de temps depuis le dernier déclanchement.
        /// </summary>
        event EventHandler<TimeSpan> OnTick;
    }
}
