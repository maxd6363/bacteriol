﻿using Model.Core.Entity;
using Model.Core.Game;
using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// 
/// </summary>
namespace Model.Statistics
{
    /// <summary>
    /// 
    /// </summary>
    class SimpleStatistics : IStatistics
    {
        /// <summary>
        /// Retourne le nombre de bactéries dans la partie actuelle.
        /// </summary>
        public int NbBacteria(IGame game)
        {
            int totalBacterias = 0;
            foreach (Bacteria bacteria in game.World.Bacterias)
            {
                totalBacterias++;
            }
            return totalBacterias;
        }

        /// <summary>
        /// Retourne le nombre d'euplotes dans la partie actuelles.
        /// </summary>
        public int NbEuplote(IGame game)
        {
            int totalEuplotes = 0;
            foreach (Euplote euplote in game.World.Euplotes)
            {
                totalEuplotes++;
            }
            return totalEuplotes;
        }

        /// <summary>
        /// Retourne le pourcentage de bactéries infectées parmis toutes les bactéries.
        /// </summary>
        public float PctInfectedBacteria(IGame game)
        {
            float totalBacterias = 0;
            float infectedbacterias = 0;
            foreach (Bacteria bacteria in game.World.Bacterias)
            {
                totalBacterias++;
                if (bacteria.InfectableModule.Infected)
                {
                    infectedbacterias++;
                }
            }
            return infectedbacterias * 100 / totalBacterias;
        }
    }
}
