﻿using Model.Core.Game;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Statistics
{
    /// <summary>
    /// Interface qui définit ce qu'est un générateur de statistiques.
    /// </summary>
    public interface IStatistics
    {
        /// <summary>
        /// Retourne le nombre de bactéries dans la partie actuelle.
        /// </summary>
        int NbBacteria(IGame game);

        /// <summary>
        /// Retourne le nombre d'euplotes dans la partie actuelles.
        /// </summary>
        int NbEuplote(IGame game);

        /// <summary>
        /// Retourne le pourcentage de bactéries infectées parmis toutes les bactéries.
        /// </summary>
        float PctInfectedBacteria(IGame game);
    }
}
