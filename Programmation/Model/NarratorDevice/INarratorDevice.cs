﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.NarratorDevice
{
    /// <summary>
    /// Interface qui définit ce qu'est une narrateur.
    /// A redéfinir dans la vue.
    /// </summary>
    interface INarratorDevice
    {
        /// <summary>
        /// Commence la lecture orale d'une phrase.
        /// </summary>
        /// <param name="sentence">Phrase à lire.</param>
        void PlaySentence(string sentence);
    }
}
