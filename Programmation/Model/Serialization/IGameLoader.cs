﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Core.Game;

namespace Model.Serialization
{
    /// <summary>
    /// Interface qui définit ce qu'est un désérialiseur de partie.
    /// </summary>
    public interface IGameLoader
    {
        /// <summary>
        /// Charge en mémoire la liste des parties.
        /// </summary>
        /// <returns>Liste des parties chargées.</returns>
        IEnumerable<IGame> Load();
    }
}
