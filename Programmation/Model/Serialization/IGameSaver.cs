﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Core.Game;

namespace Model.Serialization
{
    /// <summary>
    /// Interface qui définit ce qu'est un sérialiseur de partie.
    /// </summary>
    public interface IGameSaver
    {
        /// <summary>
        /// Sauvegarde les parties sur le disque dur.
        /// </summary>
        /// <param name="games">Liste des parties à sauvegarder.</param>
        void Save(IEnumerable<IGame> games);
    }
}
