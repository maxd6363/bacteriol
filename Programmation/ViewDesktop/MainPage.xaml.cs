﻿using Urho;

namespace ViewDesktop
{
    public sealed partial class MainPage
    {

        public MainPage()
        {
            InitializeComponent();
            LoadApplication(new BacteriolApplication.App());

        }
    }
}
