﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Serialization;
using Model.Core.Game;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;
using System.Runtime.Serialization;
using Model.Utils;

namespace Serialization
{
    public class SimpleXMLGameLoader : IGameLoader
    {
        /// <summary>
        /// Retourne ou modifie le répertoire de travail du sérialiseur.
        /// </summary>
        private string WorkDir { get; set; }

        /// <summary>
        /// Charge en mémoire la liste des parties.
        /// </summary>
        /// <returns>Liste des parties chargées.</returns>
        public IEnumerable<IGame> Load()
        {
            Directory.SetCurrentDirectory(WorkDir);
            DataContractSerializer serializer = new DataContractSerializer(typeof(List<SimpleGame>), new DataContractSerializerSettings() { PreserveObjectReferences = true });
            List<SimpleGame> games = new List<SimpleGame>();
            if (!File.Exists(AppConfig.UsedConfig.GamesFileLocation))
            {
                throw new FileNotFoundException($"Le fichier {AppConfig.UsedConfig.GamesFileLocation} n'a pas été trouvé dans le dossier {Environment.CurrentDirectory}.");
            }
            using (Stream stream = File.OpenRead(AppConfig.UsedConfig.GamesFileLocation))
            {
                games = serializer.ReadObject(stream) as List<SimpleGame>;
            }
            return games;
        }

        /// <summary>
        /// Constructeur du sérialiseur.
        /// </summary>
        /// <param name="workDir">Répertoire de travail.</param>
        public SimpleXMLGameLoader(string workDir)
        {
            WorkDir = workDir;
        }
    }
}
