﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Core.Game;
using Model.Serialization;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Xml;
using Model.Utils;

namespace Serialization
{
    public class SimpleXMLGameSaver : IGameSaver
    {
        /// <summary>
        /// Retourne ou modifie le répertoire de travail du sérialiseur.
        /// </summary>
        private string WorkDir { get; set; }

        /// <summary>
        /// Sauvegarde les parties sur le disque dur.
        /// </summary>
        /// <param name="games">Liste des parties à sauvegarder.</param>
        public void Save(IEnumerable<IGame> games)
        {
            if (!Directory.Exists(WorkDir))
            {
                Directory.CreateDirectory(WorkDir);
            }
            Directory.SetCurrentDirectory(WorkDir);
            DataContractSerializer serializer = new DataContractSerializer(typeof(List<SimpleGame>), new DataContractSerializerSettings() { PreserveObjectReferences = true });
            XmlWriterSettings writterSettings = new XmlWriterSettings() { Indent = true };
            using (XmlWriter writer = XmlWriter.Create(AppConfig.UsedConfig.GamesFileLocation, writterSettings))
            {
                serializer.WriteObject(writer, games as List<SimpleGame>);
            }
        }

        /// <summary>
        /// Constructeur du sérialiseur.
        /// </summary>
        /// <param name="workDir">Répertoire de travail.</param>
        public SimpleXMLGameSaver(string workDir)
        {
            WorkDir = workDir;
        }
    }
}
