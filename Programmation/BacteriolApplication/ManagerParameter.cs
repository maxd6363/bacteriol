﻿using Model.Core.World;

namespace BacteriolApplication
{
    public class ManagerParameter
    {
        public IWorldParameters Parameters { get; set; }


        public ManagerParameter() : this(null)
        {

        }

        public ManagerParameter(IWorldParameters parameters)
        {
            Parameters = parameters;
        }
    }

}
