﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BacteriolApplication.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ControlePage : ContentPage
    {
        public ControlePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false); //working but not as intented

        }

        private async void ButtonRetour_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PopAsync();
        }

    }

}