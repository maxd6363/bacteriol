﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BacteriolApplication.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContextePage : ContentPage
    {
        public ContextePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false); //working but not as intented
        }

        private async void ButtonLancer_Clicked(object sender, EventArgs e)
        {
            Image scaleImage = this.FindByName("scaleImage") as Image;
            Label fadingLabel = this.FindByName("fadingLabel") as Label;

            await fadingLabel.FadeTo(0, 100);
            await scaleImage.ScaleTo(3, 150);
            for (int i=4; i<=10; i++)
            {
                uint t = (uint)((i + 2) * 10);
                await scaleImage.ScaleTo(i, t);
            }
            await Navigation.PushAsync(new GamePage());
            await scaleImage.ScaleTo(1, 0);
        }
    }
}