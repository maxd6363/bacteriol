﻿using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BacteriolApplication.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OptionPage : ContentPage
    {

     

        public OptionPage()
        {
        
          



            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false); //working but not as intented
            BindingContext = this;

        }


        private async void ButtonControle_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new ControlePage());
        }

        private async void ButtonRetour_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private async void ButtonAccessibilite_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new AccessibilitePage());
        }
    }
}