﻿using Model.Core.World;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BacteriolApplication.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class JouerPage : ContentPage
    {

        private ManagerParameter ManagerParameter { get; }

        public JouerPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false); //working but not as intented
            ManagerParameter = new ManagerParameter();
        }

        private void ButtonRetour_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private async void ButtonCreer_Clicked(object sender, EventArgs e)
        {
            ManagerParameter.Parameters = new SimpleWorldParameters((float)TemperatureSlider.Value, (float)SaliniteSlider.Value, (float)OxygeneSlider.Value, 1, 0,(float)NourritureSlider.Value);

            await Navigation.PushAsync(new ContextePage());
        }
    }
}