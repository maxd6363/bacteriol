﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BacteriolApplication.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AccessibilitePage : ContentPage
    {
        public AccessibilitePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false); //working but not as intented

        }

        private void LangueBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private async void ButtonRetour_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}