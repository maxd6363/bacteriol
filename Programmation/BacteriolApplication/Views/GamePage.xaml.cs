﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BacteriolApplication.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GamePage : ContentPage
    {

        public GamePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false); //working but not as intented

        }



        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await UrhoSurface.Show<UrhoApp.BacteriolApplication>(new Urho.ApplicationOptions(assetsFolder: "Data"));
            

        }


    }
}