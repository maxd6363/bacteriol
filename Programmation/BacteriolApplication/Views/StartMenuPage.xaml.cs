﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BacteriolApplication.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StartMenuPage : ContentPage
    {

        public StartMenuPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false); //working but not as intented
        }



        private async void ButtonJouer_Clicked(object sender, EventArgs e)
        {
            await NextMenu_Animation(translateButtonJouer);
            await Navigation.PushAsync(new JouerPage());
        }


        private async void ButtonOption_Clicked(object sender, EventArgs e)
        {
            await NextMenu_Animation(translateButtonOption);
            await Navigation.PushAsync(new OptionPage());
        }


        private async 
        Task
        NextMenu_Animation(Button button)
        {
            await button.TranslateTo(20, 0, 80, Easing.BounceIn);
            await button.TranslateTo(0, 0);
            await xtranslateBoxContainer.ScaleXTo(7, 200);
            await xtranslateBoxContainer.ScaleXTo(1, 0);
        }

    }
}