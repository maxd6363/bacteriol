﻿using BacteriolApplication.UrhoApp.Utils;
using System.Collections.Generic;
using System.Diagnostics;
using Urho;
using Urho.Audio;

namespace BacteriolApplication.UrhoApp.Sound
{
    public class MusicMixer : SoundSource
    {
        private IList<string> Musics { get; }

        private int CurrentMusicIndex;
        private float Delta;

        public MusicMixer() : base()
        {

            BacteriolApplication.Current.Scene.CreateChild().AddComponent(this);
            Musics = new List<string>();
            CurrentMusicIndex = 0;
            Delta = 0f;
            var _attributs = typeof(Assets.Sounds).GetFields();
            foreach (var field in _attributs)
            {
                if ((field.GetValue(null) as string).Contains("music"))
                {
                    Musics.Add(field.GetValue(null) as string);
                }
            }

            Application.Current.Update += Update;
            Gain = UrhoConfig.UsedConfig.MusicGain;

            StartMusic();
        }

        private void Update(UpdateEventArgs obj)
        {
            Delta += obj.TimeStep;
            if(Delta >= UrhoConfig.UsedConfig.MusicDeltaCheck)
            {
                Delta = 0f;
                CurrentMusicIndex = (CurrentMusicIndex + 1) % Musics.Count;
                StartMusic();
            }
        }

        public void StartMusic()
        {
            if (!Playing)
            {
                Play(Application.Current.ResourceCache.GetSound(Musics[CurrentMusicIndex]));
                Debug.WriteLine($"Now playing : {Musics[CurrentMusicIndex]}");
            }
        }
    }
}
