﻿using Urho;
using Urho.Gui;

namespace BacteriolApplication.UrhoApp.UIElement
{
    public class DropMenu : DropDownList
    {
        public DropMenu()
        {
            BacteriolApplication.Current.UI.Root.AddChild(this);
            Visible = true;
            Size = new IntVector2(300, 80);

            var textPause = new Text
            {
                Value = "PAUSE",
                FontSize = 50
            };


            AddItem(textPause);


            Position = IntVector2.Zero;

        }
    }
}
