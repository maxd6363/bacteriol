﻿using BacteriolApplication.UrhoApp.Utils;
using Model.Core.Player;
using Model.Utils;
using System;
using System.Diagnostics;
using Urho;
using Urho.Gui;

namespace BacteriolApplication.UrhoApp.UIElement
{
    public class HealthBar : BorderImage
    {
        private UI UI { get => Application.Current.UI; }
        private Text InternalText { get; set; }
        private IPlayer Player { get; }
        private float Value;

        public HealthBar(IPlayer player)
        {
            Player = player;
            Value = -1;
            var _style = Application.Current.ResourceCache.GetXmlFile(Assets.XML.DefaultStyle);
            UI.Root.SetDefaultStyle(_style);
            UI.LoadLayoutToElement(this, Application.Current.ResourceCache, Assets.XML.ProgressBar);
            UI.Root.AddChild(this);

            VerticalAlignment = VerticalAlignment.Bottom;
            HorizontalAlignment = HorizontalAlignment.Left;

            InternalText = GetChild("S_Text", true) as Text;

            Hide();
            Application.Current.Update += Update;
        }

        private void Update(UpdateEventArgs obj)
        {
            if (Player != null && Player.EntityControlled != null)
            {
                if (Value != Player.EntityControlled.Health)
                {
                    InternalText.Value = $"{(int)(Player.EntityControlled.Health / (AppConfig.UsedConfig.EuploteBaseMaxHealth / 100))} %";
                }
                Value = Player.EntityControlled.Health;
            }
        }


        public void Delete()
        {
            try
            {
                Application.Current.Update -= Update;
                BacteriolApplication.Current.UI.Root.RemoveChild(this);
            }
            catch (NullReferenceException)
            {
                Debug.WriteLine("Cannot delete entity from UI");
            }
        }

        public void Show()
        {
            Opacity = 1;
            InternalText.Opacity = 1;
        }

        public void Hide()
        {
            Opacity = 0;
            InternalText.Opacity = 0;
        }

    }
}
