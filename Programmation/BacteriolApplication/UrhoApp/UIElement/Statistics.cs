﻿using BacteriolApplication.UrhoApp.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using Urho.Gui;

namespace BacteriolApplication.UrhoApp.UIElement
{
    public class Statistics : Text
    {
        private Model.Statistics.IStatistics GameStatistics { get; set; }

        private float Delta;

        public Statistics()
        {
            Delta = 0f;
            SetFont(Assets.Fonts.AnonymousPro, UrhoConfig.UsedConfig.DefaultFontSize);
            HorizontalAlignment = HorizontalAlignment.Right;
            VerticalAlignment = VerticalAlignment.Bottom;
            TextAlignment = HorizontalAlignment.Center;
            GameStatistics = new Model.Factory.SimpleFactory().BuildIStatistics();

            BacteriolApplication.Current.Update += Update;
            BacteriolApplication.Current.UI.Root.AddChild(this);
        }


        private void Update(Urho.UpdateEventArgs obj)
        {
            Delta += obj.TimeStep;
            if(Delta >= UrhoConfig.UsedConfig.RefreshStatistics)
            {
                Delta = 0;
                Value = string.Format(UrhoConfig.UsedConfig.StatisticsFormat, GameStatistics.NbBacteria(BacteriolApplication.Current.Manager.CurrentGame), GameStatistics.NbEuplote(BacteriolApplication.Current.Manager.CurrentGame));
            }
            
        }
    }
}
