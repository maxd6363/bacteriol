﻿using BacteriolApplication.UrhoApp.Entity.Bacteria;
using BacteriolApplication.UrhoApp.Entity.Euplote;
using BacteriolApplication.UrhoApp.EntityGraphic;
using BacteriolApplication.UrhoApp.Player;
using BacteriolApplication.UrhoApp.Utils;
using Model.Core.Entity;
using Model.Factory;
using Model.Manager;
using Serialization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Urho;
using Urho.Physics;
using System.Linq;
using BacteriolApplication.UrhoApp.Entity;
using BacteriolApplication.UrhoApp.UIElement;
using BacteriolApplication.UrhoApp.Sound;
using BacteriolApplication.UrhoApp.EntityGraphic.Gradient;

namespace BacteriolApplication.UrhoApp
{
    public class BacteriolApplication : Application
    {
        /// <summary>
        /// hiding current from urho
        /// </summary>
        public new static BacteriolApplication Current { get => Application.Current as BacteriolApplication; }
        public Scene Scene { get; private set; }
        public Camera Camera { get => CurrentPlayer.Camera; }
        public MouseEntity Mouse { get; private set; }
        public IManager Manager { get; }
        public SimpleUrhoPlayer CurrentPlayer { get; private set; }
        public IList<UrhoBacteria> UrhoBacterias { get; }
        public IList<UrhoEuplote> UrhoEuplotes { get; }
        public Factory.Factory Factory { get; private set; }
        public ModelEventHandler ModelEventHandler { get; private set; }
        public Statistics Statistics { get; private set; }
        public TextFPS HUDFps { get; private set; }
        public ShortcutHandler ShortcutHandler { get; private set; }
        public MusicMixer MusicMixer { get; private set; }
        public GradientVisualizer GradientVisualizer { get; private set; }

        public BacteriolApplication(ApplicationOptions options) : base(options)
        {
            Stopped += BacteriolApplication_Stopped;

            UrhoBacterias = new List<UrhoBacteria>();
            UrhoEuplotes = new List<UrhoEuplote>();

            try
            {
                Manager = new SimpleManager(new SimpleFactory(), new SimpleXMLGameLoader(UrhoConfig.UsedConfig.WorkingDirectory), new SimpleXMLGameSaver(UrhoConfig.UsedConfig.WorkingDirectory));
            }
            catch (Exception e)
            {
                throw e;
            }


        }





        protected override void Start()
        {
            base.Start();
            Engine.PostRenderUpdate += Engine_PostRenderUpdate;

            Manager.CreateNewGame("Game");
            Manager.SetCurrentGame("Game");
            Scene = new SceneCustom();
            HUDFps = new TextFPS();
            ShortcutHandler = new ShortcutHandler();
            Mouse = new MouseEntity();
            MusicMixer = new MusicMixer();

            CurrentPlayer = new SimpleUrhoPlayer();
            Factory = new Factory.SimpleFactory();
            ModelEventHandler = new SimpleModelEventHandler();
            Statistics = new Statistics();


            Manager.CurrentGame.Setup();
            Manager.CurrentGame.Start();


            GradientVisualizer = new SimpleGradientVisualizer();

        }

        protected override void Stop()
        {
            Manager.CurrentGame.End();
            //Manager.SaveGames();
            base.Stop();
        }

        private void Engine_PostRenderUpdate(PostRenderUpdateEventArgs obj)
        {
            if (UrhoConfig.UsedConfig.DrawDebug)
            {
                Renderer.DrawDebugGeometry(true);
                var debugRendererComp = Scene.GetComponent<DebugRenderer>();
                Scene.GetComponent<PhysicsWorld>()?.DrawDebugGeometry(debugRendererComp, false);
            }
        }



        public void DeleteEntityModel(IEntity entity)
        {
            var _toDeletebacteria = UrhoBacterias.First(bacteria => bacteria.Entity == entity);
            if (_toDeletebacteria != null)
                UrhoBacterias.Remove(_toDeletebacteria);

            var _toDeleteEuplote = UrhoEuplotes.First(euplote => euplote.Entity == entity);
            if (_toDeleteEuplote != null)
                UrhoEuplotes.Remove(_toDeleteEuplote);

        }



        private void BacteriolApplication_Stopped()
        {
            Exit();
        }

    }
}
