﻿using System;
using Urho;

namespace BacteriolApplication.UrhoApp.Inputs
{
    public abstract class AbstractInput
    {
        public bool IsActive { get; protected set; }
        public bool IsConnected { get; protected set; }

        protected float? degreeLookAt;
        protected float boost;
        protected bool divide;





        protected Action<AbstractInput> _takeOverAction;
        public event Action<AbstractInput> TakeOverAction
        {
            add => _takeOverAction += value;
            remove => _takeOverAction -= value;
        }


        protected Action<InputsActionArgs> _inputPressed;
        public event Action<InputsActionArgs> InputPressed
        {
            add => _inputPressed += value;
            remove => _inputPressed -= value;
        }






        public AbstractInput()
        {
            IsActive = false;
            IsConnected = false;

            degreeLookAt = 0f;
            boost = 0f;
            divide = false;

            Application.Current.Update += Update;

        }



        public abstract void Update(UpdateEventArgs obj);




    }
}