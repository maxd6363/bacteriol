﻿using BacteriolApplication.UrhoApp.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Urho;
using Windows.Gaming.Input;

namespace BacteriolApplication.UrhoApp.Inputs
{
    public class InputGamepad : AbstractInput
    {

        private Gamepad gamepad;

        public InputGamepad()
        {
            Gamepad.GamepadAdded += Gamepad_GamepadAdded;
            Gamepad.GamepadRemoved += Gamepad_GamepadRemoved;

            try
            {
                gamepad = Gamepad.Gamepads[0];
                IsConnected = true;

            }
            catch (IndexOutOfRangeException) { }
            catch (ArgumentOutOfRangeException) { }

        }

        private void Gamepad_GamepadRemoved(object sender, Gamepad e)
        {
            gamepad = null;
            IsConnected = false;
            IsActive = false;
            Debug.WriteLine("Gamepad Disconnected");
        }

        private void Gamepad_GamepadAdded(object sender, Gamepad e)
        {
            gamepad = e;
            IsConnected = true;
            Debug.WriteLine("Gamepad Connected");
        }


        public override void Update(UpdateEventArgs obj)
        {
            if (gamepad == null) return;
            if (!IsConnected) return;
            HandleTakeOver();


            bool _div = HandleDivision();
            bool _boost = HandleBoost();
            bool _dir = HandleDirection();

            IsActive = _div || _boost || _dir;
            if (IsActive)
            {
                _inputPressed?.Invoke(new InputsActionArgs(degreeLookAt, boost, divide));
            }
        }


        private void HandleTakeOver()
        {
            if (gamepad.GetCurrentReading().Buttons == GamepadButtons.A || gamepad.GetCurrentReading().RightTrigger >= 0.3 || gamepad.GetCurrentReading().LeftThumbstickX >= 0.5 || gamepad.GetCurrentReading().LeftThumbstickX <= -0.5 || gamepad.GetCurrentReading().LeftThumbstickY >= 0.5 || gamepad.GetCurrentReading().LeftThumbstickY <= -0.5)
            {
                _takeOverAction?.Invoke(this);
            }

        }

        private bool HandleDirection()
        {
            if (gamepad.GetCurrentReading().LeftThumbstickX >= 0.5 || gamepad.GetCurrentReading().LeftThumbstickX <= -0.5 || gamepad.GetCurrentReading().LeftThumbstickY >= 0.5 || gamepad.GetCurrentReading().LeftThumbstickY <= -0.5)
            {
                degreeLookAt = (float)(Math.Atan2(gamepad.GetCurrentReading().LeftThumbstickX, -gamepad.GetCurrentReading().LeftThumbstickY) * 180 / Math.PI) - 90;
                return true;
            }
            degreeLookAt = null;
            return false;
        }

        private bool HandleBoost()
        {
            if (gamepad.GetCurrentReading().RightTrigger >= 0.3)
            {
                boost = (float)(UrhoConfig.UsedConfig.EuploteBaseSpeed * UrhoConfig.UsedConfig.EuploteBoostRatio * gamepad.GetCurrentReading().RightTrigger);
                return true;
            }
            boost = UrhoConfig.UsedConfig.EuploteBaseSpeed;
            return false;

        }

        private bool HandleDivision()
        {
            if (gamepad.GetCurrentReading().Buttons == GamepadButtons.A)
            {
                divide = true;
                return true;
            }
            divide = false;
            return false;

        }
    }
}
