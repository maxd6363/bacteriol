﻿using BacteriolApplication.UrhoApp.Utils;
using System.Diagnostics;
using Urho;

namespace BacteriolApplication.UrhoApp.Inputs
{
    public class InputKeyboard : AbstractInput
    {
        readonly KeyMapping Mapping = KeyMapping.Mapping;

        public InputKeyboard()
        {
            IsConnected = true;
        }

        private bool GetKeyDown(Key k)
        {
            return Application.Current.Input.GetKeyDown(k);
        }


        public override void Update(UpdateEventArgs obj)
        {
            if (!IsConnected) return;
            HandleTakeOver();


            bool _div = HandleDivision();
            bool _boost = HandleBoost();
            bool _dir = HandleDirection();

            IsActive = _div || _boost || _dir;

            if (IsActive)
            {
                _inputPressed?.Invoke(new InputsActionArgs(degreeLookAt, boost, divide));
            }

        }

        private void HandleTakeOver()
        {
            if (GetKeyDown(Key.Space) || GetKeyDown(Key.Z) || GetKeyDown(Key.Q) || GetKeyDown(Key.S) || GetKeyDown(Key.D))
            {
                _takeOverAction?.Invoke(this);
            }
        }

        private bool HandleDivision()
        {
            if (GetKeyDown(Mapping.Divide))
            {
                divide = true;
                return true;
            }

            divide = false;
            return false;

        }

        private bool HandleBoost()
        {
            if (GetKeyDown(Mapping.Boost))
            {
                boost = UrhoConfig.UsedConfig.EuploteBaseSpeed * UrhoConfig.UsedConfig.EuploteBoostRatio;
                return true;
            }
            else
            {
                boost = UrhoConfig.UsedConfig.EuploteBaseSpeed; 
                return false;
            }
        }


        private bool HandleDirection()
        {

            if (GetKeyDown(Mapping.Forward) && GetKeyDown(Mapping.Left))
            {
                degreeLookAt = 135;
                return true;
            }

            if (GetKeyDown(Mapping.Left) && GetKeyDown(Mapping.Backward))
            {
                degreeLookAt = -135;
                return true;
            }

            if (GetKeyDown(Mapping.Backward) && GetKeyDown(Mapping.Right))
            {
                degreeLookAt = -45;
                return true;
            }

            if (GetKeyDown(Mapping.Right) && GetKeyDown(Mapping.Forward))
            {
                degreeLookAt = 45;
                return true;
            }


            if (GetKeyDown(Mapping.Forward))
            {
                degreeLookAt = 90;
                return true;
            }

            if (GetKeyDown(Mapping.Backward))
            {
                degreeLookAt = -90;
                return true;
            }

            if (GetKeyDown(Mapping.Left))
            {
                degreeLookAt = 180;
                return true;
            }

            if (GetKeyDown(Mapping.Right))
            {
                degreeLookAt = 0;
                return true;
            }
            degreeLookAt = null;
            return false;
        }

    }
}
