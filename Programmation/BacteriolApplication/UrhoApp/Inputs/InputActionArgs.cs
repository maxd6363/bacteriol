﻿namespace BacteriolApplication.UrhoApp.Inputs
{
    public struct InputsActionArgs
    {
        public float? DegreeLookAt { get; set; }
        public float Boost { get; set; }
        public bool Divide { get; set; }


        public InputsActionArgs(float? degreeLookAt, float boost, bool divide)
        {
            DegreeLookAt = degreeLookAt;
            Boost = boost;
            Divide = divide;
        }

        public override string ToString()
        {
            return $"{DegreeLookAt} - {Boost} - {Divide}";
        }

    }
}
