﻿using Urho;

namespace BacteriolApplication.UrhoApp.Inputs
{
    class KeyMapping
    {
        public static KeyMapping Mapping { get => new KeyMapping(); }


        public virtual Key Forward { get => Key.Z; }
        public virtual Key Backward { get => Key.S; }
        public virtual Key Left { get => Key.Q; }
        public virtual Key Right { get => Key.D; }
        public virtual Key Boost { get => Key.LeftShift; }
        public virtual Key Divide { get => Key.Space; }

    }
}
