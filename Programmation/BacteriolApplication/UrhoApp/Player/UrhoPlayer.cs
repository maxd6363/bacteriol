﻿using BacteriolApplication.UrhoApp.Entity;
using BacteriolApplication.UrhoApp.EntityGraphic;
using BacteriolApplication.UrhoApp.Inputs;
using Model.Core.Player;
using System.Collections.Generic;
using Urho;
using Urho.Gui;

namespace BacteriolApplication.UrhoApp.Player
{
    public abstract class UrhoPlayer : Component
    {

        public IPlayer Player { get; protected set; }
        public CameraCustom Camera
        {
            get => _camera;
            protected set {
                if (_camera != value)
                {
                    _camera = value;
                }
            }
        }




        public UrhoEntity ControlledEntity
        {
            get => _controlledEntity;
            protected set
            {
                if (_controlledEntity != value)
                {
                    _controlledEntity = value;
                }
            }
        }
        protected IList<AbstractInput> AvailableInputs { get; }
        
        protected EurobeatOverlay EurobeatOverlay { get; set; }
        protected TagText3D Tag { get; set; }


        private UrhoEntity _controlledEntity;
        private CameraCustom _camera;

        



        public UrhoPlayer()
        {
            BacteriolApplication.Current.Scene.CreateChild("Player").AddComponent(this);
            AvailableInputs = new List<AbstractInput>();
        }


        public abstract void TakeControl(Model.Core.Entity.IEntity entity);
        
        public abstract void ReleaseControl();


    }
}
