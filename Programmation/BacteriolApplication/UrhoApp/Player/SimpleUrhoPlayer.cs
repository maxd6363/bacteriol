﻿using BacteriolApplication.UrhoApp.EntityGraphic;
using BacteriolApplication.UrhoApp.Inputs;
using Model.Core.Player;
using Model.Core.Entity;
using Urho;
using System;
using BacteriolApplication.UrhoApp.Entity;
using System.Diagnostics;
using BacteriolApplication.UrhoApp.Utils;
using System.Linq;
using BacteriolApplication.UrhoApp.UIElement;

namespace BacteriolApplication.UrhoApp.Player
{
    public class SimpleUrhoPlayer : UrhoPlayer, IDeletable
    {
        public bool Deleted { get; protected set; }
        public HealthBar HealthBar { get; protected set; }

        public SimpleUrhoPlayer(bool cameraFollowing = true) : base()
        {
            var _app = Application.Current as BacteriolApplication;
            Player = new SimplePlayer();
            _app.Manager.CurrentGame.AddPlayer(Player);
            ControlledEntity = null;
            Camera = new CameraCustom(cameraFollowing);
            EurobeatOverlay = new EurobeatOverlay(this);
            HealthBar = new HealthBar(Player);


            AddInputs();
            Application.Current.Update += Update;
        }

        private void Update(UpdateEventArgs obj)
        {

        }


        private void AddInputs()
        {
            AvailableInputs.Add(new InputKeyboard());
            AvailableInputs.Add(new InputGamepad());

            foreach (AbstractInput input in AvailableInputs)
            {
                input.InputPressed += Input_InputPressed;
            }
        }

        private void Input_InputPressed(InputsActionArgs obj)
        {

            
            if (ControlledEntity == null) return;

            if (obj.Boost > UrhoConfig.UsedConfig.EuploteBaseSpeed)
                EurobeatOverlay.EnabledBoost = true;

            Model.Utils.Vector2 vect = obj.DegreeLookAt == null ? Model.Utils.Vector2.Zero : Model.Utils.Vector2.AngleToVector((float)obj.DegreeLookAt);
            ControlledEntity.Entity.Location += vect * obj.Boost / UrhoConfig.UsedConfig.EuploteBaseSpeed;
            ControlledEntity.Entity.LookingDirection = vect;

            //(ControlledEntity.Entity as Euplote).LookingDirection = vect;
            //(ControlledEntity.Entity as Euplote).MoveModule.Speed = obj.Boost;
            //(ControlledEntity.Entity as Euplote).MoveModule.Move(vect, obj.Boost);
        }

        public void Delete()
        {
            try
            {
                Application.Current.Update -= Update;
                BacteriolApplication.Current.Scene.RemoveChild(Node);
                HealthBar.Delete();
                ControlledEntity.Delete();

                Deleted = true;
            }
            catch (NullReferenceException)
            {
                Debug.WriteLine("Cannot delete entity from Scene");
            }
        }

        public override void TakeControl(IEntity entity)
        {
            if (entity == null) return;
            Player.EntityControlled = entity;
            ControlledEntity = BacteriolApplication.Current.UrhoEuplotes.First(e => e.Entity == entity);
            ControlledEntity.TakeControle();
            Tag = new TagText3D(ControlledEntity);
            HealthBar.Show();
        }


        public override void ReleaseControl()
        {
            if(Tag != null)
                Tag.Delete();
            

            Player.EntityControlled = null;
            HealthBar.Hide();


            if (ControlledEntity != null)
            {
                ControlledEntity.ReleaseControle();
                ControlledEntity = null;
            }
        }
    }
}

