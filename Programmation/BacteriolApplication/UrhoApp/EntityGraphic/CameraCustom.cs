﻿using BacteriolApplication.UrhoApp.Entity;
using BacteriolApplication.UrhoApp.Utils;
using Urho;
using Urho.Actions;

namespace BacteriolApplication.UrhoApp.EntityGraphic
{
    public class CameraCustom : Camera
    {
        public bool FixedCamera { get; set; } = true;
        public bool CameraFollowing { get; set; }
        public Skybox Skybox { get; }



        public CameraCustom(bool cameraFollowing = true) : base()
        {
            CameraFollowing = cameraFollowing;
            Application.Current.Update += Update;
            BacteriolApplication.Current.Scene.CreateChild().AddComponent(this);

            Skybox = new SkyboxCustom(Node);
            new ViewportCustom(this);
            Node.Position = UrhoConfig.UsedConfig.CameraDefaultPosition;


        }

        private void Update(UpdateEventArgs obj)
        {
            HandleCameraFollowing();
            HandleSkyboxFollowing();
        }
        private void HandleSkyboxFollowing()
        {
            if (!CameraFollowing) return;


            //var follow = Node.Position;
            //Skybox.Node.RunActionsAsync(new EaseIn(new MoveTo(1, new Vector3(follow.X, follow.Y, Skybox.Node.Position.Z)), 1));

        }

        private async void HandleCameraFollowing()
        {
            if (!CameraFollowing) return;

            if (BacteriolApplication.Current.CurrentPlayer.ControlledEntity != null)
            {
                var _follow = BacteriolApplication.Current.CurrentPlayer.ControlledEntity.Node.Position;
                await Node.RunActionsAsync(new EaseIn(new MoveTo(0.1f, new Vector3(_follow.X, _follow.Y, Node.Position.Z)), 1));
            }

        
        
        }


    }
}
