﻿using BacteriolApplication.UrhoApp.Player;
using BacteriolApplication.UrhoApp.Sound;
using BacteriolApplication.UrhoApp.Utils;
using System.Diagnostics;
using Urho;
using Urho.Audio;
using Urho.Gui;

namespace BacteriolApplication.UrhoApp.EntityGraphic
{
    public class EurobeatOverlay : BorderImage
    {
        public bool EnabledBoost
        {
            get => _enabledBoost;
            set
            {
                if (value != _enabledBoost)
                {
                    _enabledBoost = value;
                    Camera.Fov = _enabledBoost ? UrhoConfig.UsedConfig.CameraBoostFOV : UrhoConfig.UsedConfig.CameraNormalFOV;
                    Size = _enabledBoost ? new IntVector2(Graphics.Width, Graphics.Height) : IntVector2.Zero;
                }
            }
        }
        public SoundSource Sound { get; protected set; }


        protected Camera Camera { get => BacteriolApplication.Current.Camera; }
        protected Graphics Graphics { get => Application.Current.Graphics; }
        protected Urho.Audio.Sound Boost { get; }
        protected UrhoPlayer Player { get; set; }


        private bool _enabledBoost;

        public EurobeatOverlay(UrhoPlayer player) : base()
        {
            Application.Current.UI.Root.AddChild(this);
            Texture = Application.Current.ResourceCache.GetTexture2D(Assets.Texture.Boost);
            Position = IntVector2.Zero;
            Size = IntVector2.Zero;
            EnabledBoost = false;
            Player = player;
            Sound = Player.Node.CreateComponent<CustomSoundSource>();
            Sound.Gain = 1f;
            Boost = Application.Current.ResourceCache.GetSound(Assets.Sounds.Boost);
            Application.Current.Update += Update;
        }

        private void Update(UpdateEventArgs obj)
        {
            if (!EnabledBoost) return;
            Position = new IntVector2(ToolBox.Random.Next(-10, 10), ToolBox.Random.Next(-10, 10));
            if (!Sound.Playing)
                Sound.Play(Boost);

            EnabledBoost = false;

        }
    }
}
