﻿using BacteriolApplication.UrhoApp.Utils;
using Model.Core.Entity;
using System.Diagnostics;
using Urho;

namespace BacteriolApplication.UrhoApp.EntityGraphic
{
    public class MouseEntity : Component
    {
        protected float CameraToWorldRatioX;
        protected float CameraToWorldRatioY;
        protected float Hitbox;

        protected readonly BacteriolApplication App;
        protected StaticModel Model { get; }

        public MouseEntity()
        {
            App = BacteriolApplication.Current;
            App.Scene.CreateChild().AddComponent(this);
            Hitbox = 5f;
            Model = Node.CreateComponent<StaticModel>();
            Model.Model = App.ResourceCache.GetModel(Assets.Models.Sphere);
            Model.Material = App.ResourceCache.GetMaterial(Assets.Materials.Mouse);
            Node.Scale *= 2;
            Node.Position = Vector3.Zero;
            App.Update += Update;
            App.Input.TouchEmulation = true;
            App.Input.TouchBegin += TouchBegin;


            App.Input.SetMouseVisible(false);

        }

        private void TouchBegin(TouchBeginEventArgs obj)
        {
            foreach (Euplote e in App.Manager.CurrentGame.World.Euplotes)
            {
                if (Node.Position.X > e.Location.X - Hitbox && Node.Position.X < e.Location.X + Hitbox && Node.Position.Y > e.Location.Y - Hitbox && Node.Position.Y < e.Location.Y + Hitbox)
                {
                    App.CurrentPlayer.ReleaseControl();
                    App.CurrentPlayer.TakeControl(e);
                }
            }
        }

        private void Update(UpdateEventArgs obj)
        {
            CameraToWorldRatioX = 1f / (App.Graphics.Width / UrhoConfig.UsedConfig.WindowToWorldRatioX);
            CameraToWorldRatioY = 1f / (App.Graphics.Height / UrhoConfig.UsedConfig.WindowToWorldRatioX);
            float x = (App.Input.MousePosition.X - App.Graphics.Width / 2) * -App.Camera.Node.Position.Z * CameraToWorldRatioX + App.Camera.Node.Position.X;
            float y = (App.Input.MousePosition.Y - App.Graphics.Height / 2) * App.Camera.Node.Position.Z * CameraToWorldRatioY + App.Camera.Node.Position.Y;
            Node.Position = new Vector3(x, y, 0f);
        }
    }
}
