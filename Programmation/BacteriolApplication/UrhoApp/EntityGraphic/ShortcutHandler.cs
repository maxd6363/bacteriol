﻿using BacteriolApplication.UrhoApp.Entity.Bacteria;
using BacteriolApplication.UrhoApp.Utils;
using System.Diagnostics;
using Urho;

namespace BacteriolApplication.UrhoApp.EntityGraphic
{
    public class ShortcutHandler : Component
    {
        private readonly BacteriolApplication App;

        public ShortcutHandler()
        {
            App = BacteriolApplication.Current;
            App.Scene.CreateChild().AddComponent(this);
            App.Update += Update;
        }



        private void Update(UpdateEventArgs obj)
        {
            if (ToolBox.IsKeyDown(UrhoConfig.UsedConfig.ShortcutDrawDebug, false))
            {
                UrhoConfig.UsedConfig.DrawDebug = !UrhoConfig.UsedConfig.DrawDebug;
            }

            if (ToolBox.IsKeyDown(UrhoConfig.UsedConfig.ShortcutHUDDebug, false))
            {
                BacteriolApplication.Current.HUDFps.Visible = !BacteriolApplication.Current.HUDFps.Visible;
            }
            if (ToolBox.IsKeyDown(UrhoConfig.UsedConfig.ShortcutShowGradient, false))
            {
                BacteriolApplication.Current.GradientVisualizer.Toggle();
            }



            if (ToolBox.IsKeyDown(UrhoConfig.UsedConfig.ShortcutCameraBack, true))
            {
                App.Camera.Node.Position = new Vector3(App.Camera.Node.Position.X, App.Camera.Node.Position.Y, App.Camera.Node.Position.Z - 2f);

            }
            if (ToolBox.IsKeyDown(UrhoConfig.UsedConfig.ShortcutCameraForward, true))
            {
                App.Camera.Node.Position = new Vector3(App.Camera.Node.Position.X, App.Camera.Node.Position.Y, App.Camera.Node.Position.Z + 2f);
            }




            if (ToolBox.IsKeyDown(UrhoConfig.UsedConfig.ShortcutCameraFollowing, false))
            {
                BacteriolApplication.Current.CurrentPlayer.Camera.CameraFollowing = !(Application.Current as BacteriolApplication).CurrentPlayer.Camera.CameraFollowing;
            }

            if (ToolBox.IsKeyDown(UrhoConfig.UsedConfig.ShortcutReleaseControl, false))
            {
                BacteriolApplication.Current.CurrentPlayer.ReleaseControl();
            }


            if (ToolBox.IsKeyDown(UrhoConfig.UsedConfig.ShortcutPausePlayer, false))
            {
                var _game = BacteriolApplication.Current.Manager.CurrentGame;
                Debug.WriteLine("PAUSE/PLAY");
                if (_game.Started)
                {
                    _game.Stop();
                    
                }
                else
                    _game.Start();
            }



        }

    }
}
