﻿using BacteriolApplication.UrhoApp.Utils;
using Urho;
using Urho.Gui;

namespace BacteriolApplication.UrhoApp.EntityGraphic
{
    public class TextFPS : Text
    {

       
        
        public TextFPS()
        {
            Application.Current.Update += Update;

            VerticalAlignment = VerticalAlignment.Top;
            HorizontalAlignment = HorizontalAlignment.Left;
            Position = new IntVector2(UrhoConfig.UsedConfig.DefaultMargin, UrhoConfig.UsedConfig.DefaultMargin);
            SetColor(UrhoConfig.UsedConfig.DefaultFontColor);
            SetFont(Application.Current.ResourceCache.GetFont(Assets.Fonts.AnonymousPro), UrhoConfig.UsedConfig.DefaultFontSize);

            Application.Current.UI.Root.AddChild(this);
        }

        private void Update(UpdateEventArgs obj)
        {
            Value = $"{(int)(1.0 / obj.TimeStep)} FPS";
        }
    }
}
