﻿using BacteriolApplication.UrhoApp.Entity;
using BacteriolApplication.UrhoApp.Utils;
using Model.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using Urho;
using Urho.Gui;

namespace BacteriolApplication.UrhoApp.EntityGraphic
{
    public class TagText3D : Text3D, IDeletable
    {
        private UrhoEntity AttachedTo { get; }
        public bool Deleted { get; protected set; }
        public TagText3D(UrhoEntity attachTo)
        {
            AttachedTo = attachTo;
            AttachedTo.Node.CreateChild().AddComponent(this);
            TextAlignment = HorizontalAlignment.Center;
            Node.Position = new Urho.Vector3(0f, 50f, 0f);
            Node.Rotation = Quaternion.Identity;
            Node.Scale = UrhoConfig.UsedConfig.TagTextNodeScale;

            Text = $"{UrhoConfig.UsedConfig.TagTextValue}{AttachedTo.Entity.Health / (AppConfig.UsedConfig.EuploteBaseMaxHealth / 100):F1}{UrhoConfig.UsedConfig.TagTextPercent}";


            SetFont(Application.ResourceCache.GetFont(Assets.Fonts.AnonymousPro), 80);
            SetColor(Color.White);
            SetAlignment(HorizontalAlignment.Center, VerticalAlignment.Center);

            BacteriolApplication.Current.Update += Update;
        }

        private void Update(UpdateEventArgs obj)
        {
            Text = $"{UrhoConfig.UsedConfig.TagTextValue}{AttachedTo.Entity.Health / (AppConfig.UsedConfig.EuploteBaseMaxHealth / 100):F1}{UrhoConfig.UsedConfig.TagTextPercent}";
            SetColor(ToolBox.CalculateColorFromPercent(AttachedTo.Entity.Health / (AppConfig.UsedConfig.EuploteBaseMaxHealth / 100)));
            Node.Rotation = Quaternion.Invert(AttachedTo.Node.Rotation);
        }

        public void Delete()
        {
            BacteriolApplication.Current.Update -= Update;
            AttachedTo.Node.RemoveComponent(this);
            Opacity = 0;
        }

    }
}
