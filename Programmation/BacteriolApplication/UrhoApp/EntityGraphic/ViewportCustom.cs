﻿using BacteriolApplication.UrhoApp.Utils;
using Urho;

namespace BacteriolApplication.UrhoApp.EntityGraphic
{
    class ViewportCustom : Viewport
    {
        public ViewportCustom(Camera camera) : base(Application.Current.Context, (Application.Current as BacteriolApplication).Scene, camera)
        {

            Application.Current.Renderer.SetViewport(0, this);
            Application.Current.Renderer.HDRRendering = true;

            var rp = RenderPath.Clone();
            rp.Append(Application.Current.ResourceCache.GetXmlFile(Assets.PostProcess.Bloom));
            rp.Append(Application.Current.ResourceCache.GetXmlFile(Assets.PostProcess.FXAA3));
            RenderPath = rp;


        }
    }
}
