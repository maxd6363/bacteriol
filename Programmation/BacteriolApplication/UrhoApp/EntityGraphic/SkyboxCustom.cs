﻿using BacteriolApplication.UrhoApp.Utils;
using Urho;

namespace BacteriolApplication.UrhoApp.EntityGraphic
{
    class SkyboxCustom : Skybox
    {
        public SkyboxCustom(Node attach)
        {
            attach.CreateChild("Skybox").AddComponent(this);
            Node.Scale = UrhoConfig.UsedConfig.SkyboxScale;
            Model = Application.Current.ResourceCache.GetModel(Assets.Models.Box);
            Material = Application.Current.ResourceCache.GetMaterial(Assets.Materials.Skybox);
        }
    }
}
