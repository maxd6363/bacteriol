﻿using BacteriolApplication.UrhoApp.Utils;
using System;
using System.Diagnostics;

namespace BacteriolApplication.UrhoApp.EntityGraphic.Gradient
{
    public class SimpleGradientVisualizer : GradientVisualizer
    {

        public int Width { get; }
        public int Height { get; }
        public bool Visible { get; private set; }

        private GradientVector[,] Vectors;

        public SimpleGradientVisualizer()
        {
            var chunck = BacteriolApplication.Current.Manager.CurrentGame.World.GetChunk(Model.Utils.Vector2.Zero);
            Width = (int)chunck.Size.X;
            Height = (int)chunck.Size.Y;
            Debug.WriteLine($"{Width} {Height}");
            Vectors = new GradientVector[Width, Height];


            for (int i = 0; i < Width; i += UrhoConfig.UsedConfig.GradientStep)
            {
                for (int j = 0; j < Height; j += UrhoConfig.UsedConfig.GradientStep)
                {
                    var position = new Model.Utils.Vector2(i, j);
                    var value = BacteriolApplication.Current.Manager.CurrentGame.World.GetNodeValue(position) * 100;
                    if (value == null) continue;
                    Vectors[i, j] = new SimpleGradientVector((int)value, ToolBox.ConvertVector(position));
                    Debug.WriteLine(value);
                }
            }
        }

        public override void Toggle()
        {
            Visible = !Visible;

            for (int i = 0; i < Width; i += UrhoConfig.UsedConfig.GradientStep)
            {
                for (int j = 0; j < Height; j += UrhoConfig.UsedConfig.GradientStep)
                {
                    Vectors[i, j].Toggle();
                }
            }

        }
    }
}
