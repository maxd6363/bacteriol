﻿using BacteriolApplication.UrhoApp.Utils;
using System.Diagnostics;
using Urho;

namespace BacteriolApplication.UrhoApp.EntityGraphic.Gradient
{
    public class SimpleGradientVector : GradientVector
    {
        public new int ValuePercent
        {
            get => valuePercent;
            set
            {
                if (value != valuePercent)
                {
                    valuePercent = value;
                    Model.Material = Material.FromColor(ToolBox.ColorGradient(valuePercent));
                }
            }
        }
        public bool Visible { get; private set; }


        private StaticModel Model { get; }

        private int valuePercent;


        public SimpleGradientVector(int value, Vector3 position) : base(value, position)
        {
            Visible = false;
            BacteriolApplication.Current.Scene.CreateChild().AddComponent(this);
            Model = Node.CreateComponent<StaticModel>();
            Model.Model = BacteriolApplication.Current.ResourceCache.GetModel(Assets.Models.Sphere);
            ValuePercent = value;
            Node.Position = position;
            Node.Scale = Vector3.Zero;

        }

        public override void Toggle()
        {
            Visible = !Visible;
            if (Visible)
                Node.Scale = Vector3.One * 5;
            else
                Node.Scale = Vector3.Zero;
        }
    }
}
