﻿using Urho;

namespace BacteriolApplication.UrhoApp.EntityGraphic.Gradient
{
    public abstract class GradientVector : Component
    {
        public int ValuePercent { get; protected set; }
        public GradientVector(int value, Vector3 position)
        {
            ValuePercent = value;
        }

        public abstract void Toggle();



    }
}
