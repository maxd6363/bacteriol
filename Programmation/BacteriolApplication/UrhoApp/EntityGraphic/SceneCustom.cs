﻿using Urho;
using Urho.Physics;

namespace BacteriolApplication.UrhoApp.EntityGraphic
{
    class SceneCustom : Scene
    {
        private static readonly int BOUNDING_BOX_SIZE = 1000;


        public SceneCustom()
        {
            CreateComponent<Octree>();
            CreateComponent<PhysicsWorld>().SetGravity(Vector3.Zero);
            CreateComponent<DebugRenderer>();
            Zone zone = Scene.CreateComponent<Zone>();
            zone.SetBoundingBox(new BoundingBox(Vector3.One * -BOUNDING_BOX_SIZE, Vector3.One * BOUNDING_BOX_SIZE));
            zone.AmbientColor = Color.White;
        }
    }
}
