﻿using Model.Manager;
using System;
using Urho;

namespace BacteriolApplication.UrhoApp.Utils
{
    public abstract class ModelEventHandler
    {

        protected BacteriolApplication App { get; }
        public IManager Manager { get; }


        public ModelEventHandler()
        {
            App = Application.Current as BacteriolApplication;
            Manager = App.Manager;
        }

    }
}
