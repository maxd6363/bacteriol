﻿namespace BacteriolApplication.UrhoApp.Utils
{
    public static class Assets
    {
        public static class Materials
        {
            public const string Bacteria = "Materials/Bacteria.xml";
            public const string EuploteControlled = "Materials/EuploteControlled.xml";
            public const string Euplote = "Materials/Euplote.xml";
            public const string Skybox = "Materials/Skybox.xml";
            public const string Mouse = "Materials/mouseCross.xml";

        }

        public static class Models
        {
            public const string Sphere = "Models/Sphere.mdl";
            public const string Box = "Models/Box.mdl";
            public const string Bacteria = "Models/Bacterial.mdl";
            public const string Euplote = "Models/Euplote.mdl";
        }

        public static class Sounds
        {
            public const string Crounch = "Sounds/crounch.wav";
            public const string Divide = "Sounds/divide.wav";
            public const string Boost = "Sounds/boost.wav";
            public const string Boost2 = "Sounds/boost2.wav";
            public const string Music1 = "Sounds/music1.wav";
            public const string Music2 = "Sounds/music2.wav";
            public const string Music3 = "Sounds/music3.wav";
        }

        public static class PostProcess
        {
            public const string AutoExposure = "PostProcess/AutoExposure.xml";
            public const string Bloom = "PostProcess/Bloom.xml";
            public const string BloomHDR = "PostProcess/BloomHDR.xml";
            public const string Blur = "PostProcess/Blur.xml";
            public const string ColorCorrection = "PostProcess/ColorCorrection.xml";
            public const string FXAA2 = "PostProcess/FXAA2.xml";
            public const string FXAA3 = "PostProcess/FXAA3.xml";
            public const string GammaCorrection = "PostProcess/GammaCorrection.xml";
            public const string GreyScale = "PostProcess/GreyScale.xml";
            public const string Tonemap = "PostProcess/Tonemap.xml";

        }


        public static class Fonts
        {
            public const string AnonymousPro = "Fonts/Anonymous Pro.ttf";
            public const string TimeNewRoman = "Fonts/TimeNewRoman.ttf";
        }

        public static class Texture
        {
            public const string Boost = "Textures/overlay.png";
        }

        public static class Animation
        {
            public const string Euplote = "Animations/Euplote.ani";
        }

        public static class XML
        {
            public const string ProgressBar = "XML/progressBar.xml";
            public const string DefaultStyle = "XML/default_style.xml";
        }


    }
}
