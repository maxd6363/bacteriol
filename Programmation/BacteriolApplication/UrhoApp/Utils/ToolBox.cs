﻿using System;
using Urho;

namespace BacteriolApplication.UrhoApp.Utils
{
    public static class ToolBox
    {

        public static float DegreeToRadian(double angle)
        {
            return (float)(Math.PI * angle / 180.0);
        }

        public static float RadianToDegree(double angle)
        {
            return (float)(angle * 180.0 / Math.PI);
        }


        public static float DAngleBetween2Points(Vector3 start, Vector3 end)
        {
            return -RadianToDegree(Math.Atan2(start.Y - end.Y, end.X - start.X));
        }


        public static float DCos(double angle)
        {
            return (float)Math.Cos(DegreeToRadian(angle));
        }
        public static float DSin(double angle)
        {
            return (float)Math.Sin(DegreeToRadian(angle));
        }

        public static Vector3 AngleToVector(double angle)
        {
            return new Vector3(ToolBox.DCos(angle), ToolBox.DSin(angle), 0);
        }
        public static float VectorToAngle(Vector3 vector)
        {
            return RadianToDegree(Math.Atan2(vector.X, vector.Y));
        }


        public static float VectorToAngle(Model.Utils.Vector2 vector)
        {
            return RadianToDegree(Math.Atan2(vector.X, vector.Y));
        }



        public static Random Random { get; } = new Random();


        public static Color CalculateColorFromPercent(float percent)
        {

            if (percent <= 10)
                return Color.FromHex("#ff1100");
            if (percent <= 20)
                return Color.FromHex("#ff3c00");
            if (percent <= 30)
                return Color.FromHex("#ff7700");
            if (percent <= 40)
                return Color.FromHex("#ffc400");
            if (percent <= 50)
                return Color.FromHex("#ffff00");
            if (percent <= 60)
                return Color.FromHex("#c3ff00");
            if (percent <= 70)
                return Color.FromHex("#77ff00");
            if (percent <= 80)
                return Color.FromHex("#33ff00");
            return Color.FromHex("#000000");
            

        }


        public static int MappingInt(int value, int inputStart, int inputEnd, int outputStart, int outputEnd)
        {
            return outputStart + ((outputEnd - outputStart) / (inputEnd - inputStart)) * (value - inputStart);
        }


        public static bool IsKeyDown(Key key, bool continuous)
        {
            return continuous ? Application.Current.Input.GetKeyDown(key) : Application.Current.Input.GetKeyPress(key);

        }


        public static Vector3 ConvertVector(Model.Utils.Vector3 vector)
        {
            return new Vector3((float)vector.X, (float)vector.Y, (float)vector.Z);
        }

        public static Vector3 ConvertVector(Model.Utils.Vector2 vector)
        {
            return new Vector3((float)vector.X, (float)vector.Y, 0);
        }


        public static Model.Utils.Vector3 ConvertVector(Vector3 vector)
        {
            return new Model.Utils.Vector3((float)vector.X, (float)vector.Y, (float)vector.Z);
        }

        public static Model.Utils.Vector3 ConvertVector(Vector2 vector)
        {
            return new Model.Utils.Vector3((float)vector.X, (float)vector.Y, 0);
        }



        public static Color ColorGradient(int percent)
        {
            if (percent <= 0)
                return Color.FromHex("#000000");
            if (percent <= 20)
                return Color.FromHex("#990000");
            if (percent <= 40)
                return Color.FromHex("#afbf00");
            if (percent <= 60)
                return Color.FromHex("#afbf00");
            if (percent <= 80)
                return Color.FromHex("#00d990");
            return Color.FromHex("#00ff08");


        }



    }
}
