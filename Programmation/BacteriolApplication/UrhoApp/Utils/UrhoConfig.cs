﻿using Urho;

namespace BacteriolApplication.UrhoApp.Utils
{
    public class UrhoConfig
    {
        private static UrhoConfig _usedConfig = null;
        public static UrhoConfig UsedConfig
        {
            get
            {
                if (_usedConfig == null)
                    _usedConfig = new UrhoConfig();
                return _usedConfig;
            }
        }


        private UrhoConfig()
        {

        }


        public float TagTextFontSize { get => 40; }
        public Vector3 TagTextNodeScale { get => Vector3.One * 35; }
        public string TagTextValue { get => "Value : "; }
        public string TagTextPercent { get => " %"; }

        public virtual string WorkingDirectory { get => "./"; }
        public bool DrawDebug { get => InternalDrawDebug; set => InternalDrawDebug = value; }
        protected virtual bool InternalDrawDebug { get; set; } = false;

        public virtual Vector3 SkyboxScale { get => new Vector3(3000, 3000, 3000); }
        public virtual Vector3 CameraDefaultPosition { get => new Vector3((float)Model.Utils.AppConfig.UsedConfig.ChunkSize.X / 2f, (float)Model.Utils.AppConfig.UsedConfig.ChunkSize.Y / 2f, -380); }
        public virtual float CameraNormalFOV { get => 60f; }
        public virtual float CameraBoostFOV { get => 90f; }





        public virtual float EntitySpawnEffectDuration { get => 2f; }
        public virtual int EntitySpawnBound { get => 100; }
        public virtual int EntityUpdateRate { get => 60; }


        public virtual Vector3 BacteriaSizeCapsule { get => Vector3.One / 5f; }
        public virtual Vector3 BacteriaNodeScale { get => Vector3.One / 5f; }
        public virtual float BacteriaHeightCapsule { get => 32f; }
        public virtual float BacteriaDiameterCapsule { get => 15f; }
        public virtual float BacteriaBaseSpeed { get => 35f; }




        public virtual float EuploteShapeRadius { get => 40f; }
        public virtual float EuploteBaseSpeed { get => 35f; }
        public virtual float EuploteBoostRatio { get => 3f; }
        public virtual float EuploteAnimationSpeed { get => 2.5f; }
        public virtual float EuploteAnimationRatio { get => 1f / 3f; }
        public virtual Vector3 EuploteNodeScale { get => Vector3.One / 2f; }



        public virtual Key ShortcutDrawDebug { get => Key.F8; }
        public virtual Key ShortcutHUDDebug { get => Key.F3; }
        public virtual Key ShortcutShowGradient { get => Key.F5; }
        public virtual Key ShortcutCameraBack { get => Key.KP_Minus; }
        public virtual Key ShortcutCameraForward { get => Key.KP_Plus; }
        public virtual Key ShortcutPausePlayer { get => Key.P; }
        public virtual Key ShortcutCameraFollowing { get => Key.C; }
        public virtual Key ShortcutReleaseControl { get => Key.R; }


        public virtual float WindowToWorldRatioX { get => 1.8f; }
        public virtual float WindowToWorldRatioY { get => 0.90f; }

        public virtual float MusicDeltaCheck { get => 5f; }
        public virtual float MusicGain { get => 0.1f; }
        
        
        public virtual int GradientStep{ get => 30; }
        
        
        public virtual string StatisticsFormat { get => "Bacterias : {0}\nEuplote : {1}"; }


        public virtual int DefaultFontSize { get => 20; }
        public virtual int DefaultMargin { get => 20; }
        public virtual Color DefaultFontColor { get => Color.White; }
        public float RefreshStatistics { get => 1f; }
    }
}
