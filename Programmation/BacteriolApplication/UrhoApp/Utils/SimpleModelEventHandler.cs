﻿using Model.Core.Entity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace BacteriolApplication.UrhoApp.Utils
{
    class SimpleModelEventHandler : ModelEventHandler
    {
        public SimpleModelEventHandler() : base()
        {
            Manager.CurrentGame.World.OnBacteriaDie += CurrentGame_OnBacteriaDie;
            Manager.CurrentGame.World.OnBacteriaDivide += CurrentGame_OnBacteriaDivide;
            Manager.CurrentGame.World.OnBacteriaSpawn += CurrentGame_OnBacteriaSpawn;
            Manager.CurrentGame.World.OnEuploteDie += CurrentGame_OnEuploteDie;
            Manager.CurrentGame.World.OnEuploteDivide += CurrentGame_OnEuploteDivide;
            Manager.CurrentGame.World.OnEuploteSpawn += CurrentGame_OnEuploteSpawn;
            Debug.WriteLine("EVENT SETUP");

        }

        protected void CurrentGame_OnBacteriaDie(object sender, Bacteria e)
        {
            BacteriolApplication.Current.DeleteEntityModel(e);
        }

        protected void CurrentGame_OnBacteriaDivide(object sender, Tuple<Bacteria, Bacteria> e)
        {
            //BacteriolApplication.Current.Factory.CreateBacteria(e.Item1);
        }

        protected void CurrentGame_OnBacteriaSpawn(object sender, Bacteria e)
        {
            BacteriolApplication.Current.Factory.CreateBacteria(e);
        }

        protected void CurrentGame_OnEuploteDie(object sender, Euplote e)
        {

            BacteriolApplication.Current.DeleteEntityModel(e);
        }

        protected void CurrentGame_OnEuploteDivide(object sender, Tuple<Euplote, Euplote> e)
        {
            //BacteriolApplication.Current.Factory.CreateEuplote(e.Item1);

        }

        protected void CurrentGame_OnEuploteSpawn(object sender, Euplote e)
        {
            BacteriolApplication.Current.Factory.CreateEuplote(e);
        }
    }
}
