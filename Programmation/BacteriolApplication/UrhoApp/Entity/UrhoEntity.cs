﻿using BacteriolApplication.UrhoApp.Entity.Behavior.Movement;
using BacteriolApplication.UrhoApp.Utils;
using Model.Core.Entity;
using System;
using System.Diagnostics;
using Urho;
using Urho.Actions;
using Urho.Audio;
using Urho.Physics;
using Urho.Shapes;

namespace BacteriolApplication.UrhoApp.Entity
{
    public abstract class UrhoEntity : Component, IDeletable
    {
        public IEntity Entity { get; protected set; }
        public RigidBody RigidBody { get; protected set; }
        public AnimatedModel Model3D { get; protected set; }
        public CollisionShape Shape { get; protected set; }
        public SoundSource Sound { get; protected set; }
        public bool Deleted { get; protected set; }
        public SmoothMovement SmoothMovement {get; protected set;}



        public UrhoEntity(IEntity entityModel) : base()
        {
            BacteriolApplication.Current.Scene.CreateChild("Entity").AddComponent(this);

            RigidBody = Node.CreateComponent<RigidBody>();
            Model3D = Node.CreateComponent<AnimatedModel>();
            Shape = Node.CreateComponent<CollisionShape>();
            Sound = Node.CreateComponent<SoundSource>();
            Entity = entityModel;
            Deleted = false;
            SmoothMovement = new SmoothMovement(this);

            Node.Position = ToolBox.ConvertVector(Entity.Location);
            
            var _vectorLook = ToolBox.ConvertVector(Entity.LookingDirection);
            Node.Rotation = Quaternion.FromAxisAngle(Node.Position, ToolBox.VectorToAngle(_vectorLook));
             

            Application.Current.Update += Update;


        }

        protected virtual void Update(UpdateEventArgs obj)
        {
            SmoothMovement.Move();
        }

        protected virtual void RandomPosition()
        {
            Node.Position = new Vector3(ToolBox.Random.Next(-UrhoConfig.UsedConfig.EntitySpawnBound, UrhoConfig.UsedConfig.EntitySpawnBound), ToolBox.Random.Next(-UrhoConfig.UsedConfig.EntitySpawnBound, UrhoConfig.UsedConfig.EntitySpawnBound), 0);
        }
     


        protected virtual async void SpawnEffect()
        {
            var _oldScaleX = Node.Scale.X;
            var _oldScaleY = Node.Scale.Y;
            var _oldScaleZ = Node.Scale.Z;
            Node.Scale = Vector3.Zero;
            await Node.RunActionsAsync(new EaseIn(new ScaleTo(UrhoConfig.UsedConfig.EntitySpawnEffectDuration, _oldScaleX, _oldScaleY, _oldScaleZ), 1));
        }

        public virtual void Delete()
        {

            try
            {
                Application.Current.Update -= Update;
                BacteriolApplication.Current.Scene.RemoveChild(Node);
                Deleted = true;
            }
            catch (NullReferenceException)
            {
                Debug.WriteLine("Cannot delete entity from Scene");
            }
        }

        public abstract void TakeControle();
        public abstract void ReleaseControle();

    }
}
