﻿using BacteriolApplication.UrhoApp.Utils;
using Model.Core.Entity;
using System;
using Urho;
using Urho.Gui;
using BacteriolApplication.UrhoApp.EntityGraphic;

namespace BacteriolApplication.UrhoApp.Entity.Euplote
{
    class SimpleUrhoEuplote : UrhoEuplote
    {
        public bool Controlled { get; private set; }



        public SimpleUrhoEuplote(IEntity entity ,bool controlled = false): base(entity) {
            Controlled = controlled;

            Model3D.AnimationEnabled = true;
            Model3D.Model = Application.ResourceCache.GetModel(Assets.Models.Euplote);
            Model3D.AddAnimationState(Application.ResourceCache.GetAnimation(Assets.Animation.Euplote)).Looped = true;
            Model3D.Material = Application.ResourceCache.GetMaterial(Controlled ? Assets.Materials.EuploteControlled : Assets.Materials.Euplote);
            Model3D.AnimationEnabled = true;


            var anim = Model3D.AddAnimationState(Application.ResourceCache.GetAnimation(Assets.Animation.Euplote));
            anim.Weight = 1;
            anim.Looped = true;
            Shape.SetSphere(UrhoConfig.UsedConfig.EuploteShapeRadius, Vector3.Zero, Quaternion.Identity);
            Node.Scale = UrhoConfig.UsedConfig.EuploteNodeScale;
            
            SpawnEffect();
        }



        protected override void Update(UpdateEventArgs obj)
        {
            base.Update(obj);
            HandleAnimation();
        }

        private void HandleAnimation()
        {
            try
            {
                if (Model3D.NumAnimationStates > 0)
                {
                    var state = Model3D.AnimationStates[0];
                    state.AddTime(Application.Current.Time.TimeStep * (RigidBody.LinearVelocity.Length <= UrhoConfig.UsedConfig.EuploteAnimationSpeed ? UrhoConfig.UsedConfig.EuploteAnimationSpeed : RigidBody.LinearVelocity.Length) * UrhoConfig.UsedConfig.EuploteAnimationRatio);
                }
            }
            catch (InvalidOperationException) { }
        }

        public override void Delete()
        {
            ReleaseControle();
            base.Delete();
        }

        public override void TakeControle()
        {
            Controlled = true;
            Model3D.Material = Application.ResourceCache.GetMaterial(Assets.Materials.EuploteControlled);

        }

        public override void ReleaseControle()
        {
            Controlled = false;
            Model3D.Material = Application.ResourceCache.GetMaterial(Assets.Materials.Euplote);
        }
    }
}
