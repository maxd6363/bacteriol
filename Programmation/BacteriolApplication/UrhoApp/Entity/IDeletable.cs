﻿namespace BacteriolApplication.UrhoApp.Entity
{
    public interface IDeletable 
    {
        bool Deleted { get; }

        void Delete();

    }
}
