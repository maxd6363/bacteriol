﻿using BacteriolApplication.UrhoApp.Entity.Bacteria;
using BacteriolApplication.UrhoApp.Utils;
using Model.Core.Entity;
using Model.Core.Entity.Modules;
using System;
using Urho;

namespace BacteriolApplication.UrhoApp.Entity.Behavior.Predator
{
    public class SimpleEuplotePredator : IPredator
    {
        public IEntity Entity { get; set; }
        public UrhoEntity UrhoEntity { get; }

        public IFood Target { get; set; }
        public UrhoEntity UrhoTarget { get; set; }

        public SimpleEuplotePredator(UrhoEntity urhoEntity)
        {
            UrhoEntity = urhoEntity;
        }


        public object Clone()
        {
            return null;
        }

        public void SearchNewTarget()
        {
            if (ToolBox.Random.Next(UrhoConfig.UsedConfig.EntityUpdateRate) == 0)
            {

                try
                {
                    if (BacteriolApplication.Current.UrhoBacterias.Count == 0) { throw new InvalidOperationException(); }

                    float min = float.MaxValue;
                    UrhoBacteria nearestBacteria = BacteriolApplication.Current.UrhoBacterias[0];
                    foreach (UrhoBacteria b in (Application.Current as BacteriolApplication).UrhoBacterias)
                    {
                        float distance = Vector3.Distance(b.Node.Position, UrhoEntity.Node.Position);
                        if (distance < min)
                        {
                            min = distance;
                            nearestBacteria = b;
                        }
                    }
                    Target = nearestBacteria.Entity as IFood;
                    UrhoTarget = nearestBacteria;
                }
                catch (InvalidOperationException)
                {
                    Target = null;
                    UrhoTarget = null;
                }
                catch (ArgumentOutOfRangeException)
                {
                    Target = null;
                    UrhoTarget = null;

                }
            }
        }

        public void EatTargetIfPossible()
        {

        }

        public void ReceiveNutriment(float nutrimentQuantity)
        {

        }
    }
}
