﻿using BacteriolApplication.UrhoApp.Inputs;
using BacteriolApplication.UrhoApp.Utils;
using Model.Core.Entity;
using Model.Core.Entity.Modules;
using Urho;
using Urho.Actions;

namespace BacteriolApplication.UrhoApp.Entity.Behavior.Movement
{
    public class SimpleEuploteMove
    {
        public IEntity Entity { get; set; }
        public UrhoEntity UrhoEntity { get; set; }

        public float Speed { get; }

        public SimpleEuploteMove(UrhoEntity urhoEntity)
        {
            UrhoEntity = urhoEntity;
        }


        public object Clone()
        {
            return null;
        }


        public async void Move(Model.Utils.Vector2? degree, float boost)
        {
            if (UrhoEntity == null) return;
            if (degree == null) return;
            var degreeProcessed = new Model.Utils.Vector2(-((Model.Utils.Vector2)degree).X, ((Model.Utils.Vector2)degree).Y);
            float deg = Model.Utils.Vector2.VectorToAngle(degreeProcessed) + 90;
            await UrhoEntity.Node.RunActionsAsync(new EaseElasticInOut(new MoveBy(0.1f, ToolBox.AngleToVector(deg) * boost * Application.Current.Time.TimeStep), 1));
            await UrhoEntity.Node.RunActionsAsync(new EaseBackOut(new RotateTo(0.8f, 0, 0, deg)));
        }
    }
}
