﻿using BacteriolApplication.UrhoApp.Utils;
using Model.Core.Entity;
using Model.Core.Entity.Modules;
using System.Diagnostics;
using Urho;
using Urho.Actions;

namespace BacteriolApplication.UrhoApp.Entity.Behavior.Movement
{
    public class SimpleEuploteInactiveMove
    {

        public IEntity Entity { get; set; }
        public UrhoEntity UrhoEntity { get; set; }

        public float Speed { get; }

        public SimpleEuploteInactiveMove(UrhoEntity urhoEntity)
        {
            UrhoEntity = urhoEntity;
        }


        public object Clone()
        {
            return null;
        }


        public async void Move(Model.Utils.Vector2? degree, float boost)
        {
            if (UrhoEntity == null) return;

            if (ToolBox.Random.Next(UrhoConfig.UsedConfig.EntityUpdateRate) == 0)
            {
                float deg = (float)ToolBox.Random.NextDouble() * 360;
                await UrhoEntity.Node.RunActionsAsync(new EaseElasticInOut(new MoveBy(3f, ToolBox.AngleToVector(deg) * boost * 50 * Application.Current.Time.TimeStep), 1));
                await UrhoEntity.Node.RunActionsAsync(new EaseBackOut(new RotateTo(3f, 0, 0, deg)));

            }



        }

    }
}
