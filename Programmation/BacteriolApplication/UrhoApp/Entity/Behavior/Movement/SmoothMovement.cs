﻿using BacteriolApplication.UrhoApp.Utils;
using Urho.Actions;

namespace BacteriolApplication.UrhoApp.Entity.Behavior.Movement
{
    public class SmoothMovement
    {
        public UrhoEntity UrhoEntity { get; }


        public SmoothMovement(UrhoEntity urhoEntity)
        {
            UrhoEntity = urhoEntity;
        }


        public async void Move()
        {
            if (UrhoEntity == null) return;
            await UrhoEntity.Node.RunActionsAsync(new MoveTo(0.01f,ToolBox.ConvertVector(UrhoEntity.Entity.Location)));
            await UrhoEntity.Node.RunActionsAsync(new EaseBackOut(new RotateTo(0.01f, ToolBox.VectorToAngle(UrhoEntity.Entity.LookingDirection))));
            
            
            //UrhoEntity.Node.Position = ToolBox.ConvertVector(UrhoEntity.Entity.Location);

        }


    }
}
