﻿using BacteriolApplication.UrhoApp.Entity.Behavior.Movement;
using BacteriolApplication.UrhoApp.Utils;
using Model.Core.Entity;
using System.Diagnostics;
using Urho;
using Urho.Audio;

namespace BacteriolApplication.UrhoApp.Entity.Bacteria
{
    public class SimpleUrhoBacteria : UrhoBacteria
    {

        
        public SimpleUrhoBacteria(IEntity entity) : base(entity)
        {           
            Model3D.Model = Application.ResourceCache.GetModel(Assets.Models.Bacteria);
            Model3D.Material = Application.ResourceCache.GetMaterial(Assets.Materials.Bacteria);
            Shape.SetCapsule(UrhoConfig.UsedConfig.BacteriaDiameterCapsule, UrhoConfig.UsedConfig.BacteriaHeightCapsule, Vector3.Zero, Quaternion.Identity);
            Node.Scale = UrhoConfig.UsedConfig.BacteriaNodeScale;

            SpawnEffect();

        }

        public override void ReleaseControle()
        {

        }

        public override void TakeControle()
        {

        }

        protected override void Update(UpdateEventArgs obj)
        {
            base.Update(obj);
        }


    }
}
