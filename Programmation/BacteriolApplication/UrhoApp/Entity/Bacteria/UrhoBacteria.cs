﻿using Model.Core.Entity;
using Urho;

namespace BacteriolApplication.UrhoApp.Entity.Bacteria
{
    public abstract class UrhoBacteria : UrhoEntity
    {
        public UrhoBacteria(IEntity entityModel) : base(entityModel)
        {
            BacteriolApplication.Current.UrhoBacterias.Add(this);
        }


        protected override void Update(UpdateEventArgs obj)
        {
            base.Update(obj);
        }
    }
}
