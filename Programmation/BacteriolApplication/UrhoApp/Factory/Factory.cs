﻿using BacteriolApplication.UrhoApp.Entity.Bacteria;
using BacteriolApplication.UrhoApp.Entity.Euplote;
using Model.Core.Entity;
using Urho;

namespace BacteriolApplication.UrhoApp.Factory
{
    public abstract class Factory
    {

        public abstract UrhoBacteria CreateBacteria(Bacteria bacteria);
        public abstract UrhoEuplote CreateEuplote(Euplote euplote, bool controlled = false);

    }
}
