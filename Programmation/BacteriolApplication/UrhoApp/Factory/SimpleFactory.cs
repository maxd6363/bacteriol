﻿using BacteriolApplication.UrhoApp.Entity.Bacteria;
using BacteriolApplication.UrhoApp.Entity.Euplote;
using Model.Core.Entity;

namespace BacteriolApplication.UrhoApp.Factory
{
    public class SimpleFactory : Factory
    {
        public override UrhoBacteria CreateBacteria(Bacteria bacteria)
        {
            var _ = new SimpleUrhoBacteria(bacteria);
            BacteriolApplication.Current.UrhoBacterias.Add(_);
            return _;
        }

        public override UrhoEuplote CreateEuplote(Euplote euplote, bool controlled = false)
        {
            var _ = new SimpleUrhoEuplote(euplote, controlled);
            BacteriolApplication.Current.UrhoEuplotes.Add(_);
            return _;
        }

    }
}
