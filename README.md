# BACTERIOL - Simulation d'un environnement microbiologique

## Références

### Documents externes

[Cahier des charges](https://docs.google.com/document/d/1nEbq7E9XbnIrUZ1NJLGPZfYX6FZs9HDI97WSNDubmbo/edit?usp=sharing)<br />
[Résumé des séances](https://docs.google.com/spreadsheets/d/1xbQ1Q5YAER3wUVjNGufHsNqsBt2ajV7Y4CbDJ_R-IeI/edit?usp=sharing)<br />
[Gantt prévisionnel collectif](https://docs.google.com/spreadsheets/d/1xXieZZhd72AWW81lxnmQ8VxlbHh6bqD-jdglQBDSUXc/edit?usp=sharing)<br />
[Gantt réel collectif](https://docs.google.com/spreadsheets/d/1Kb0oEcx33RGik8oo4ivi2Af4JeBj88h1ambikJTgGbM/edit?usp=sharing)<br />
[Gantt réel collectif(MS Project)](https://mega.nz/#!TsJABQZC!0u1Nsma-yxNeyU5QttUyyZceqXRkVrYKyzxxKpmmGQs)<br />
[Analyse des écarts](https://docs.google.com/spreadsheets/d/137fiMFGgdro2iH7RDnn3gdwVs0975JC85p9EEk93vv4/edit?usp=sharing)<br />
[Rapport de projet](https://docs.google.com/document/d/16Y4rlpBB25YafQdaM6-ZGO_ECVxpDhNCmx1j-ZEXOSQ/edit?usp=sharing)<br />

### Architecture du projet

```
.
├── Documentation
│   ├── Brainstorming
|   |   └── Cahier des charges.pdf
│   ├── Diagrammes
│   │   ├── Cas d'utilisation.mdj
│   │   ├── Classes.mdj
│   │   ├── Modèle du domaine.mdj
│   │   ├── Paquetage.mdj
│   │   └── Séquence système.mdj
│   ├── Gestion de projet
│   │   ├── Tâches à réaliser - A1.png
│   │   ├── Tâches à réaliser - A4 - Partie 1.png
│   │   ├── Tâches à réaliser - A4 - Partie 2.png
│   │   ├── Tâches à réaliser - A4 - Partie 3.png
│   │   ├── Tâches à réaliser - A4 - Partie 4.png
|   |   └── Tâches à réaliser - WBS.pdf
│   └── Visuel de l'application
│       ├── Sketchs.mdj
│       └── Storyboard.mdj
├── Programmation
│   ├── BacteriolApplication
│   │   └── ...
│   ├── Model
│   │   └── ...
│   ├── packages
│   │   └── ...
│   ├── Serialization
│   │   └── ...
│   ├── ViewDesktop
│   │   └── ...
│   └── Bacteriol.sln
├── .gitignore
└── README.md
```

## Comment utiliser Bacteriol

### Présentation

Bacteriol est un projet à but éducatif, en collaboration avec le microbiologiste Philippe BOUCHARD. Étant maître de conférences à UMR de Microorganismes de Clermont-Ferrand, M. BOUCHARD est souvent amené à faire des présentations de son travail à un public non spécialisé au sujet du monde micro bactérien, afin de faciliter la compréhension de tous, il fallait un outil ludique permettant d'intéresser l’auditoire. Nous avions donc pour mission de développer un logiciel permettant d’introduire de façon ludique, mais toutefois scientifique, le concept de microbiologie. Si nous rentrons plus en détail, M.BOUCHARD travaille actuellement sur ce que l’on nomme “le jus d'huître”. Il s’agit de l’eau contenue dans la coquille d’huître, elle est donc chargé en matière organique grâce à sa proximité avec l’animal. Cette eau est propice au développement micro bactériens, et de fait à l’aide d’un microscope on y voit évoluer des bactéries et des bactériophages. C’est tout un micro écosystème qui évolue dans la coquille d’huître, et qui est tout aussi fragile qu’un écosystème d’échelle macroscopique. Le but de M.BOUCHARD, à travers ses présentations, et de faire prendre conscience aux petits et aux grands qu’il existe à l’échelle d’une simple huître, tout un monde que l’Homme influence sans vraiment le savoir.

### Notice d'utilisation

Une fois Bacteriol installé et démarré, vous pouvez commencer à jouer. Pour cela vous devez créez un nouveau monde depuis le menu principal, choisir les paramètres de base de la partie puis cliquer sur "Charger". A partir de là vous pourrez choisir d'observer les entités évoluer dans l'environnement ou bien prendre part à la simulation en prenant le contrôle de l'une d'elles.

#### Contrôles
| Touche | Action correspondante |
|----|------|
| ZQSD | Diriger dans une direction l'entité contrôllée. |
| P  | Play / Pause |
| C  | Activer / Désactiver la caméra fixée au joueur. |
| R  | Arrêter de contrôller une entité. |
| +  | Zoomer avec la caméra. |
| -  | Dézoomer avec la caméra. |
| F2 | Prendre une capture d'écran. |
| F3 | Montrer / Chacher le nombre d'images par seconde. |
| F5 | Montrer / Cacher le gradient de la quantité de matière organique. |
| F8 | Montrer / Cacher les hitbox des entités. |







